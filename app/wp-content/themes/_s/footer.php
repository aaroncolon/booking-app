<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
    <div class="row column">
  		<div class="site-info">
  			&copy; 2020 Booking Inc
  		</div><!-- .site-info -->
    </div>
	</footer><!-- #colophon -->

  <?php get_template_part('template-parts/content', 'notifications'); ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

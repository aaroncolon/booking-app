import {
  SHOW_DIALOG_CONFIRM,
  HIDE_DIALOG_CONFIRM
} from '../actions/actionTypes';

const initialState = {
  confirmed: null,
  confirmAction: null,
  cancelAction: null,
  copy: '',
  showDialog: null
};

export default function(state = initialState, action) {
  switch (action.type) {

    case SHOW_DIALOG_CONFIRM : {
      const { confirmAction, cancelAction, copy, showDialog } = action.payload;

      return {
        confirmAction : confirmAction,
        cancelAction : cancelAction,
        copy : copy,
        showDialog : showDialog
      }
    }

    case HIDE_DIALOG_CONFIRM : {
      return {
        confirmAction : null,
        cancelAction : null,
        copy : '',
        showDialog : false
      }
    }
    
    default:
      return state;
  }

}

import {
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION
} from '../actions/actionTypes';

const initialState = {
  notifications: {}
};

export default function(state = initialState, action) {
  switch (action.type) {

    case ADD_NOTIFICATION : {
      const { id, copy } = action.payload;
      return {
        notifications: {
          ...state.notifications,
          [id]: {
            rId: id,
            copy: copy
          }
        }
      };
    }

    case REMOVE_NOTIFICATION : {
      const { id } = action.payload;
      return {
        notifications: {
          ...state.notifications,
          [id] : null
        }
      };
    }

    default:
      return state;
  }
}

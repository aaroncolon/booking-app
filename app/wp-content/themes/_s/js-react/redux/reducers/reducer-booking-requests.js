import {
  GET_BOOKING_REQUESTS,
  GET_BOOKING_REQUESTS_REQUEST,
  GET_BOOKING_REQUESTS_RECEIVE,
  GET_BOOKING_REQUESTS_FAILURE,
  GET_BOOKING_REQUESTS_ALL,
  GET_BOOKING_REQUESTS_ALL_REQUEST,
  GET_BOOKING_REQUESTS_ALL_RECEIVE,
  GET_BOOKING_REQUESTS_ALL_FAILURE,
  ACCEPT_BOOKING_REQUEST,
  ACCEPT_BOOKING_REQUEST_REQUEST,
  ACCEPT_BOOKING_REQUEST_RECEIVE,
  ACCEPT_BOOKING_REQUEST_FAILURE,
  REJECT_BOOKING_REQUEST,
  REJECT_BOOKING_REQUEST_REQUEST,
  REJECT_BOOKING_REQUEST_RECEIVE,
  REJECT_BOOKING_REQUEST_FAILURE
} from '../actions/actionTypes';

const initialState = {
  bookingRequests: {},
  page: 1,
  totalPages: 1,
  isFetching: false
};

// @NOTE state is immutable
export default function(state = initialState, action) {
  switch (action.type) {

    case GET_BOOKING_REQUESTS_REQUEST : {
      return {
        ...state,
        isFetching: true
      };
    }
    case GET_BOOKING_REQUESTS_RECEIVE : {
      const { bookingRequests, page, totalPages } = action.payload;
      const bookingRequestsObj = {};

      // bookingRequests.forEach((item) => {
      //   bookingRequestsObj[item.post_id] = {
      //     asset_type          : item.asset_type,
      //     booking_status      : item.booking_status,
      //     booking_status_code : item.booking_status_code,
      //     experience_level    : item.experience_level,
      //     date_start          : item.date_start,
      //     date_end            : item.date_end,
      //     post_id             : item.post_id,
      //     user_id             : item.user_id
      //   };
      // });

      bookingRequests.forEach((item) => {
        bookingRequestsObj[item.post_id] = {
          ...item
        };
      });

      return {
        ...state,
        bookingRequests: {
          ...state.bookingRequests,
          ...bookingRequestsObj
        },
        page: page,
        totalPages: totalPages,
        isFetching: false
      };
    }
    case GET_BOOKING_REQUESTS_FAILURE : {
      return {
        ...state,
        isFetching: false
      };
    }

    /**
     * Booking Requests All
     */
    case GET_BOOKING_REQUESTS_ALL_REQUEST : {
      return {
        ...state,
        isFetching: true
      };
    }
    case GET_BOOKING_REQUESTS_ALL_RECEIVE : {
      const { bookingRequests } = action.payload;
      const bookingRequestsObj = {};

      // bookingRequests.forEach((item) => {
      //   bookingRequestsObj[item.post_id] = {
      //     asset_type          : item.asset_type,
      //     booking_status      : item.booking_status,
      //     booking_status_code : item.booking_status_code,
      //     experience_level    : item.experience_level,
      //     date_start          : item.date_start,
      //     date_end            : item.date_end,
      //     post_id             : item.post_id,
      //     user_id             : item.user_id
      //   };
      // });
      
      bookingRequests.forEach((item) => {
        bookingRequestsObj[item.post_id] = {
          ...item
        };
      });
      
      return {
        ...state,
        bookingRequests: {
          ...state.bookingRequests,
          ...bookingRequestsObj
        },
        totalPages: null,
        isFetching: false
      };
    }
    case GET_BOOKING_REQUESTS_ALL_FAILURE : {
      return {
        ...state,
        isFetching: false
      };
    }


    /**
     * Accept Booking Request
     */
    case ACCEPT_BOOKING_REQUEST_REQUEST : {
      return {
        ...state,
        isFetching: true
      };
    }
    case ACCEPT_BOOKING_REQUEST_RECEIVE : {
      const { bookingRequest } = action.payload;
      
      return {
        ...state,
        bookingRequests: {
          ...state.bookingRequests,
          [bookingRequest.post_id] : {
            ...bookingRequest
          }
        },
        isFetching: false
      };
    }
    case ACCEPT_BOOKING_REQUEST_FAILURE : {
      return {
        ...state,
        isFetching: false
      };
    }

    /**
     * Reject Booking Request
     */
    case REJECT_BOOKING_REQUEST_REQUEST : {
      return {
        ...state,
        isFetching: true
      };
    }
    case REJECT_BOOKING_REQUEST_RECEIVE : {
      const { bookingRequest } = action.payload;
      
      return {
        ...state,
        bookingRequests: {
          ...state.bookingRequests,
          [bookingRequest.post_id] : null
        },
        isFetching: false
      };
    }
    case REJECT_BOOKING_REQUEST_FAILURE : {
      return {
        ...state,
        isFetching: false
      };
    }


    default:
      return state;
  }
}

import { combineReducers } from 'redux';
import asset from './reducer-asset';
import bookingRequests from './reducer-booking-requests';
import dialogConfirm from './reducer-dialog-confirm';
import notifications from './reducer-notifications';
import requester from './reducer-requester';

export default combineReducers({
  asset: asset,
  bookingRequests: bookingRequests,
  dialogConfirm: dialogConfirm,
  notifications: notifications,
  requester: requester
});

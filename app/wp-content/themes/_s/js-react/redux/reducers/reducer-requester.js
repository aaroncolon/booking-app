import {
   GET_ASSET_AVAILABILITY,
   GET_ASSET_AVAILABILITY_REQUEST,
   GET_ASSET_AVAILABILITY_RECEIVE,
   GET_ASSET_AVAILABILITY_FAILURE,
   CREATE_ASSET_REQUEST,
   CREATE_ASSET_REQUEST_REQUEST,
   CREATE_ASSET_REQUEST_RECEIVE,
   CREATE_ASSET_REQUEST_FAILURE,
   DELETE_ASSET_REQUEST,
   DELETE_ASSET_REQUEST_REQUEST,
   DELETE_ASSET_REQUEST_RECEIVE,
   DELETE_ASSET_REQUEST_FAILURE
} from '../actions/actionTypes';

const initialState = {
  availableAssets: [],
  createAssetRequestData: null,
  requestSummary: null,
  searchComplete: false,
  isFetching: false
};

// @NOTE state is immutable
export default function(state = initialState, action) {
  switch (action.type) {

    /**
     * Requester
     */
    case GET_ASSET_AVAILABILITY_REQUEST : {
      return {
        ...state,
        createAssetRequestData: null,
        requestSummary: null,
        searchComplete: false,
        isFetching: true
      };
    }
    case GET_ASSET_AVAILABILITY_RECEIVE : {
      const { assetData, totalPages } = action.payload;
      let availableAssets = [];
      let requestSummary = null;

      if (assetData.available_assets) {
        availableAssets = assetData.available_assets;
        requestSummary = {
          'asset_email'      : assetData.asset_email,
          'asset_id'         : assetData.available_assets[0].asset_id,
          'asset_type'       : assetData.asset_type,
          'experience_level' : assetData.experience_level,
          'date_start'       : assetData.date_start,
          'date_end'         : assetData.date_end,
          'available_assets' : assetData.available_assets
        };
      }

      return {
        ...state,
        availableAssets : availableAssets,
        requestSummary : requestSummary,
        searchComplete: true,
        isFetching : false
      };
    }
    case GET_ASSET_AVAILABILITY_FAILURE : {
      return {
        ...state,
        createAssetRequestData: null,
        requestSummary: null,
        searchComplete: false,
        isFetching: false
      };
    }


    /**
     * Create Asset Request
     */
    case CREATE_ASSET_REQUEST_REQUEST : {
      return {
        ...state,
        createAssetRequestData: null,
        isFetching: true
      };
    }
    case CREATE_ASSET_REQUEST_RECEIVE : {
      const { createAssetRequestData } = action.payload;

      return {
        ...state,
        createAssetRequestData: createAssetRequestData,
        requestSummary: null,
        searchComplete: false,
        isFetching: false
      };
    }
    case CREATE_ASSET_REQUEST_FAILURE : {
      return {
        ...state,
        createAssetRequestData: null,
        searchComplete: false,
        isFetching: false
      };
    }

    default:
      return state;
  }
}

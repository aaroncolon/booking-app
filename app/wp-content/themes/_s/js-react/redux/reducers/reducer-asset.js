import {
   GET_AVAILABILITY,
   GET_AVAILABILITY_REQUEST,
   GET_AVAILABILITY_RECEIVE,
   GET_AVAILABILITY_FAILURE,
   CREATE_AVAILABILITY,
   CREATE_AVAILABILITY_REQUEST,
   CREATE_AVAILABILITY_RECEIVE,
   CREATE_AVAILABILITY_FAILURE,
   DELETE_AVAILABILITY,
   DELETE_AVAILABILITY_REQUEST,
   DELETE_AVAILABILITY_RECEIVE,
   DELETE_AVAILABILITY_FAILURE,
   GET_ABILITIES,
   GET_ABILITIES_REQUEST,
   GET_ABILITIES_RECEIVE,
   GET_ABILITIES_FAILURE,
   CREATE_ABILITIES,
   CREATE_ABILITIES_REQUEST,
   CREATE_ABILITIES_RECEIVE,
   CREATE_ABILITIES_FAILURE,
   DELETE_ABILITIES,
   DELETE_ABILITIES_REQUEST,
   DELETE_ABILITIES_RECEIVE,
   DELETE_ABILITIES_FAILURE
} from '../actions/actionTypes';

const initialState = {
  abilitiesData: [],
  createAbilitiesData: null,
  deleteAbilitiesData: null,
  availabilityData: {},
  createAvailabilityData: null,
  deleteAvailabilityData: null,
  page: 1,
  totalPages: 1,
  isFetching: false,
};

// @NOTE state is immutable
export default function(state = initialState, action) {
  switch (action.type) {

    /**
     * Availability
     */
    case GET_AVAILABILITY_REQUEST : {
      return {
        ...state,
        isFetching: true
      };
    }
    case GET_AVAILABILITY_RECEIVE : {
      const { availabilityData, page, totalPages } = action.payload;
      const availabilityDataObj = {};

      availabilityData.forEach((item) => {
        availabilityDataObj[item.post_id] = {
          ...item
        }
      });

      return {
        ...state,
        availabilityData : {
          ...state.availabilityData,
          ...availabilityDataObj
        },
        page : page,
        totalPages : totalPages,
        isFetching : false
      };
    }
    case GET_AVAILABILITY_FAILURE : {
      return {
        ...state,
        isFetching: false
      };
    }


    case CREATE_AVAILABILITY_REQUEST : {
      return {
        ...state,
        isFetching: true
      };
    }
    case CREATE_AVAILABILITY_RECEIVE : {
      const { createAvailabilityData, totalPages } = action.payload;
      
      return {
        ...state,
        availabilityData : {
          ...state.availabilityData
        },
        createAvailabilityData: createAvailabilityData,
        isFetching: false
      };
    }
    case CREATE_AVAILABILITY_FAILURE : {
      return {
        ...state,
        createAvailabilityData: null,
        isFetching: false
      };
    }


    case DELETE_AVAILABILITY_REQUEST : {
      return {
        ...state,
        isFetching: true
      };
    }
    case DELETE_AVAILABILITY_RECEIVE : {
      const { deleteAvailabilityData, postId, totalPages } = action.payload;
      
      return {
        ...state,
        availabilityData : {
          ...state.availabilityData,
          [postId] : null
        },
        deleteAvailabilityData: deleteAvailabilityData,
        isFetching: false
      };
    }
    case DELETE_AVAILABILITY_FAILURE : {
      return {
        ...state,
        deleteAvailabilityData: null,
        isFetching: false
      };
    }





    /**
     * Abilities
     */
    case GET_ABILITIES_REQUEST : {
      return {
        ...state,
        isFetching: true
      }
    }
    case GET_ABILITIES_RECEIVE : {
      const { abilitiesData } = action.payload;
      return {
        ...state,
        abilitiesData: abilitiesData,
        isFetching: false
      }
    }
    case GET_ABILITIES_FAILURE : {
      return {
        ...state,
        isFetching: false
      }
    }


    case CREATE_ABILITIES_REQUEST : {
      return {
        ...state,
        isFetching: true
      }
    }
    case CREATE_ABILITIES_RECEIVE : {
      const { createAbilitiesData } = action.payload;
      return {
        ...state,
        createAbilitiesData: createAbilitiesData,
        isFetching: false
      }
    }
    case CREATE_ABILITIES_FAILURE : {
      return {
        ...state,
        isFetching: false
      }
    }


    case DELETE_ABILITIES_REQUEST : {
      return {
        ...state,
        isFetching: true
      }
    }
    case DELETE_ABILITIES_RECEIVE : {
      const { deleteAbilitiesData } = action.payload;
      return {
        ...state,
        deleteAbilitiesData: deleteAbilitiesData,
        isFetching: false
      }
    }
    case DELETE_ABILITIES_FAILURE : {
      return {
        ...state,
        deleteAbilitiesData: null,
        isFetching: false
      }
    }


    default:
      return state;
  }
}

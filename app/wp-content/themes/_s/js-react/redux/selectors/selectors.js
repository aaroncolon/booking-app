const getBookingRequests = (store) => {
  return store.bookingRequests.bookingRequests;
}

// @return {Array} array of store.bookingRequests.bookingRequests
export const selectBookingRequests = (store) => {
  const array = [];
  const bookingRequests = getBookingRequests(store);
  for (let item in bookingRequests) {
    array.push(bookingRequests[item]);
  }
  return array;
}

const getAvailabilityData = (store) => {
  return store.asset.availabilityData;
}

// @return {Array} array of store.asset.availabilityData
export const selectAvailabilityData = (store) => {
  const array = [];
  const availabilityData = getAvailabilityData(store);
  for (let item in availabilityData) {
    array.push(availabilityData[item]);
  }
  return array;
};

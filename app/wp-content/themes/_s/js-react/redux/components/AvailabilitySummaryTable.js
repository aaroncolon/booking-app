import React from 'react';
import { connect } from 'react-redux';

// Components
import AvailabilitySummaryTableRow from './AvailabilitySummaryTableRow';

// Selectors
import { selectAvailabilityData } from '../selectors/selectors';


class AvailabilitySummaryTable extends React.Component {
  constructor(props) {
    super(props);
  }

  doTableRows() {
    if (! this.props.availabilityData.length) {
      return null;
    }

    const rows = this.props.availabilityData.map((item, index) => {
      if (!item) {
        return null;
      }
      
      return (
        <AvailabilitySummaryTableRow 
          key={index + item.date_start + item.date_end}
          date_start={item.date_start}
          date_end={item.date_end}
          asset_request_id={item.asset_request_id}
          post_id={item.post_id} />
      );
    });

    return rows;
  }

  render() {
    return (
      <table id="availability-summary__table">
        <thead>
          <tr>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Booking ID</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {this.doTableRows()}
        </tbody>
      </table>
    );
  }
}

function mapStateToProps(state) {
  return {
    availabilityData: selectAvailabilityData(state)
  };
}

export default connect(mapStateToProps)(AvailabilitySummaryTable);

import React from 'react';
import { connect } from 'react-redux';

// Actions
import { getAbilities } from '../actions/actions-asset';

// Components
import AbilitiesSummaryTable from './AbilitiesSummaryTable';
import AbilitiesForm from './AbilitiesForm';

class AbilitiesSummary extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    this.props.getAbilities();

    return (
      <div className="row column">
        <div id="user-asset-types" className="section section--user-asset-types">
          <div className="row">
            <div className="column medium-8">
              <h2 className="title title--section">Abilities</h2>
              <h3 className="title title--column">Abilities Summary</h3>

              <div className="user-asset-types__summary">
                <AbilitiesSummaryTable />
              </div>

            </div>
            <div className="column medium-4">
              <h3 className="title title--column">Add Abilities</h3>

              <div className="user-asset-types__add">
                <AbilitiesForm />
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  getAbilities: getAbilities
};

export default connect(null, mapDispatchToProps)(AbilitiesSummary);

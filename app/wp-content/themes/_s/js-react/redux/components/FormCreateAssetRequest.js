import React from 'react';
import { connect } from 'react-redux';

// Actions
import { createAssetRequest } from '../actions/actions-requester';
import { 
  showDialogConfirm, 
  hideDialogConfirm 
} from '../actions/actions-dialog-confirm';

class FormCreateAssetRequest extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDialogConfirm = this.handleDialogConfirm.bind(this);
    this.handleDialogCancel = this.handleDialogCancel.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    this.props.showDialogConfirm({
      confirmAction : this.handleDialogConfirm,
      cancelAction : this.handleDialogCancel,
      copy : 'Please Confirm Your Request: ',
      showDialog : true
    });
  }

  handleDialogConfirm(e) {
    e.preventDefault();

    this.props.createAssetRequest({
      'asset_id'         : this.props.requestSummary.asset_id,
      'asset_email'      : this.props.requestSummary.asset_email,
      'asset_type'       : this.props.requestSummary.asset_type,
      'experience_level' : this.props.requestSummary.experience_level,
      'date_start'       : this.props.requestSummary.date_start,
      'date_end'         : this.props.requestSummary.date_end
    });
  }

  handleDialogCancel(e) {
    e.preventDefault();
    this.props.hideDialogConfirm();
  }

  render() {
    if (! this.props.requestSummary) {
      return null;
    }

    return (
      <>
        <form onSubmit={this.handleSubmit} method="POST">
          <button className="submit button expanded success">Complete Request</button>
        </form>
      </>
    );
  }
}

const mapDispatchToProps = {
  createAssetRequest : createAssetRequest,
  showDialogConfirm : showDialogConfirm,
  hideDialogConfirm : hideDialogConfirm
};

export default connect(null, mapDispatchToProps)(FormCreateAssetRequest);

import React from 'react';

export default function RequestSummaryCopy(props) {
  return (
    <div className="request-summary__message">
      <p>{props.copy}</p>
    </div>
  );
}

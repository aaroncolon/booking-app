import React from 'react';
import { connect } from 'react-redux';

import RequestSummaryRow from './RequestSummaryRow';

class CreateAssetRequestSummary extends React.Component {
  constructor(props) {
    super(props);
  }

  doRequestSummary() {
    const data = this.props.createAssetRequestData;
    return (
      <RequestSummaryRow 
        assetType={data.asset_type}
        experienceLevel={data.experience_level}
        dateStart={data.date_start}
        dateEnd={data.date_end}
        assetEmail={data.asset_email} />
    );
  }

  render() {
    if (! this.props.createAssetRequestData) {
      return null;
    }

    return (
      <div className="section section--request-summary">
        <header className="request-summary__header">
          <h2 className="title title--section">Request Summary</h2>
          <div className="request-summary__message"><p>Your asset request has been received. The asset you requested must now accept or reject your request. We will notify you of the results shortly.</p></div>
        </header>
        <div className="request-summary__summary">
          <table id="request-summary__table">
            <thead>
              <tr>
                <th>Asset Type</th>
                <th>Experience Level</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Asset Email</th>
              </tr>
            </thead>
            <tbody>
              {this.doRequestSummary()}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    createAssetRequestData: state.requester.createAssetRequestData
  };
}

export default connect(mapStateToProps)(CreateAssetRequestSummary);

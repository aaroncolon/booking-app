import React from 'react';
import { connect } from 'react-redux';

import { 
  getBookingRequests, 
  getBookingRequestsAll 
} from '../actions/actions-booking-requests';

class BookingRequestsPagination extends React.Component {
  constructor(props) {
    super(props);

    // event handlers
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.handleLoadAll = this.handleLoadAll.bind(this);
  }

  handleLoadMore(e) {
    e.preventDefault();

    const nextPage = ++this.props.bookingRequests.page;

    this.props.getBookingRequests({
      page: nextPage
    });
  }

  handleLoadAll(e) {
    e.preventDefault();

    this.props.getBookingRequestsAll();
  }

  render() {
    if (! this.props.bookingRequests.totalPages || this.props.bookingRequests.page >= this.props.bookingRequests.totalPages) {
      return null;
    }

    return (
      <div className="booking-requests__pagination">
        <div className="button-group">
          <button 
            type="button"
            className="button secondary"
            onClick={this.handleLoadMore} 
          >
            Load More
          </button>

          <button 
            type="button"
            className="button secondary"
            onClick={this.handleLoadAll}
          >
            Load All
          </button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    bookingRequests: state.bookingRequests
  }
};

const mapDispatchToProps = {
  getBookingRequests: getBookingRequests,
  getBookingRequestsAll: getBookingRequestsAll
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingRequestsPagination);

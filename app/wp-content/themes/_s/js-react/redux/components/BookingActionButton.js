import React from 'react';
import { connect } from 'react-redux';

// Actions
import {
  acceptBookingRequest,
  rejectBookingRequest
} from '../actions/actions-booking-requests';
import { 
  showDialogConfirm, 
  hideDialogConfirm 
} from '../actions/actions-dialog-confirm';

class BookingActionButton extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleDialogConfirm = this.handleDialogConfirm.bind(this);
    this.handleDialogCancel = this.handleDialogCancel.bind(this);
  }

  handleClick(e) {
    e.preventDefault();

    this.props.showDialogConfirm({
      confirmAction : this.handleDialogConfirm,
      cancelAction : this.handleDialogCancel,
      copy : 'Are You Sure You Want To '+ this.props.text +' This Booking Request?',
      showDialog : true
    });
  }

  handleDialogConfirm(e) {
    e.preventDefault();

    const action = this.props.action;
    const data = {
      action     : action,
      user_id    : this.props.user_id,
      post_id    : this.props.post_id,
      date_start : this.props.date_start,
      date_end   : this.props.date_end
    };

    if (action === "accept") {
      this.props.acceptBookingRequest(data);
    } else if (action === "reject") {
      this.props.rejectBookingRequest(data);
    }
  }

  handleDialogCancel(e) {
    e.preventDefault();
    this.props.hideDialogConfirm();
  }

  render() {
    return (
      <>
        <button
          onClick={this.handleClick}
          type="button"
          className={this.props.classes}
          id={this.props.post_id}
          name={this.props.post_id}
          data-post-id={this.props.post_id}
          data-date-start={this.props.date_start}
          data-date-end={this.props.date_end}
          data-user-id={this.props.user_id}
          data-action={this.props.action}
        >
          {this.props.text}
        </button>
      </>
    );
  }
}

const mapDispatchToProps = {
  acceptBookingRequest: acceptBookingRequest,
  rejectBookingRequest: rejectBookingRequest,
  showDialogConfirm: showDialogConfirm, 
  hideDialogConfirm: hideDialogConfirm
};

export default connect(null, mapDispatchToProps)(BookingActionButton);

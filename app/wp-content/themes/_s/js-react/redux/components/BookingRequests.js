import React from 'react';
import { connect } from 'react-redux';

// components
import BookingRequestsTable from './BookingRequestsTable';
import BookingRequestsPagination from './BookingRequestsPagination';

// actions
import { getBookingRequests } from '../actions/actions-booking-requests';

class BookingRequests extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    this.props.getBookingRequests();

    return (
      <div className="row column">
        <div className="section section--booking-requests">
          <h2 className="title title--section">Booking Requests</h2>
          <BookingRequestsTable />
          <BookingRequestsPagination />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  getBookingRequests: getBookingRequests
};

export default connect(null, mapDispatchToProps)(BookingRequests);

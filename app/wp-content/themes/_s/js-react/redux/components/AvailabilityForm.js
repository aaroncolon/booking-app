import React from 'react';
import { connect } from 'react-redux';

import Utilities from '../../Utilities';

// Actions
import { createAvailability } from '../actions/actions-asset';

class AvailabilityForm extends React.Component {
  constructor(props) {
    super(props);

    // state
    this.state = {
      minDate: Utilities.getDateTodayISO()
    };

    // refs
    this.dateStart = React.createRef();
    this.dateEnd = React.createRef();

    // events handlers
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    const dateStart = this.dateStart.current.value;
    const dateEnd   = this.dateEnd.current.value;

    this.props.createAvailability({
      date_start: dateStart,
      date_end: dateEnd
    });
  }

  handleDateChange(e) {
    e.preventDefault();
    this.setState({
      minDate: this.dateStart.current.value
    });
  }

  render() {
    const minDate = this.state.minDate;

    return (
      <form onSubmit={this.handleSubmit} action="" method="POST">

        <div className="form-group">
          <label htmlFor="date-start">Start Date</label>
          <input 
            ref={this.dateStart}
            onChange={this.handleDateChange} 
            className="form-control" 
            type="date" 
            id="date-start" 
            name="date-start" 
            required 
            pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" 
            min={Utilities.getDateTodayISO()} />
        </div>

        <div className="form-group">
          <label htmlFor="date-end">End Date</label>
          <input 
            ref={this.dateEnd}
            className="form-control" 
            type="date" 
            id="date-end" 
            name="date-end" 
            required 
            pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" 
            min={minDate} />
        </div>

        <div className="form-group">
          <button className="submit button primary expanded">Add</button>
        </div>

      </form>
    );
  }
}

const mapDispatchToProps = {
  createAvailability: createAvailability
};

export default connect(null, mapDispatchToProps)(AvailabilityForm);

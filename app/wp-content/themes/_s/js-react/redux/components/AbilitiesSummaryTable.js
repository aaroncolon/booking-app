import React from 'react';
import { connect } from 'react-redux';

// Components
import AbilitiesSummaryTableRow from './AbilitiesSummaryTableRow';

class AbilitiesSummaryTable extends React.Component {
  constructor(props) {
    super(props);
  }

  doRows() {
    if (! this.props.abilitiesData.length) {
      return null;
    }

    return this.props.abilitiesData.map((item, index) => {
      return (
        <AbilitiesSummaryTableRow
          key={index + item.asset_type_exp}
          asset_type={item.asset_type}
          experience_level={item.exp_level}
          asset_type_exp={item.asset_type_exp}
          asset_type_formatted={item.asset_type_formatted}
          exp_level_formatted={item.exp_level_formatted} />
      );
    });
  }

  render() {
    const rows = this.doRows();

    return (
      <table id="user-asset-types__table">
        <thead>
          <tr>
            <th>Asset Type</th>
            <th>Experience</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  }
}

function mapStateToProps(state) {
  return {
    abilitiesData: state.asset.abilitiesData
  };
}

export default connect(mapStateToProps)(AbilitiesSummaryTable);

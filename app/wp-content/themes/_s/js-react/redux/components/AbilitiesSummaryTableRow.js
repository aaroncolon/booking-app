import React from 'react';
import { connect } from 'react-redux';

import { deleteAbilities } from '../actions/actions-asset';
import { 
  showDialogConfirm, 
  hideDialogConfirm 
} from '../actions/actions-dialog-confirm';

class AbilitiesSummaryTableRow extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleDialogConfirm = this.handleDialogConfirm.bind(this);
    this.handleDialogCancel = this.handleDialogCancel.bind(this);
  }

  handleClick(e) {
    e.preventDefault();

    this.props.showDialogConfirm({
      confirmAction: this.handleDialogConfirm,
      cancelAction: this.handleDialogCancel,
      copy: 'Are you Sure You Want To Delete This Abilitiy?',
      showDialog: true
    });
  }

  handleDialogConfirm(e) {
    e.preventDefault();

    this.props.deleteAbilities({
      asset_type: this.props.asset_type,
      experience_level: this.props.experience_level
    });
  }

  handleDialogCancel(e) {
    e.preventDefault();
    this.props.hideDialogConfirm();
  }

  render() {
    return (
      <tr>
        <td>{this.props.asset_type}</td>
        <td>{this.props.experience_level}</td>
        <td>
          <a onClick={this.handleClick}>Delete</a>
        </td>
      </tr>
    );
  }
}

const mapDispatchToProps = {
  deleteAbilities: deleteAbilities,
  showDialogConfirm: showDialogConfirm,
  hideDialogConfirm: hideDialogConfirm
};

export default connect(null, mapDispatchToProps)(AbilitiesSummaryTableRow);

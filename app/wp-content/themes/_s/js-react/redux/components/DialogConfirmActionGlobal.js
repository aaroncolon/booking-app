import React from 'react';
import { connect } from 'react-redux';

class DialogConfirmActionGlobal extends React.Component {
  constructor(props) {
    super(props);

    this.options = {
      classes : {
        overlay: {
          base: 'dialog__overlay',
          visible: 'dialog__overlay--visible',
          removing: 'dialog__overlay--removing'
        },
        dialog: {
          base: 'dialog__confirm-action',
          visible: 'dialog__confirm-action--visible',
          removing: 'dialog__confirm-action--removing'
        }
      },
      delayFadeOut: 200
    };

    this.timerIdFadeOut = null;

    // Ref for class manipulation after creation
    this.refOverlay = React.createRef();
    this.refDialog = React.createRef();
  }

  doClasses() {
    // @NOTE initialized props.showDialog = null
    if (this.props.showDialog === true) {
      this.refOverlay.current.classList.add(this.options.classes.overlay.visible);
      this.refDialog.current.classList.add(this.options.classes.dialog.visible);
    } else if (this.props.showDialog === false) {
      this.refOverlay.current.classList.remove(this.options.classes.overlay.visible);
      this.refOverlay.current.classList.add(this.options.classes.overlay.removing);
      this.refDialog.current.classList.remove(this.options.classes.dialog.visible);
      this.refDialog.current.classList.add(this.options.classes.dialog.removing)
      this.setRemoveTimer();
    }
  }

  setRemoveTimer() {
    this.timerIdFadeOut = window.setTimeout(() => {
      setRemoveTimerCb(this);
    }, this.options.delayFadeOut);

    function setRemoveTimerCb(_this) {
      _this.resetDialog();
    }
  }

  resetDialog() {
    this.refOverlay.current.classList.remove(this.options.classes.overlay.removing);
    this.refDialog.current.classList.remove(this.options.classes.dialog.removing);

    window.clearTimeout(this.timerIdFadeOut);
    this.timerIdFadeOut = null;
  }

  render() {
    this.doClasses();

    return (
      <>
        <div ref={this.refOverlay} className={this.options.classes.overlay.base}></div>
        <div ref={this.refDialog} className={this.options.classes.dialog.base}>
          <p>{this.props.copy}</p>
          <div className="button-group">
            <button onClick={this.props.confirmAction} className="success button" type="button">Confirm</button>
            <button onClick={this.props.cancelAction} className="alert button" type="button">Cancel</button>
          </div>
        </div>
      </>
    );
  }
}

function mapStateToProps(state) {
  return {
    copy: state.dialogConfirm.copy,
    confirmAction: state.dialogConfirm.confirmAction, 
    cancelAction: state.dialogConfirm.cancelAction,
    showDialog: state.dialogConfirm.showDialog
  };
}

export default connect(mapStateToProps, null)(DialogConfirmActionGlobal);

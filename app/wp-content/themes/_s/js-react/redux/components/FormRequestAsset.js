import React from 'react';
import { connect } from 'react-redux';

import Utilities from '../../Utilities';

// actions
import { getAssetAvailability } from '../actions/actions-requester';

class FormRequestAsset extends React.Component {
  constructor(props) {
    super(props);

    // state
    this.state = {
      assetKind: 'new',
      minDate: Utilities.getDateTodayISO()
    };

    // refs
    this.assetEmail = React.createRef();
    this.assetType = React.createRef();
    this.expLevel = React.createRef();
    this.dateStart = React.createRef();
    this.dateEnd = React.createRef();

    // event handlers
    this.handleAssetType = this.handleAssetType.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleAssetType(e) {
    this.setState({
      assetKind: e.target.value
    });
  }

  handleDateChange(e) {
    this.setState({
      minDate: this.dateStart.current.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    
    const assetEmail = this.assetEmail.current.value;
    const assetType  = this.assetType.current.value;
    const expLevel   = this.expLevel.current.value;
    const dateStart  = this.dateStart.current.value;
    const dateEnd    = this.dateEnd.current.value;

    const data = {
      'asset_email'      : assetEmail,
      'asset_type'       : assetType,
      'experience_level' : expLevel,
      'date_start'       : dateStart,
      'date_end'         : dateEnd
    };

    this.props.getAssetAvailability(data);
  }

  generateOptions(data) {
    if (data) {
      return data.map((item, i) => {
        return <option key={i + item.slug} value={item.slug}>{item.name}</option>;
      });
    }
  }

  doAssetFields() {
    this.doAssetFieldsExisting();
    this.doAssetFieldsNew();
  }

  doAssetFieldsExisting() {
    const display = (this.state.assetKind === 'specific') ? 'block' : 'none';
    const disable = (this.state.assetKind === 'specific') ? false : true;

    return (
      <div className="form-group" style={{display: display}}>
        <label htmlFor="asset-email">Asset Email</label>
        <input 
          ref={this.assetEmail}
          className="form-control" 
          type="email" 
          id="asset-email" 
          name="asset-email" 
          required 
          disabled={disable} />
      </div>
    );
  }

  doAssetFieldsNew() {
    const display = (this.state.assetKind === 'new') ? 'block' : 'none';
    const disable = (this.state.assetKind === 'new') ? false : true;
    const assetTypes = this.generateOptions(_s_react.asset_types);
    const expLevels = this.generateOptions(_s_react.exp_levels);

    return (
      <>
        <div className="form-group form-group--asset-new" style={{display: display}}>
          <label htmlFor="asset-type">Asset Type</label>
          <select 
            ref={this.assetType}
            className="form-control" 
            id="asset-type" 
            name="asset-type" 
            required 
            disabled={disable} 
            defaultValue="">
            <option value="">Select Asset Type...</option>
            {assetTypes}
          </select>
        </div>

        <div className="form-group form-group--asset-new" style={{display: display}}>
          <label htmlFor="experience-level">Experience Level</label>
          <select 
            ref={this.expLevel}
            className="form-control" 
            id="experience-level" 
            name="experience-level" 
            required 
            disabled={disable} 
            defaultValue="">
            <option value="">Select Experience Level...</option>
            {expLevels}
          </select>
        </div>
      </>
    );
  }

  doDateFields() {
    const minDate = this.state.minDate;

    return (
      <>
        <div className="form-group">
          <label htmlFor="date-start">Start Date</label>
          <input ref={this.dateStart} onChange={this.handleDateChange} className="form-control" type="date" id="date-start" name="date-start" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" min={Utilities.getDateTodayISO()} />
        </div>

        <div className="form-group">
          <label htmlFor="date-end">End Date</label>
          <input ref={this.dateEnd}  className="form-control" type="date" id="date-end" name="date-end" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" min={minDate} />
        </div>
      </>
    );
  }

  render() {
    return (
      <form id="react-form-request-asset" action="" method="GET" onSubmit={this.handleSubmit}>

        <div className="form-group form-group--asset-new-specific">
          <fieldset name="asset-new-specific-options">
            <legend>New or Specific Asset?</legend>
            <div className="radio">
              <label>
                <input onChange={this.handleAssetType} type="radio" name="asset-new-specific" id="asset-new" value="new" defaultChecked />
                New Asset
              </label>
            </div>
            <div className="radio">
              <label>
                <input onChange={this.handleAssetType} type="radio" name="asset-new-specific" id="asset-specific" value="specific" />
                Specific Asset
              </label>
            </div>
          </fieldset>
        </div>

        {this.doAssetFieldsNew()}
        {this.doAssetFieldsExisting()}
        {this.doDateFields()}

        <div className="form-group">
          <button className="submit button expanded">Submit</button>
        </div>

      </form>
    );
  }
}

const mapDispatchToProps = {
  getAssetAvailability: getAssetAvailability
};

export default connect(null, mapDispatchToProps)(FormRequestAsset);

import React from 'react';

function RequestSummaryRow(props) {
  if (! props) {
    return null;
  }

  const assetType = (! props.assetType || props.assetType === 'null') ? 'n/a' : props.assetType;
  const expLevel  = (! props.experienceLevel || props.experienceLevel === 'null') ? 'n/a' : props.experienceLevel;
  const assetEmail = (! props.assetEmail || props.assetEmail === 'null') ? 'n/a' : props.assetEmail;

  return(
    <tr>
      <td>{assetType}</td>
      <td>{expLevel}</td>
      <td>{props.dateStart}</td>
      <td>{props.dateEnd}</td>
      <td>{assetEmail}</td>
    </tr>
  );
};

export default RequestSummaryRow;

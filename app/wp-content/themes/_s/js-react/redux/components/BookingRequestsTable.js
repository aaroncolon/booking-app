import React from 'react';
import { connect } from 'react-redux';

import { selectBookingRequests } from '../selectors/selectors';
import TableRow from './BookingRequestsTableRow';

class BookingRequestsTable extends React.Component {
  constructor(props) {
    super(props);
  }

  doTableHeader() {
    const userType = _s_utilities.user_type;

    if (userType === 'requester') {
      return (
        <tr>
          <td>Asset Type</td>
          <td>Exp Level</td>
          <td>Start Date</td>
          <td>End Date</td>
          <td>Asset Email</td>
          <td>Status</td>
        </tr>
      );
    } else {
      return (
        <tr>
          <td>Asset Type</td>
          <td>Exp Level</td>
          <td>Start Date</td>
          <td>End Date</td>
          <td>Status</td>
          <td>Action</td>
        </tr>
      );
    }
  }

  doTableBody() {
    let rows = [];
    if (this.props.bookingRequests) {
      rows = this.props.bookingRequests.map((item, index) => {
        return (! item) ? null : (
          <TableRow 
            key={item.post_id}
            user_type={_s_utilities.user_type}
            asset_type={item.asset_type}
            asset_email={item.asset_email}
            experience_level={item.experience_level}
            date_start={item.date_start}
            date_end={item.date_end}
            booking_status={item.booking_status}
            post_id={item.post_id} />
        );
      });
    }

    return rows;
  }

  render() {
    const header = this.doTableHeader();
    const body   = this.doTableBody();

    return (
      <table>    
        <thead>
          {header}
        </thead>
        <tbody>
          {body}
        </tbody>
      </table>
    );
  }
}

// return only the piece of state we need for this component (Selector)
function mapStateToProps(state) {
  const bookingRequests = selectBookingRequests(state);
  return {
    bookingRequests : bookingRequests
  };
}

export default connect(mapStateToProps)(BookingRequestsTable);

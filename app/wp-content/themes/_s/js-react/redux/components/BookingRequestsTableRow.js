import React from 'react';

import BookingActionButton from './BookingActionButton';

class BookingRequestsTableRow extends React.Component {
  constructor(props) {
    super(props);
  }

  doActionButtons() {
    if (this.props.user_type === 'asset' && this.props.booking_status === 'pending') {
      return (
        <div className="button-group">
          <BookingActionButton
            classes="button success"
            id={"btn-accept-" + this.props.post_id}
            name={"btn-accept-" + this.props.post_id}
            post_id={this.props.post_id}
            date_start={this.props.date_start}
            date_end={this.props.date_end}
            user_id={this.props.user_id}
            action="accept"
            text="Accept" />
          <BookingActionButton
            classes="button alert"
            id={"btn-reject-" + this.props.post_id}
            name={"btn-reject-" + this.props.post_id}
            post_id={this.props.post_id}
            date_start={this.props.date_start}
            date_end={this.props.date_end}
            user_id={this.props.user_id}
            action="reject"
            text="Reject" />
        </div>
      );
    } 
    // else if (_s_utilities.user_type === 'requester') {
    //   return (
    //     <div className="button-group">
    //       <BookingActionButton
    //         classes="button alert"
    //         id={"btn-cancel-" + this.props.post_id}
    //         name={"btn-cancel-" + this.props.post_id}
    //         post_id={this.props.post_id}
    //         action="cancel"
    //         text="Cancel" />
    //     </div>
    //   );
    // } 
    else {
      return 'n/a';
    }
  }

  render() {
    const assetType = (!this.props.asset_type || this.props.asset_type === 'null') ? 'n/a' : this.props.asset_type;
    const expLevel = (!this.props.experience_level || this.props.experience_level === 'null') ? 'n/a' : this.props.experience_level;
    const dateStart = this.props.date_start;
    const dateEnd = this.props.date_end;
    const assetEmail = (!this.props.asset_email || this.props.asset_email === 'null') ? 'n/a' : this.props.asset_email;
    const bookingStatus = this.props.booking_status;

    if (this.props.user_type === 'requester') {
      return(
        <tr>
          <td>{assetType}</td>
          <td>{expLevel}</td>
          <td>{dateStart}</td>
          <td>{dateEnd}</td>
          <td>{assetEmail}</td>
          <td>{bookingStatus}</td>
        </tr>
      );
    } else if (this.props.user_type === 'asset') {
      return(
        <tr>
          <td>{assetType}</td>
          <td>{expLevel}</td>
          <td>{dateStart}</td>
          <td>{dateEnd}</td>
          <td>{bookingStatus}</td>
          <td>
            {this.doActionButtons()}
          </td>
        </tr>
      );
    } else {
      return null;
    }
  }
}

export default BookingRequestsTableRow;

import React from 'react';
import { connect } from 'react-redux';

import { createAbilities } from '../actions/actions-asset';

class AbilitiesForm extends React.Component {
  constructor(props) {
    super(props);

    this.assetType = React.createRef();
    this.expLevel = React.createRef();

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  generateOptions(data) {
    if (data) {
      return data.map((item, i) => {
        return <option key={i + item.slug} value={item.slug}>{item.name}</option>;
      });
    }
  }

  handleSubmit(e) {
    e.preventDefault();

    const assetType = this.assetType.current.value;
    const expLevel = this.expLevel.current.value;

    this.props.createAbilities({
      'asset_type': assetType,
      'experience_level': expLevel
    });
  }

  render() {
    const assetTypes = this.generateOptions(_s_react.asset_types);
    const expLevels = this.generateOptions(_s_react.exp_levels);

    return (
      <form onSubmit={this.handleSubmit} method="POST">

        <div className="form-group">
          <label htmlFor="asset-type">Asset Type</label>
          <select ref={this.assetType} className="form-control" id="asset-type" name="asset-type" required defaultValue="">
            <option value="">Select Asset Type...</option>
            {assetTypes}
          </select>
        </div>

        <div className="form-group">
          <label htmlFor="experience-level">Experience Level</label>
          <select ref={this.expLevel} className="form-control" id="experience-level" name="experience-level" required defaultValue="">
            <option value="">Select Experience Level...</option>
            {expLevels}
          </select>
        </div>

        <div className="form-group">
          <button className="button primary expanded submit">Add</button>
        </div>

      </form>
    );
  }
}

const mapDispatchToProps = {
  createAbilities: createAbilities
};

export default connect(null, mapDispatchToProps)(AbilitiesForm);

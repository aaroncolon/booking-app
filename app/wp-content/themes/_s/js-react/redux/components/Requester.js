import React from 'react';

// Components
import FormRequestAsset from './FormRequestAsset';
import RequestSummary from './RequestSummary';
import CreateAssetRequestSummary from './CreateAssetRequestSummary';
import BookingRequests from './BookingRequests';
import DialogConfirmActionGlobal from './DialogConfirmActionGlobal';

class Requester extends React.Component {
  render() {
    return (
      <>
        <DialogConfirmActionGlobal />
        <div className="row">
          <div className="column medium-4">
            <div className="section section--request-asset"> 
              <h2 className="title title--section">Request Asset</h2>
              <div className="wrap wrap--form">
                <FormRequestAsset />
              </div>
            </div>
          </div>
          <div className="column medium-8">
            <RequestSummary />
            <CreateAssetRequestSummary />
          </div>
        </div>

        <div className="row column">
          <hr />
        </div>
        
        <BookingRequests />
      </>
    );
  }
}

export default Requester;

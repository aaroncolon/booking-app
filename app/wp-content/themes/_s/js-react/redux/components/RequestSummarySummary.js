import React from 'react';

import RequestSummaryRow from './RequestSummaryRow';
import FormCreateAssetRequest from './FormCreateAssetRequest';

class RequestSummarySummary extends React.Component {
  constructor(props) {
    super(props);
  }

  doRequestSummary() {
    const data = this.props.requestSummary;

    return (
      <RequestSummaryRow 
        assetType={data.asset_type}
        experienceLevel={data.experience_level}
        dateStart={data.date_start}
        dateEnd={data.date_end}
        assetEmail={data.asset_email} />
    );
  }

  render() {
    if (! this.props.requestSummary) {
      return null;
    }
    
    return (
      <>
        <div className="request-summary__summary">
          <table id="request-summary__table">
            <thead>
              <tr>
                <th>Asset Type</th>
                <th>Experience Level</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Asset Email</th>
              </tr>
            </thead>
            <tbody>
              {this.doRequestSummary()}
            </tbody>
          </table>
        </div>
        <div className="request-summary__button-wrap">
          <FormCreateAssetRequest requestSummary={this.props.requestSummary} />
        </div>
      </>
    );  
  }
  
}

export default RequestSummarySummary;

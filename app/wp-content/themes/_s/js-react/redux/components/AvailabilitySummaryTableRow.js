import React from 'react';
import { connect } from 'react-redux';

import { deleteAvailability } from '../actions/actions-asset';
import { 
  showDialogConfirm, 
  hideDialogConfirm 
} from '../actions/actions-dialog-confirm';

class AvailabilitySummaryTableRow extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleDialogConfirm = this.handleDialogConfirm.bind(this);
    this.handleDialogCancel = this.handleDialogCancel.bind(this);
  }

  handleClick(e) {
    e.preventDefault();

    this.props.showDialogConfirm({
      confirmAction : this.handleDialogConfirm,
      cancelAction : this.handleDialogCancel,
      copy : 'Are You Sure You Want To Delete This Unavailability?',
      showDialog : true
    });
  }

  handleDialogConfirm(e) {
    e.preventDefault();

    this.props.deleteAvailability({
      post_id: this.props.post_id
    });
  }

  handleDialogCancel(e) {
    e.preventDefault();
    this.props.hideDialogConfirm();
  }

  doActionButtons() {
    if (this.props.asset_request_id) {
      return null;
    }

    return (
      <a onClick={this.handleClick}>Delete</a>
    );
  }

  render() {
    return (
      <tr>
        <td>{this.props.date_start}</td>
        <td>{this.props.date_end}</td>
        <td>{this.props.asset_request_id}</td>
        <td>
          {this.doActionButtons()}
        </td>
      </tr>
    );
  }
}

const mapDispatchToProps = {
  deleteAvailability: deleteAvailability,
  showDialogConfirm: showDialogConfirm,
  hideDialogConfirm: hideDialogConfirm
};

export default connect(null, mapDispatchToProps)(AvailabilitySummaryTableRow);

import React from 'react';
import { connect } from 'react-redux';

// Components
import AvailabilitySummaryTable from './AvailabilitySummaryTable';
import AvailabilityForm from './AvailabilityForm';

// Actions 
import { getAvailability } from '../actions/actions-asset';

class AvailabilitySummary extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    this.props.getAvailability();

    return (
      <div className="row">

        <div className="column medium-8">
          <div id="availability-summary" className="section section--availability-summary">
            <h2 className="title title--section">Unavailability Summary</h2>
            <p>You will not be booked for the dates below:</p>

            <div className="availability-summary__table-wrap">
              <AvailabilitySummaryTable />
            </div>
          </div>
        </div>

        <div className="column medium-4">
          <div id="availability-add" className="section section--availability-add">
            <h2 className="title title--section">Add Unavailability</h2>
            <AvailabilityForm />
          </div>
        </div>

      </div>
    );
  }
}

const mapDispatchToProps = {
  getAvailability: getAvailability
};

export default connect(null, mapDispatchToProps)(AvailabilitySummary);

import React from 'react';

// Components
import BookingRequests from './BookingRequests';
import DialogConfirmActionGlobal from './DialogConfirmActionGlobal';
import AvailabilitySummary from './AvailabilitySummary';
import AbilitiesSummary from './AbilitiesSummary';

class Asset extends React.Component {
  render() {
    return (
      <>
        <DialogConfirmActionGlobal />

        <div className="row column">
          <p><strong>Last Booked: </strong> {_s_asset.last_booked}</p>
        </div>
        <BookingRequests />

        <div className="row column">
          <hr />
        </div>

        <AvailabilitySummary />
        <div className="row column">
          <hr />
        </div>

        <AbilitiesSummary />
      </>
    );
  }
}

export default Asset;

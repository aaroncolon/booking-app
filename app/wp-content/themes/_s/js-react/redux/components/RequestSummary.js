import React from 'react';
import { connect } from 'react-redux';

import RequestSummaryCopy from './RequestSummaryCopy';
import RequestSummarySummary from './RequestSummarySummary';

class RequestSummary extends React.Component {
  constructor(props) {
    super(props);

    this.copy = {
      'assetDefault'     : 'Please request an asset.',
      'assetAvailable'   : 'Match found. Please confirm your request.',
      'assetUnavailable' : 'No matches available at this time.',
      'requestAssetComplete' : 'Your asset request has been received. The asset you requested must now accept or reject your request. We will notify you of the results shortly.'
    };
  }

  doRequestCopy() {
    let copy = this.copy.assetDefault;

    if (this.props.requestSummary) {
      copy = this.copy.assetAvailable;
    } else {
      copy = this.copy.assetUnavailable;
    }

    return (
      <RequestSummaryCopy 
        copy={copy} />
    );
  }

  render() {
    if (! this.props.searchComplete) {
      return null;
    }

     return (
      <div className="section section--request-summary">
        <header className="request-summary__header">
          <h2 className="title title--section">Request Summary</h2>
          {this.doRequestCopy()}
        </header>
        <RequestSummarySummary requestSummary={this.props.requestSummary} />
      </div>
    );
  }
};

function mapStateToProps(state) {
  return {
    requestSummary: state.requester.requestSummary,
    searchComplete: state.requester.searchComplete,
    createAssetRequestData: state.requester.createAssetRequestData
  };
}

export default connect(mapStateToProps)(RequestSummary);

import React from 'react';
import { connect } from 'react-redux';

// components
import Notification from './Notification';

// actions
import { removeNotification } from '../actions/actions-notifications';

class Notifications extends React.Component {
  constructor(props) {
    super(props);
    this.handleRemoveNotification = this.handleRemoveNotification.bind(this);
  }
  
  handleRemoveNotification(_id) {
    // dispatch redux action
    this.props.removeNotification(_id);
  }

  formatData() {
    // create array of notifItems
    let arr = [];
    for (let item in this.props.notifItems) {
      arr.push(this.props.notifItems[item]);
    }
    return arr;
  }

  render() {
    // every time the state updates, we render a notification...
    if (! this.props.notifItems) {
      return null;
    }

    // map our elements
    const arr = this.formatData();
    const notifs = arr.map((item, i) => {
      if (!item) {
        return null;
      }

      const id   = item.rId;
      const copy = item.copy;
      return (
        <Notification 
          key={id} 
          copy={copy} 
          id={id} 
          removeNotification={this.handleRemoveNotification} />
      );
    });

    return (
      <div className="section-notifications">
        <div id="notification-wrap" className="notification-wrap">
          {(notifs) ? notifs : null}
        </div>
      </div>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    notifItems : state.notifications.notifications
  };
};

const mapDispatchToProps = { 
  removeNotification: removeNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);

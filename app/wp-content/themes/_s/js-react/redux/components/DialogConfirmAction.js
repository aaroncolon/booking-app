import React from 'react';

function DialogConfirmAction(props) {
  return (
    <div className="dialog__confirm-action">
      <p>{props.copy}</p>
      <div className="button-group">
        <button onClick={props.confirmAction} className="success button" type="button">Confirm</button>
        <button onClick={props.cancelAction} className="alert button" type="button">Cancel</button>
      </div>
    </div>
  );
}

export default DialogConfirmAction;

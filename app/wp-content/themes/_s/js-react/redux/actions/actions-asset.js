import fetch from 'cross-fetch';
import Utilities from '../../Utilities'
import {
  GET_AVAILABILITY,
  GET_AVAILABILITY_REQUEST,
  GET_AVAILABILITY_RECEIVE,
  GET_AVAILABILITY_FAILURE,
  CREATE_AVAILABILITY,
  CREATE_AVAILABILITY_REQUEST,
  CREATE_AVAILABILITY_RECEIVE,
  CREATE_AVAILABILITY_FAILURE,
  DELETE_AVAILABILITY,
  DELETE_AVAILABILITY_REQUEST,
  DELETE_AVAILABILITY_RECEIVE,
  DELETE_AVAILABILITY_FAILURE,
  GET_ABILITIES,
  GET_ABILITIES_REQUEST,
  GET_ABILITIES_RECEIVE,
  GET_ABILITIES_FAILURE,
  CREATE_ABILITIES,
  CREATE_ABILITIES_REQUEST,
  CREATE_ABILITIES_RECEIVE,
  CREATE_ABILITIES_FAILURE,
  DELETE_ABILITIES,
  DELETE_ABILITIES_REQUEST,
  DELETE_ABILITIES_RECEIVE,
  DELETE_ABILITIES_FAILURE
} from './actionTypes';

import { hideDialogConfirm } from './actions-dialog-confirm';
import { addNotification } from './actions-notifications';

// @NOTE actions are functions that return plain objects with `type` and `payload` properties

export function getAvailability(data = {page: 1}) {
  // Thunk
  return function(dispatch) {
    dispatch(getAvailabilityRequest());

    const url = _s_utilities.rest_url + 'booking/v1/availability';
    const fullUrl = url + '?' + Utilities.buildQuery(data);
    const options = {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      }
    };

    return fetch(fullUrl, options)
      .then(
        (response) => {
          return {
            totalPages: response.headers.get('X-WP-TotalPages'),
            json: response.json()
          }; // returns value to next .then()
        },
        (error) => {
          console.log('Fetch error occured', error);
          dispatch(getAvailabilityFailure(error));
        }
      ).then(
        (response2) => {
          // resolve response.json()
          response2.json.then((jsonData) => {
            return dispatch(getAvailabilityReceive(
              {
                jsonData : jsonData,
                page: data.page,
                totalPages : response2.totalPages
              }
            ));
          });
        }
      );

  }
}
export function getAvailabilityRequest() {
  return {
    type: GET_AVAILABILITY_REQUEST,
    payload: {}
  };
}
export function getAvailabilityReceive(response) {
  return {
    type: GET_AVAILABILITY_RECEIVE,
    payload: {
      availabilityData: response.jsonData,
      page: response.page,
      totalPages: response.totalPages
    }
  };
}
export function getAvailabilityFailure() {
  return {
    type: GET_AVAILABILITY_FAILURE,
    payload: {}
  };
}



export function createAvailability(data) {
  // Thunk
  return function(dispatch) {
    dispatch(createAvailabilityRequest());

    const url = _s_utilities.rest_url + 'booking/v1/availability';
    const options = {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      },
      body: JSON.stringify(data)
    };

    return fetch(url, options)
      .then(
        (response) => {
          return {
            totalPages: response.headers.get('X-WP-TotalPages'),
            json: response.json()
          }; // returns value to next .then()
        },
        (error) => {
          console.log('Fetch error occured', error);
          dispatch(createAvailabilityFailure(error));
        }
      ).then(
        (response2) => {
          // resolve response.json()
          response2.json.then((jsonData) => {
            dispatch(createAvailabilityReceive(
              {
                jsonData : jsonData,
                totalPages : response2.totalPages
              }
            ));
            dispatch(addNotification({
              copy: 'Success: Unavailability Added.',
              json: jsonData
            }));
            dispatch(getAvailability());
          });
        }
      );

  }
}
export function createAvailabilityRequest() {
  return {
    type: CREATE_AVAILABILITY_REQUEST,
    payload: {}
  };
}
export function createAvailabilityReceive(response) {
  return {
    type: CREATE_AVAILABILITY_RECEIVE,
    payload: {
      createAvailabilityData: response.jsonData,
      totalPages: response.totalPages
    }
  };
}
export function createAvailabilityFailure(error) {
  return {
    type: CREATE_AVAILABILITY_FAILURE,
    payload: {}
  };
}



export function deleteAvailability(data) {
  // Thunk
  return function(dispatch) {
    dispatch(deleteAvailabilityRequest());

    const url = _s_utilities.rest_url + 'booking/v1/availability';
    const options = {
      method: 'DELETE',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      },
      body: JSON.stringify(data)
    };

    return fetch(url, options)
      .then(
        (response) => {
          return {
            totalPages: response.headers.get('X-WP-TotalPages'),
            json: response.json()
          }; // returns value to next .then()
        },
        (error) => {
          console.log('Fetch error occured', error);
          dispatch(deleteAvailabilityFailure(error));
          dispatch(addNotification({
            copy: 'Error: Unavailability Not Added.',
            json: jsonData
          }));
        }
      ).then(
        (response2) => {
          // resolve response.json()
          response2.json.then((jsonData) => {
            dispatch(deleteAvailabilityReceive(
              {
                jsonData : jsonData,
                totalPages : response2.totalPages
              }
            ));
            dispatch(hideDialogConfirm());
            dispatch(addNotification({
              copy: 'Success: Unavailability Deleted.',
              json: jsonData
            }));
            dispatch(getAvailability());
          });
        }
      );

  }
}
export function deleteAvailabilityRequest() {
  return {
    type: DELETE_AVAILABILITY_REQUEST,
    payload: {}
  };
}
export function deleteAvailabilityReceive(response) {
  return {
    type: DELETE_AVAILABILITY_RECEIVE,
    payload: {
      deleteAvailabilityData: response.jsonData,
      postId: response.jsonData.post_id.ID,
      totalPages: response.totalPages
    }
  };
}
export function deleteAvailabilityFailure(error) {
  return {
    type: DELETE_AVAILABILITY_FAILURE,
    payload: {}
  };
}





export function getAbilities(data = {page: 1}) {
  // Thunk
  return function(dispatch) {
    dispatch(getAbilitiesRequest());

    const url = _s_utilities.rest_url + 'booking/v1/asset-abilities';
    const fullUrl = url + '?' + Utilities.buildQuery(data);
    const options = {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      }
    };

    return fetch(fullUrl, options)
      .then(
        (response) => {
          return {
            totalPages: response.headers.get('X-WP-TotalPages'),
            json: response.json()
          };
        },
        (error) => {
          console.log('Fetch error occured', error);
          dispatch(getAbilitiesFailure(error));
        }
      ).then(
        (response2) => {
          // resolve response.json()
          response2.json.then((jsonData) => {
            dispatch(getAbilitiesReceive(
              {
                jsonData : jsonData,
                page: data.page,
                totalPages : response2.totalPages
              }
            ));
          });
        }
      );

  }
}
export function getAbilitiesRequest() {
  return {
    type: GET_ABILITIES_REQUEST,
    payload: {}
  };
}
export function getAbilitiesReceive(response) {
  return {
    type: GET_ABILITIES_RECEIVE,
    payload: {
      abilitiesData: response.jsonData,
      page: response.page,
      totalPages: response.totalPages
    }
  };
}
export function getAbilitiesFailure() {
  return {
    type: GET_ABILITIES_FAILURE,
    payload: {}
  };
}



export function createAbilities(data = {}) {
  // Thunk
  return function(dispatch) {
    dispatch(createAbilitiesRequest());

    const url = _s_utilities.rest_url + 'booking/v1/asset-abilities';
    const options = {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      },
      body: JSON.stringify(data)
    };

    return fetch(url, options)
      .then(
        (response) => {
          return {
            totalPages: response.headers.get('X-WP-TotalPages'),
            json: response.json()
          }; // returns value to next .then()
        },
        (error) => {
          console.log('Fetch error occured', error);
          dispatch(createAbilitiesFailure(error));
          dispatch(addNotification({
            copy: 'Error: Abilitiy Not Added.',
            json: jsonData
          }));
        }
      ).then(
        (response2) => {
          // resolve response.json()
          response2.json.then((jsonData) => {
            dispatch(createAbilitiesReceive(
              {
                jsonData : jsonData,
                page: data.page,
                totalPages : response2.totalPages
              }
            ));
            dispatch(addNotification({
              copy: 'Success: Abilitiy Added.',
              json: jsonData
            }));
            dispatch(getAbilities());
          });
        }
      );

  }
}
export function createAbilitiesRequest() {
  return {
    type: CREATE_ABILITIES_REQUEST,
    payload: {}
  };
}
export function createAbilitiesReceive(response) {
  return {
    type: CREATE_ABILITIES_RECEIVE,
    payload: {
      createAbilitiesData: response.jsonData,
      page: response.page,
      totalPages: response.totalPages
    }
  };
}
export function createAbilitiesFailure() {
  return {
    type: CREATE_ABILITIES_FAILURE,
    payload: {}
  };
}



export function deleteAbilities(data = {}) {
  // Thunk
  return function(dispatch) {
    dispatch(deleteAbilitiesRequest());

    const url = _s_utilities.rest_url + 'booking/v1/asset-abilities';
    const options = {
      method: 'DELETE',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      },
      body: JSON.stringify(data)
    };

    return fetch(url, options)
      .then(
        (response) => {
          return {
            totalPages: response.headers.get('X-WP-TotalPages'),
            json: response.json()
          }; // returns value to next .then()
        },
        (error) => {
          console.log('Fetch error occured', error);
          dispatch(deleteAbilitiesFailure(error));
          dispatch(addNotification({
            copy: 'Error: Abilitiy Not Deleted.',
            json: jsonData
          }));
        }
      ).then(
        (response2) => {
          // resolve response.json()
          response2.json.then((jsonData) => {
            dispatch(deleteAbilitiesReceive(
              {
                jsonData : jsonData,
                page: data.page,
                totalPages : response2.totalPages
              }
            ));
            dispatch(hideDialogConfirm());
            dispatch(addNotification({
              copy: 'Success: Ability Deleted.',
              json: jsonData
            }));
            dispatch(getAbilities());
          });
        }
      );

  }
}
export function deleteAbilitiesRequest() {
  return {
    type: DELETE_ABILITIES_REQUEST,
    payload: {}
  };
}
export function deleteAbilitiesReceive(response) {
  return {
    type: DELETE_ABILITIES_RECEIVE,
    payload: {
      deleteAbilitiesData: response.jsonData,
      page: response.page,
      totalPages: response.totalPages
    }
  };
}
export function deleteAbilitiesFailure() {
  return {
    type: DELETE_ABILITIES_FAILURE,
    payload: {}
  };
}

import fetch from 'cross-fetch';
import Utilities from '../../Utilities';
import {
  GET_ASSET_AVAILABILITY,
  GET_ASSET_AVAILABILITY_REQUEST,
  GET_ASSET_AVAILABILITY_RECEIVE,
  GET_ASSET_AVAILABILITY_FAILURE,
  CREATE_ASSET_REQUEST,
  CREATE_ASSET_REQUEST_REQUEST,
  CREATE_ASSET_REQUEST_RECEIVE,
  CREATE_ASSET_REQUEST_FAILURE,
  DELETE_ASSET_REQUEST
} from './actionTypes';

import { getBookingRequests } from './actions-booking-requests';
import { hideDialogConfirm } from './actions-dialog-confirm';
import { addNotification } from './actions-notifications';

// @NOTE actions are functions that return plain objects with `type` and `payload` properties

/**
 * Get Asset Availability
 */
export function getAssetAvailability(data) {

  return function(dispatch) {
    dispatch(getAssetAvailabilityRequest());

    const url = _s_utilities.rest_url + 'booking/v1/assets';
    const fullUrl = url + '?' + Utilities.buildQuery(data);
    const options = {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      }
    };

    return fetch(fullUrl, options)
      .then(
        (response) => {
          return {
            totalPages: response.headers.get('X-WP-TotalPages'),
            json: response.json()
          }; // returns value to next .then()
        },
        (error) => {
          dispatch(getAssetAvailabilityFailure(error));
          dispatch(addNotification({
            copy : 'Error: Get Asset Availability.',
            json : error
          }));
        }
      ).then(
        (response) => {
          // resolve response.json()
          response.json.then((jsonData) => {
            dispatch(getAssetAvailabilityReceive({
              json : jsonData,
              totalPages : response.totalPages
            }));

            dispatch(addNotification({
              copy : 'Success: Get Asset Availability.',
              json : jsonData
            }));
          });
        }
      );
  }
}

export function getAssetAvailabilityRequest() {
  return {
    type: GET_ASSET_AVAILABILITY_REQUEST,
    payload: {}
  }
}

export function getAssetAvailabilityReceive(response) {
  return {
    type: GET_ASSET_AVAILABILITY_RECEIVE,
    payload: {
      assetData: response.json,
      totalPages: response.totalPages
    }
  }
}

export function getAssetAvailabilityFailure(error) {
  return {
    type: GET_ASSET_AVAILABILITY_FAILURE,
    payload: {}
  }
}



/**
 * Asset Requests
 */
export function createAssetRequest(data) {

  return function(dispatch) {
    dispatch(createAssetRequestRequest());

    const url = _s_utilities.rest_url + 'booking/v1/assets';
    const options = {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      },
      body: JSON.stringify(data)
    };

    return fetch(url, options)
      .then(
        (response) => {
          return response.json();
        },
        (error) => {
          dispatch(createAssetRequestFailure(error));
          dispatch(addNotification({
            copy: 'Error: Create Asset Request.',
            json: error
          }));
        }
      ).then(
        (jsonData) => {
          dispatch(createAssetRequestReceive(jsonData));
          dispatch(hideDialogConfirm());
          dispatch(addNotification({
            copy: 'Success: Create Asset Request.',
            json: jsonData
          }));
          dispatch(getBookingRequests());
        }
      );
  }

}

export function createAssetRequestRequest() {
  return {
    type: CREATE_ASSET_REQUEST_REQUEST,
    payload: {}
  }
}

export function createAssetRequestReceive(response) {
  return {
    type: CREATE_ASSET_REQUEST_RECEIVE,
    payload: {
      createAssetRequestData: response
    }
  }
}

export function createAssetRequestFailure(error) {
  return {
    type: CREATE_ASSET_REQUEST_FAILURE,
    payload: {}
  }
}

export function deleteAssetRequest() {
  return {
    type: DELETE_ASSET_REQUEST,
    payload: {}
  }
}

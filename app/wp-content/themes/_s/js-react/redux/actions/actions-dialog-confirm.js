import {
  SHOW_DIALOG_CONFIRM,
  HIDE_DIALOG_CONFIRM
} from './actionTypes';

// @NOTE actions are functions that return plain objects with `type` and `payload` properties
export function showDialogConfirm(data) {
  return {
    type: SHOW_DIALOG_CONFIRM,
    payload: {
      confirmAction: data.confirmAction,
      cancelAction: data.cancelAction,
      copy: data.copy,
      showDialog: data.showDialog
    }
  }
}

export function hideDialogConfirm() {
  return {
    type: HIDE_DIALOG_CONFIRM,
    payload: false
  }
}

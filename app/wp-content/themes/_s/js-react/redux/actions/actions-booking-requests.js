import fetch from 'cross-fetch';
import Utilities from '../../Utilities';
import {
  RESET_BOOKING_REQUESTS,
  GET_BOOKING_REQUESTS,
  GET_BOOKING_REQUESTS_REQUEST,
  GET_BOOKING_REQUESTS_RECEIVE,
  GET_BOOKING_REQUESTS_FAILURE,
  GET_BOOKING_REQUESTS_ALL,
  GET_BOOKING_REQUESTS_ALL_REQUEST,
  GET_BOOKING_REQUESTS_ALL_RECEIVE,
  GET_BOOKING_REQUESTS_ALL_FAILURE,
  ACCEPT_BOOKING_REQUEST,
  ACCEPT_BOOKING_REQUEST_REQUEST,
  ACCEPT_BOOKING_REQUEST_RECEIVE,
  ACCEPT_BOOKING_REQUEST_FAILURE,
  REJECT_BOOKING_REQUEST,
  REJECT_BOOKING_REQUEST_REQUEST,
  REJECT_BOOKING_REQUEST_RECEIVE,
  REJECT_BOOKING_REQUEST_FAILURE
} from './actionTypes';

import { getAvailability } from './actions-asset';
import { hideDialogConfirm } from './actions-dialog-confirm';
import { addNotification } from './actions-notifications';

// @NOTE actions are functions that return plain objects with `type` and `payload` properties
export function getBookingRequests(data = {page: 1}) {

  return function(dispatch) {
    dispatch(getBookingRequestsRequest());

    const url = _s_utilities.rest_url + 'booking/v1/requests';
    const fullUrl = url + '?' + Utilities.buildQuery(data);
    const options = {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      }
    };

    return fetch(fullUrl, options)
      .then(
        (response) => {
          return {
            totalPages: response.headers.get('X-WP-TotalPages'),
            json: response.json()
          }; // returns value to next .then()
        },
        (error) => {
          dispatch(getBookingRequestsFailure(error));
        }
      ).then(
        (response2) => {
          // resolve response.json()
          response2.json.then((jsonData) => {
            dispatch(getBookingRequestsReceive(
              {
                jsonData : jsonData,
                page: data.page,
                totalPages : response2.totalPages
              }
            ));
          });
        }
      );
  }

}

export function getBookingRequestsRequest() {
  return {
    type: GET_BOOKING_REQUESTS_REQUEST,
    payload: {}
  }
}

export function getBookingRequestsReceive(response) {
  return {
    type: GET_BOOKING_REQUESTS_RECEIVE,
    payload: {
      bookingRequests: response.jsonData,
      page: response.page,
      totalPages: response.totalPages
    }
  }
}

export function getBookingRequestsFailure(error) {
  return {
    type: GET_BOOKING_REQUESTS_FAILURE,
    payload: {}
  }
}


export function getBookingRequestsAll(data) {
  return function(dispatch) {
    dispatch(getBookingRequestsAllRequest());

    const url = _s_utilities.rest_url + 'booking/v1/requests';

    // fetch recursively
    const fetchPromise = Utilities.fetchGetRecursive(url, data);

    return fetchPromise.then(
      (response) => {
        dispatch(getBookingRequestsAllReceive(response));
      },
      (error) => {
        dispatch(getBookingRequestsAllFailure(error));
      }
    );
  }
}

export function getBookingRequestsAllRequest() {
  return {
    type: GET_BOOKING_REQUESTS_ALL_REQUEST,
    payload: {}
  }
}

export function getBookingRequestsAllReceive(response) {
  return {
    type: GET_BOOKING_REQUESTS_ALL_RECEIVE,
    payload: {
      bookingRequests: response
    }
  }
}

export function getBookingRequestsAllFailure(error) {
  return {
    type: GET_BOOKING_REQUESTS_ALL_FAILURE,
    payload: {}
  }
}




export function acceptBookingRequest(data = {}) {

  return function(dispatch) {
    dispatch(acceptBookingRequestRequest());

    const url = _s_utilities.rest_url + 'booking/v1/respond';
    const options = {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      },
      body: JSON.stringify(data)
    };

    return fetch(url, options)
      .then(
        (response) => {
          return {
            totalPages: response.headers.get('X-WP-TotalPages'),
            json: response.json()
          }; // returns value to next .then()
        },
        (error) => {
          console.log('Fetch error occured', error);
          dispatch(acceptBookingRequestFailure(error));
        }
      ).then(
        (response2) => {
          // resolve response.json()
          response2.json.then((jsonData) => {
            dispatch(acceptBookingRequestReceive(
              {
                jsonData : jsonData,
                totalPages : response2.totalPages
              }
            ));
            dispatch(hideDialogConfirm());
            dispatch(addNotification({
              copy: 'Success: Booking Request Accepted.',
              json: jsonData
            }));
            dispatch(getAvailability());
          });
        }
      );
  }

}

export function acceptBookingRequestRequest() {
  return {
    type: ACCEPT_BOOKING_REQUEST_REQUEST,
    payload: {}
  }
}

export function acceptBookingRequestReceive(response) {
  return {
    type: ACCEPT_BOOKING_REQUEST_RECEIVE,
    payload: {
      bookingRequest: response.jsonData,
      page: response.page,
      totalPages: response.totalPages
    }
  }
}

export function acceptBookingRequestFailure(error) {
  return {
    type: ACCEPT_BOOKING_REQUEST_FAILURE,
    payload: {}
  }
}


export function rejectBookingRequest(data = {}) {

  return function(dispatch) {
    dispatch(rejectBookingRequestRequest());

    const url = _s_utilities.rest_url + 'booking/v1/respond';
    const options = {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-WP-Nonce': _s_utilities.rest_nonce
      },
      body: JSON.stringify(data)
    };

    return fetch(url, options)
      .then(
        (response) => {
          return {
            totalPages: response.headers.get('X-WP-TotalPages'),
            json: response.json()
          }; // returns value to next .then()
        },
        (error) => {
          console.log('Fetch error occured', error);
          dispatch(rejectBookingRequestFailure(error));
        }
      ).then(
        (response2) => {
          // resolve response.json()
          response2.json.then((jsonData) => {
            dispatch(rejectBookingRequestReceive(
              {
                jsonData : jsonData,
                totalPages : response2.totalPages
              }
            ));
            dispatch(hideDialogConfirm());
            dispatch(addNotification({
              copy: 'Success: Booking Request Rejected.',
              json: jsonData
            }));
            dispatch(getAvailability());
          });
        }
      );
  }

}

export function rejectBookingRequestRequest() {
  return {
    type: REJECT_BOOKING_REQUEST_REQUEST,
    payload: {}
  }
}

export function rejectBookingRequestReceive(response) {
  return {
    type: REJECT_BOOKING_REQUEST_RECEIVE,
    payload: {
      bookingRequest: response.jsonData,
      page: response.page,
      totalPages: response.totalPages
    }
  }
}

export function rejectBookingRequestFailure(error) {
  return {
    type: REJECT_BOOKING_REQUEST_FAILURE,
    payload: {}
  }
}

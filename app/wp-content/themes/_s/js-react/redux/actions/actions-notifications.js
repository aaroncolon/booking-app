import {
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
} from './actionTypes';

// @NOTE actions are functions that return plain objects with `type` and `payload` properties
let _id = 0;
export function addNotification(data) {
  return {
    type: ADD_NOTIFICATION,
    payload: {
      id: _id++,
      copy: data.copy,
      json: data.json
    }
  }
}

export function removeNotification(id) {
  return {
    type: REMOVE_NOTIFICATION,
    payload: {
      id: id
    }
  };
}

import React from 'react';
import ReactDOM from 'react-dom';

// Components
import App from './App';

// Redux
import { Provider } from 'react-redux';
import store from './redux/stores/store';

const reactRoot = document.getElementById('react-root');

if (reactRoot) {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    reactRoot
  );
}

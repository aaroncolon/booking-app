import React from 'react';

// Components
import Notifications from './redux/components/Notifications';
import Requester from './redux/components/Requester';
import Asset from './redux/components/Asset';

class App extends React.Component {
  render() {
    const userType = _s_utilities.user_type;
    let view = null;

    switch (userType) {
      case 'requester':
        view = <Requester />;
        break;
      case 'asset':
        view = <Asset />;
        break;
      default:
        view = null;
        break;
    }

    return (
      <>
        <Notifications />
        {view}
      </>
    );
  }
}

export default App;

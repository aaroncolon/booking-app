<?php

/**
 * Verifies an ID against the Current User ID
 *
 * @param {String} $role a role
 * @param {Object} $user a WP User Object
 * @return {Boolean} true if match, false if no match
 */
function _s_is_user_role($role, $user = null) {
  $user = ($user) ? $user : wp_get_current_user();
  return in_array($role, $user->roles);
}

/**
 * Verifies an ID against the Current User ID
 *
 * @param {String} $id the ID
 * @return {String|Boolean} ID if match, false if no match
 */
function _s_verify_user_id($id) {
  $user_id = (int) sanitize_text_field($id);
  if (empty($user_id) || $user_id !== get_current_user_id()) {
    return false;
  }
  return $user_id;
}

/**
 * Adds Custom Post Types
 */
function _s_register_cpt() {
  $_s_asset_requests_labels = array(
    'name'               => _x( 'Asset Requests', 'post type general name', '_s-textdomain' ),
    'singular_name'      => _x( 'Asset Request', 'post type singular name', '_s-textdomain' ),
    'menu_name'          => _x( 'Asset Requests', 'admin menu', '_s-textdomain' ),
    'name_admin_bar'     => _x( 'Asset Request', 'add new on admin bar', '_s-textdomain' ),
    'add_new'            => _x( 'Add New', 'Asset Request', '_s-textdomain' ),
    'add_new_item'       => __( 'Add New Asset Request', '_s-textdomain' ),
    'new_item'           => __( 'New Asset Request', '_s-textdomain' ),
    'edit_item'          => __( 'Edit Asset Request', '_s-textdomain' ),
    'view_item'          => __( 'View Asset Request', '_s-textdomain' ),
    'all_items'          => __( 'All Asset Requests', '_s-textdomain' ),
    'search_items'       => __( 'Search Asset Requests', '_s-textdomain' ),
    'parent_item_colon'  => __( 'Parent Asset Requests:', '_s-textdomain' ),
    'not_found'          => __( 'No Asset Requests found.', '_s-textdomain' ),
    'not_found_in_trash' => __( 'No Asset Requests found in Trash.', '_s-textdomain' )
  );

  register_post_type('_s_asset_requests', array(
    'labels'          => $_s_asset_requests_labels,
    'description'     => '',
    'has_archive'     => true,
    'public'          => true,
    'show_ui'         => true,
    'show_in_menu'    => true,
    'menu_position'   => 7,
    'capability_type' => 'post',
    'map_meta_cap'    => true,
    'hierarchical'    => false,
    'rewrite'         => array('slug' => 'asset-requests', 'with_front' => false),
    'query_var'       => true,
    'supports'        => array('title', 'editor', 'author', 'thumbnail', 'custom-fields'),
    'taxonomies'      => array('_s_asset_types', '_s_experience_levels'),
    'show_in_rest'    => true,
  ));

  $_s_asset_schedules_labels = array(
    'name'               => _x( 'Asset Schedules', 'post type general name', '_s-textdomain' ),
    'singular_name'      => _x( 'Asset Schedule', 'post type singular name', '_s-textdomain' ),
    'menu_name'          => _x( 'Asset Schedules', 'admin menu', '_s-textdomain' ),
    'name_admin_bar'     => _x( 'Asset Schedule', 'add new on admin bar', '_s-textdomain' ),
    'add_new'            => _x( 'Add New', 'Asset Schedule', '_s-textdomain' ),
    'add_new_item'       => __( 'Add New Asset Schedule', '_s-textdomain' ),
    'new_item'           => __( 'New Asset Schedule', '_s-textdomain' ),
    'edit_item'          => __( 'Edit Asset Schedule', '_s-textdomain' ),
    'view_item'          => __( 'View Asset Schedule', '_s-textdomain' ),
    'all_items'          => __( 'All Asset Schedules', '_s-textdomain' ),
    'search_items'       => __( 'Search Asset Schedules', '_s-textdomain' ),
    'parent_item_colon'  => __( 'Parent Asset Schedules:', '_s-textdomain' ),
    'not_found'          => __( 'No Asset Schedules found.', '_s-textdomain' ),
    'not_found_in_trash' => __( 'No Asset Schedules found in Trash.', '_s-textdomain' )
  );

  register_post_type('_s_asset_schedules', array(
    'labels'          => $_s_asset_schedules_labels,
    'description'     => '',
    'has_archive'     => true,
    'public'          => true,
    'show_ui'         => true,
    'show_in_menu'    => true,
    'menu_position'   => 7,
    'capability_type' => 'post',
    'map_meta_cap'    => true,
    'hierarchical'    => false,
    'rewrite'         => array('slug' => 'asset-schedules', 'with_front' => false),
    'query_var'       => true,
    'supports'        => array('title', 'editor', 'author', 'thumbnail', 'custom-fields'),
    'taxonomies'      => array('_s_asset_types', '_s_experience_levels'),
    'show_in_rest'    => true,
  ));
} // _s_register_cpt()
add_action( 'init', '_s_register_cpt' );

function _s_register_asset_types_taxonomy() {
  $labels = array(
    'name' => 'Asset Types',
    'singular_name' => 'Asset Type',
    'search_items' => 'Search Asset Types',
    'all_items' => 'All Asset Types',
    'edit_item' => 'Edit Asset Type',
    'update_item' => 'Update Asset Type',
    'add_new_item' => 'Add New Asset Type',
    'new_item_name' => 'New Asset Type Name'
  );

  $args = array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true
  );

  register_taxonomy( '_s_asset_types', array('_s_asset_requests', '_s_asset_schedules'), $args );
}
add_action('init', '_s_register_asset_types_taxonomy');

function _s_register_experience_levels_taxonomy() {
  $labels = array(
    'name' => 'Experience Levels',
    'singular_name' => 'Experience Level',
    'search_items' =>  'Search Experience Levels',
    'all_items' => 'All Experience Levels',
    'edit_item' => 'Edit Experience Level',
    'update_item' => 'Update Experience Level',
    'add_new_item' => 'Add New Experience Level',
    'new_item_name' => 'New Experience Level Name'
  );

  $args = array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true
  );

  register_taxonomy( '_s_experience_levels', array('_s_asset_requests', '_s_asset_schedules'), $args );
}
add_action('init', '_s_register_experience_levels_taxonomy');


// function _s_register_user_asset_types_taxonomy() {
//     $labels = array(
//       'name' => 'User Asset Types',
//       'singular_name' => 'User Asset Type',
//       'search_items' =>  'Search User Asset Types',
//       'all_items' => 'All User Asset Types',
//       'edit_item' => 'Edit User Asset Type',
//       'update_item' => 'Update User Asset Type',
//       'add_new_item' => 'Add New User Asset Type',
//       'new_item_name' => 'New User Asset Type Name'
//     );

//     $args = array(
//       'hierarchical' => true,
//       'labels' => $labels,
//       'show_ui' => true,
//       'show_admin_column' => true,
//       'update_count_callback' => function() { return; } 
//     );

//     register_taxonomy('_s_user_asset_types', 'user', $args);
// }
// add_action('init', '_s_register_experience_levels_taxonomy');


function _s_user_registration_save( $user_id ) {
  update_user_meta($user_id, '_re_last_booked', time());
}
add_action('user_register', '_s_user_registration_save', 10, 1);



function _s_do_get_booking_requests() {
  // Build Query
  $args = array(
    'post_type' => '_s_asset_requests',
    'order'     => 'ASC',
    'orderby'   => 'meta_value_num',
    'meta_key'  => 're_date_start_unix',
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key'     => 're_asset_id',
        'value'   => get_current_user_id(),
        'compare' => '=',
        'type'    => 'NUMERIC'
      )
    )
  );

  $booking_q = new WP_Query($args);

  if ($booking_q->have_posts()) :
    while ($booking_q->have_posts()) : $booking_q->the_post();
  ?>
    <tr>
      <td>
        <?php esc_html_e(get_post_meta(get_the_ID(), 're_asset_type', true)) ?>
      </td>
      <td>
        <?php esc_html_e(get_post_meta(get_the_ID(), 're_experience_level', true)) ?>
      </td>
      <td>
        <?php esc_html_e(get_post_meta(get_the_ID(), 're_date_start', true)) ?>
      </td>
      <td>
        <?php esc_html_e(get_post_meta(get_the_ID(), 're_date_end', true)) ?>
      </td>
      <td>
        <?php esc_html_e(get_post_meta(get_the_ID(), 're_booking_status', true)) ?>
      </td>
      <td>
        <button id="btn-accept-<?php esc_attr_e(get_the_ID()) ?>" data-post-id="<?php esc_attr_e(get_the_ID()) ?>" data-action="accept">Accept</button>
        <button id="btn-reject-<?php esc_attr_e(get_the_ID()) ?>" data-post-id="<?php esc_attr_e(get_the_ID()) ?>" data-action="reject">Reject</button>
      </td>
    </tr>
<?php
    endwhile;
    wp_reset_postdata();
  else:
    var_dump('No Booking Requests found.');
  endif;
}

function _s_register_rest_custom_fields() {
  $object_type = 'post'; // custom post types are of type post
  $meta_args_asset_type = array(
    'object_subtype' => '_s_asset_requests',
    // Validate and sanitize the meta value.
    // The type of data associated with this meta key. Valid values are 'string', 'boolean', 'integer', 'number', 'array', and 'object'.
    'type' => 'string',
    'description' => 'Asset Type',
    'single' => true,
    // 'sanitize_callback' => '',
    // 'auth_callback' => '',
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_asset_type', $meta_args_asset_type );

  $meta_args_asset_email = array(
    'object_subtype' => '_s_asset_requests',
    'type' => 'string',
    'description' => 'Asset Email',
    'single' => true,
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_asset_email', $meta_args_asset_email );

  $meta_args_asset_id = array(
    'object_subtype' => '_s_asset_requests',
    'type' => 'number',
    'description' => 'Asset ID',
    'single' => true,
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_asset_id', $meta_args_asset_id );

  $meta_args_exp_level = array(
    'object_subtype' => '_s_asset_requests',
    'type' => 'string',
    'description' => 'Experience Level',
    'single' => true,
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_experience_level', $meta_args_exp_level );

  $meta_args_date_start = array(
    'object_subtype' => '_s_asset_requests',
    'type' => 'string',
    'description' => 'Date Start',
    'single' => true,
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_date_start', $meta_args_date_start );

  $meta_args_date_end = array(
    'object_subtype' => '_s_asset_requests',
    'type' => 'string',
    'description' => 'Date End',
    'single' => true,
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_date_end', $meta_args_date_end );

  $meta_args_date_start_unix = array(
    'object_subtype' => '_s_asset_requests',
    'type' => 'number',
    'description' => 'Date Start Unix',
    'single' => true,
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_date_start_unix', $meta_args_date_start_unix );

  $meta_args_date_end_unix = array(
    'object_subtype' => '_s_asset_requests',
    'type' => 'number',
    'description' => 'Date End Unix',
    'single' => true,
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_date_end_unix', $meta_args_date_end_unix );

  $meta_args_timestamp = array(
    'object_subtype' => '_s_asset_requests',
    'type' => 'number',
    'description' => 'Timestamp',
    'single' => true,
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_timestamp', $meta_args_timestamp );

  $meta_args_booking_status = array(
    'object_subtype' => '_s_asset_requests',
    'type' => 'string',
    'description' => 'Booking Status',
    'single' => true,
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_booking_status', $meta_args_booking_status );

  $meta_args_booking_status_code = array(
    'object_subtype' => '_s_asset_requests',
    'type' => 'number',
    'description' => 'Booking Status Code',
    'single' => true,
    'show_in_rest' => true,
  );
  register_meta( $object_type, 're_booking_status_code', $meta_args_booking_status_code );
}
add_action( 'rest_api_init', '_s_register_rest_custom_fields' );

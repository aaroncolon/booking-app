<?php
/**
 * Send request asset email to asset
 */
function _s_send_request_asset_email($data) {
  // Get details
  $asset_type      = $data['asset_type'];
  $exp_level       = $data['experience_level']; 
  $date_start      = $data['date_start'];
  $date_end        = $data['date_end'];
  $post_id         = $data['post_id'];
  $asset_id        = (int) $data['asset_id'];
  $asset_email     = get_the_author_meta('user_email', $asset_id);
  if (!empty($data['requester_email'])) {
    $requester_email = $data['requester_email'];
  } else {
    $requester_id    = get_post_field('post_author', $post_id);
    $requester_email = get_the_author_meta('user_email', $requester_id);
  }

  // Build booking URLs
  $url_accept = http_build_query( array(
    'action' => 'accept',
    'post_id' => $post_id,
    'user_id' => $asset_id,
    // 'nonce'   => $nonce_accept
  ) );
  $url_reject = http_build_query( array(
    'action' => 'reject',
    'post_id' => $post_id,
    'user_id' => $asset_id,
    // 'nonce'   => $nonce_reject
  ) );
  $url_accept = get_site_url() .'/get-request/?'. $url_accept;  
  $url_reject = get_site_url() .'/get-request/?'. $url_reject;

  // Build Email
  $to      = $asset_email; // asset email
  $from    = 'dev@aaroncolon.net'; // @TODO Postmark email address
  $headers = array('Content-type: text/html; charset=utf-8;');
  $message = file_get_contents(locate_template('email-templates/asset-request-notification.html'));
  if (!$message) {
    return $message;
  }
  $search = array(
    '%asset_email%',
    '%requester_email%',
    '%date_start%',
    '%date_end%',
    '%asset_type%',
    '%exp_level%',
    '%url_accept%',
    '%url_reject%'
  );
  $replace = array(
    esc_html($asset_email),
    esc_html($requester_email),
    esc_html($date_start),
    esc_html($date_end),
    esc_html($asset_type),
    esc_html($exp_level),
    esc_html($url_accept),
    esc_html($url_reject)
  );
  $message_final = str_replace($search, $replace, $message);

  // $message = "test message!";
  return wp_mail($to, $from, $message_final, $headers);
  
  // return array(
  //   'asset_type' => $asset_type,
  //   'exp_level' => $exp_level,
  //   'date_start' => $date_start,
  //   'date_end' => $date_end,
  //   'post_id' => $post_id,
  //   'nonce_a' => $nonce_accept,
  //   'nonce_r' => $nonce_reject,
  //   'asset_id' => $asset_id,
  //   'asset_email' => $asset_email,
  //   'requester_name' => $requester_name,
  //   'url_accept' => $url_accept,
  //   'url_reject' => $url_reject,
  //   'to' => $to,
  //   'from' => $from,
  //   'headers' => $headers,
  //   'message' => $message,
  //  );
}

/**
 * Send request asset confirmation email to requester
 */
function _s_send_request_asset_email_confirmation($data) {
  // Get details
  $asset_type      = $data['asset_type'];
  $exp_level       = $data['experience_level']; 
  $date_start      = $data['date_start'];
  $date_end        = $data['date_end'];
  $post_id         = $data['post_id'];
  $requester_email = $data['requester_email'];

  // Build Email
  $to      = $requester_email; // requester email
  $from    = 'wordpress@aaroncolon.net'; // @TODO site notification address
  $headers = array('Content-type: text/html; charset=utf-8;');
  $message = file_get_contents(locate_template('email-templates/asset-request-received-confirmation.html'));

  if (!$message) {
    return $message;
  }

  $search = array(
    '%requester_email%',
    '%date_start%',
    '%date_end%',
    '%asset_type%',
    '%exp_level%'
  );
  $replace = array(
    esc_html($requester_email),
    esc_html($date_start),
    esc_html($date_end),
    esc_html($asset_type),
    esc_html($exp_level)
  );
  $message_final = str_replace($search, $replace, $message);

  return wp_mail($to, $from, $message_final, $headers);
}

function _s_send_accept_booking_email_asset($data) {
  // Get details
  $asset_type      = $data['asset_type'];
  $exp_level       = $data['asset_exp']; 
  $date_start      = $data['date_start'];
  $date_end        = $data['date_end'];
  $post_id         = $data['post_id'];
  $asset_id        = (int) $data['asset_id'];
  $asset_email     = get_the_author_meta('user_email', $asset_id);
  if (!empty($data['requester_email'])) {
    $requester_email = $data['requester_email'];
  } else {
    $requester_id    = get_post_field('post_author', $post_id);
    $requester_email = get_the_author_meta('user_email', $requester_id);
  }

  // Build Email
  $to      = $asset_email;
  $from    = 'wordpress@aaroncolon.net'; // @TODO site notification address
  $headers = array('Content-type: text/html; charset=utf-8;');
  $message = file_get_contents(locate_template('email-templates/asset-accept-booking.html'));
  if (!$message) {
    return $message;
  }
  $search = array(
    '%asset_email%',
    '%requester_email%',
    '%date_start%',
    '%date_end%',
    '%asset_type%',
    '%exp_level%',
    '%post_id%'
  );
  $replace = array(
    esc_html($asset_email),
    esc_html($requester_email),
    esc_html($date_start),
    esc_html($date_end),
    esc_html($asset_type),
    esc_html($exp_level),
    esc_html($post_id)
  );
  $message_final = str_replace($search, $replace, $message);

  return wp_mail($to, $from, $message_final, $headers);
}

function _s_send_reject_booking_email_asset($data) {
  // Get details
  $asset_type      = $data['asset_type'];
  $exp_level       = $data['asset_exp']; 
  $date_start      = $data['date_start'];
  $date_end        = $data['date_end'];
  $post_id         = $data['post_id'];
  $asset_id        = (int) $data['asset_id'];
  $asset_email     = get_the_author_meta('user_email', $asset_id);
  if (!empty($data['requester_email'])) {
    $requester_email = $data['requester_email'];
  } else {
    $requester_id    = get_post_field('post_author', $post_id);
    $requester_email = get_the_author_meta('user_email', $requester_id);
  }

  // Build Email
  $to      = $asset_email;
  $from    = 'wordpress@aaroncolon.net'; // @TODO site notification address
  $headers = array('Content-type: text/html; charset=utf-8;');
  $message = file_get_contents(locate_template('email-templates/asset-reject-booking.html'));
  if (!$message) {
    return $message;
  }
  $search = array(
    '%asset_email%',
    '%date_start%',
    '%date_end%',
    '%asset_type%',
    '%exp_level%',
    '%post_id%'
  );
  $replace = array(
    esc_html($asset_email),
    esc_html($date_start),
    esc_html($date_end),
    esc_html($asset_type),
    esc_html($exp_level),
    esc_html($post_id)
  );
  $message_final = str_replace($search, $replace, $message);

  return wp_mail($to, $from, $message_final, $headers);
}

function _s_send_accept_booking_email_requester($data) {
  // Get details
  $asset_type      = $data['asset_type'];
  $exp_level       = $data['asset_exp']; 
  $date_start      = $data['date_start'];
  $date_end        = $data['date_end'];
  $post_id         = $data['post_id'];
  $asset_id        = (int) $data['asset_id'];
  $asset_email     = get_the_author_meta('user_email', $asset_id);
  if (!empty($data['requester_email'])) {
    $requester_email = $data['requester_email'];
  } else {
    $requester_id    = get_post_field('post_author', $post_id);
    $requester_email = get_the_author_meta('user_email', $requester_id);
  }

  // Build Email
  $to      = $requester_email;
  $from    = 'wordpress@aaroncolon.net'; // @TODO site notification address
  $headers = array('Content-type: text/html; charset=utf-8;');
  $message = file_get_contents(locate_template('email-templates/requester-accept-booking.html'));

  if (!$message) {
    return $message;
  }

  $search = array(
    '%requester_email%',
    '%asset_email%',
    '%date_start%',
    '%date_end%',
    '%asset_type%',
    '%exp_level%',
    '%post_id%'
  );
  $replace = array(
    esc_html($requester_email),
    esc_html($asset_email),
    esc_html($date_start),
    esc_html($date_end),
    esc_html($asset_type),
    esc_html($exp_level),
    esc_html($post_id)
  );
  $message_final = str_replace($search, $replace, $message);

  return wp_mail($to, $from, $message_final, $headers);
}

function _s_send_reject_booking_email_requester($data) {

}

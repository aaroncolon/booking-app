<?php
/**
 * Get user booking requests
 */
function _s_get_user_booking_requests_handler() {
  // Verify nonce
  check_ajax_referer('nonce_booking_requests_get', 'nonce');

  $booking_data = array();
  $paged        = (!empty($_POST['paged'])) ? intval($_POST['paged']) : 1;
  $user_id      = get_current_user_id();

  // Build Query
  $args = array(
    'post_type' => '_s_asset_requests',
    'posts_per_page' => 10,
    'paged'     => $paged,
    'order'     => 'ASC',
    'orderby'   => 'meta_value_num',
    'meta_key'  => 're_date_start_unix',
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key'     => 're_asset_id',
        'value'   => $user_id,
        'compare' => '=',
        'type'    => 'NUMERIC'
      )
    )
  );

  // Query
  $q = new WP_Query($args);

  if ($q->have_posts()) :
    while ($q->have_posts()) : $q->the_post();
      // Prepare Data
      $booking_data[] = array(
        'asset_type'          => get_post_meta(get_the_ID(), 're_asset_type', true),
        'experience_level'    => get_post_meta(get_the_ID(), 're_experience_level', true),
        'date_start'          => get_post_meta(get_the_ID(), 're_date_start', true),
        'date_end'            => get_post_meta(get_the_ID(), 're_date_end', true),
        'booking_status'      => get_post_meta(get_the_ID(), 're_booking_status', true),
        'booking_status_code' => get_post_meta(get_the_ID(), 're_booking_status_code', true),
        'post_id'             => get_the_ID(),
        'user_id'             => $user_id,
        'nonce_accept'        => wp_create_nonce('nonce_booking_request_accept_'. get_the_ID() .'_'. $user_id),
        'nonce_reject'        => wp_create_nonce('nonce_booking_request_reject_'. get_the_ID() .'_'. $user_id),
      );
    endwhile;
    wp_reset_postdata();
  endif;

  // Result
  $res = array();
  $res['booking_data']  = $booking_data;
  $res['paged']         = $paged;
  $res['max_num_pages'] = $q->max_num_pages;

  wp_send_json_success($res);
}
add_action('wp_ajax__s_get_user_booking_requests_event', '_s_get_user_booking_requests_handler');
add_action('wp_ajax_nopriv__s_get_user_booking_requests_event', '_s_get_user_booking_requests_handler');


/**
 * Accept a booking request
 */
function _s_booking_request_accept_handler() {
  // Verify nonce
  check_ajax_referer('nonce_booking_request_accept', 'nonce');

  // Verify User ID
  $user_id = _s_verify_user_id($_POST['user_id']);
  if ($user_id === false) {
    wp_send_json_error();
  }

  // Sanitize Data
  $post_id    = sanitize_text_field($_POST['post_id']);
  $date_start = sanitize_text_field($_POST['date_start']);
  $date_end   = sanitize_text_field($_POST['date_end']);
  $timestamp  = time();

  // Get Asset Request data
  $asset_req  = get_post_meta($post_id);
  $asset_type = $asset_req['re_asset_type'][0];
  $asset_exp  = $asset_req['re_experience_level'][0];

  // @TODO Validate Data

  // Accept booking request
  $accept_booking = _s_accept_booking_request($post_id, $user_id, $timestamp);

  // Set Asset Availability
  $set_availability = _s_set_availability($date_start, $date_end, $post_id);

  // Update Asset _re_last_booked
  $update_last_booked = update_user_meta($user_id, '_re_last_booked', $timestamp);

  // Reject any conflicting requests
  $reject_conflicts = _s_reject_booking_request_conflicts($date_start, $date_end, $user_id);

  // Send Notifications
  $data_ = array(
    'post_id'    => $post_id,
    'asset_id'   => $user_id,
    'asset_type' => $asset_type,
    'asset_exp'  => $asset_exp,
    'date_start' => $date_start,
    'date_end'   => $date_end,
  );
  $email_asset     = _s_send_accept_booking_email_asset($data_);
  $email_requester = _s_send_accept_booking_email_requester($data_);

  $res = array();
  $res['booking_status']         = get_post_meta($post_id, 're_booking_status', true);
  $res['booking_status_code']    = get_post_meta($post_id, 're_booking_status_code', true);
  $res['asset_booked']           = get_post_meta($post_id, 're_asset_booked', true);
  $res['post_meta']              = get_post_meta($post_id);
  $res['update_last_booked_res'] = $update_last_booked;
  $res['set_availability_res']   = $set_availability;
  $res['accept_booking_res']     = $accept_booking;
  $res['reject_conflicts']       = $reject_conflicts;

  wp_send_json_success($res);
}
add_action('wp_ajax__s_booking_request_accept_event', '_s_booking_request_accept_handler');
add_action('wp_ajax_nopriv__s_booking_request_accept_event', '_s_booking_request_accept_handler');


/**
 * Reject a booking request
 */
function _s_booking_request_reject_handler() {
  // Verify nonce
  check_ajax_referer('nonce_booking_request_reject', 'nonce');

  // Verify User ID
  $user_id = _s_verify_user_id($_POST['user_id']);
  if ($user_id === false) {
    wp_send_json_error();
  }

  // Sanitize data
  $post_id = sanitize_text_field($_POST['post_id']);

  // Get Asset Request data
  $asset_req  = get_post_meta($post_id);
  $asset_type = $asset_req['re_asset_type'][0];
  $asset_exp  = $asset_req['re_experience_level'][0];
  $date_start = $asset_req['re_date_start'][0];
  $date_end   = $asset_req['re_date_end'][0];

  // Reject booking request
  $reject_booking = _s_reject_booking_request($post_id, $user_id);

  // Trigger _s_cron_get_next_available_asset()
  $next_available = _s_cron_get_next_available_asset($post_id);

  // Send Notifications
  $data_ = array(
    'post_id'    => $post_id,
    'asset_id'   => $user_id,
    'asset_type' => $asset_type,
    'asset_exp'  => $asset_exp,
    'date_start' => $date_start,
    'date_end'   => $date_end,
  );
  $email_asset = _s_send_reject_booking_email_asset($data_);

  $res = array();
  $res['booking_status']      = get_post_meta($post_id, 're_booking_status', true);
  $res['booking_status_code'] = get_post_meta($post_id, 're_booking_status_code', true);
  $res['asset_booked']        = get_post_meta($post_id, 're_asset_booked', true);
  $res['post_meta']           = get_post_meta($post_id);
  $res['reject_booking_res']  = $reject_booking;
  $res['next_available_res']  = $next_available;
  $res['email_asset_res']     = $email_asset;

  wp_send_json_success($res);
}
add_action('wp_ajax__s_booking_request_reject_event', '_s_booking_request_reject_handler');
add_action('wp_ajax_nopriv__s_booking_request_reject_event', '_s_booking_request_reject_handler');


/**
 * Accept a booking request via URL
 * 
 * Requires user login.
 */
function _s_booking_request_link_accept_handler() {
  // @TODO require user login on the page_get-request.php

  // Verify User ID
  $user_id = _s_verify_user_id($_POST['user_id']);
  if ($user_id === false) {
    wp_send_json_error();
  }

  // Sanitize Data
  $post_id   = sanitize_text_field($_POST['post_id']);
  $timestamp = time();
  
  // Accept booking request
  $accept_booking = _s_accept_booking_request($post_id, $user_id, $timestamp);

  // Set Asset Availability
  $set_availability = _s_set_availability($date_start, $date_end, $post_id);

  // Update Asset _re_last_booked
  $update_last_booked = update_user_meta($user_id, '_re_last_booked', $timestamp);

  // Reject any conflicting requests
  _s_reject_booking_request_conflicts($date_start, $date_end, $user_id);

  $res = array();
  $res['booking_status']         = get_post_meta($post_id, 're_booking_status', true);
  $res['booking_status_code']    = get_post_meta($post_id, 're_booking_status_code', true);
  $res['asset_booked']           = get_post_meta($post_id, 're_asset_booked', true);
  $res['post_meta']              = get_post_meta($post_id);
  $res['accept_booking_res']     = $accept_booking;
  $res['set_availability_res']   = $set_availability;
  $res['update_last_booked_res'] = $update_last_booked;

  wp_send_json_success($res);
}
add_action('wp_ajax__s_booking_request_link_accept_event', '_s_booking_request_link_accept_handler');
add_action('wp_ajax_nopriv__s_booking_request_link_accept_event', '_s_booking_request_link_accept_handler');


/**
 * Reject a booking request via URL
 */
function _s_booking_request_link_reject_handler() {
  // @TODO require user login on the page_get-request.php

  // Verify User ID
  $user_id = _s_verify_user_id($_POST['user_id']);
  if ($user_id === false) {
    wp_send_json_error();
  }

  // Sanitize Data
  $post_id = sanitize_text_field($_POST['post_id']);

  // Reject booking
  $reject_booking = _s_reject_booking_request($post_id, $user_id);

  $res = array();
  $res['booking_status']      = get_post_meta($post_id, 're_booking_status', true);
  $res['booking_status_code'] = get_post_meta($post_id, 're_booking_status_code', true);
  $res['asset_booked']        = get_post_meta($post_id, 're_asset_booked', true);
  $res['post_meta']           = get_post_meta($post_id);
  $res['reject_booking_res']  = $reject_booking;

  wp_send_json_success($res);
}
add_action('wp_ajax__s_booking_request_link_reject_event', '_s_booking_request_link_reject_handler');
add_action('wp_ajax_nopriv__s_booking_request_link_reject_event', '_s_booking_request_link_reject_handler');

















/**
 * Get availability posts
 */
function _s_get_availability_handler() {
  // Verify nonce
  check_ajax_referer('nonce_availability', 'nonce');

  $availability_data = array();
  $paged             = (!empty($_POST['paged'])) ? intval($_POST['paged']) : 1;

  // Build Query
  $args = array(
    'post_type' => '_s_asset_schedules',
    'posts_per_page' => 10,
    'paged'     => $paged,
    'author'    => get_current_user_id(),
    'order'     => 'ASC',
    'orderby'   => 'meta_value_num',
    'meta_key'  => 're_date_start_unix'
  );

  $q = new WP_Query($args);

  if ($q->have_posts()) :
    while ($q->have_posts()) : $q->the_post();
      $availability_data[] = array(
        'date_start'       => get_post_meta(get_the_ID(), 're_date_start', true),
        'date_end'         => get_post_meta(get_the_ID(), 're_date_end', true),
        'asset_request_id' => get_post_meta(get_the_ID(), 're_asset_request_id', true),
        'post_id'          => get_the_ID(),
      );
    endwhile;
    wp_reset_postdata();
  endif;

  $res = array();
  $res['availability_data'] = $availability_data;
  $res['paged']             = $paged;
  $res['max_num_pages']     = $q->max_num_pages;

  wp_send_json_success($res);
}
add_action('wp_ajax__s_get_availability_event', '_s_get_availability_handler');
add_action('wp_ajax_nopriv__s_get_availability_event', '_s_get_availability_handler');


/**
 * Create an availability post
 */
function _s_set_availability_handler() {
  // Verify nonce
  check_ajax_referer('nonce_availability', 'nonce');

  // Validate input data
  $date_start = sanitize_text_field($_POST['date_start']);
  $date_end   = sanitize_text_field($_POST['date_end']);
  if (empty($date_start) || empty($date_end)) {
    wp_send_json_error();
  }
  
  // Create the post
  $post_id = _s_set_availability($date_start, $date_end);

  // Verify post insertion
  if (is_wp_error($post_id)) {
    wp_send_json_error('Unable to complete request. Please try again.');
  }

  // Build result
  $result = array();
  $result['post_id']    = $post_id;
  $result['date_start'] = $date_start;
  $result['date_end']   = $date_end;

  wp_send_json_success($result);
}
add_action('wp_ajax__s_set_availability_event', '_s_set_availability_handler');
add_action('wp_ajax_nopriv__s_set_availability_event', '_s_set_availability_handler');


/**
 * Delete an availability post
 */
function _s_delete_availability_handler() {
  // Verify nonce
  check_ajax_referer('nonce_availability', 'nonce');

  // Validate input data
  $post_id = sanitize_text_field($_POST['post_id']);

  // Delete the post
  $post_result = _s_delete_availability($post_id);

  // Verify post insertion
  if (!$post_result) {
    wp_send_json_error('Unable to complete request. Please try again.');
  }

  // Build result
  $result = array();
  $result['post_id'] = $post_result;
              
  wp_send_json_success($result);
}
add_action('wp_ajax__s_delete_availability_event', '_s_delete_availability_handler');
add_action('wp_ajax_nopriv__s_delete_availability_event', '_s_delete_availability_handler');





/**
 * Add user Asset Type
 */
function _s_add_user_asset_type_handler() {
  // Verify nonce
  check_ajax_referer('nonce_user_asset_types', 'nonce');

  $result = array();
  $add    = null;

  $asset_type     = sanitize_text_field($_POST['asset_type']);
  $exp_level      = sanitize_text_field($_POST['experience_level']);
  $asset_type_exp = array($asset_type, $exp_level);
  // $asset_type_exp = implode('-', array($asset_type, $exp_level));

  // Manually check if value already exists
  $user_asset_types = get_user_meta(get_current_user_id(), '_re_asset_type', false);
  if ( (! $user_asset_types) || ($user_asset_types && ! in_array($asset_type_exp, $user_asset_types, true)) ) {
    // @NOTE adds duplicate key with unique value. when retrieved with get_user_meta, an array is returned
    $add = add_user_meta(get_current_user_id(), '_re_asset_type', $asset_type_exp, false);

    if (!$add) {
      wp_send_json_error('Add user meta failed.');
    }

    // Update existing user Asset Schedule posts
    _s_update_user_asset_schedules();

    $result['add_result'] = $add;
  }

  $result['add_result'] = $add;
  
  wp_send_json_success($result);
}
add_action('wp_ajax__s_add_user_asset_type_event', '_s_add_user_asset_type_handler');
add_action('wp_ajax_nopriv__s_add_user_asset_type_event', '_s_add_user_asset_type_handler');

/**
 * Delete user Asset Type
 */
function _s_delete_user_asset_type_handler() {
  // Verify nonce
  check_ajax_referer('nonce_user_asset_types', 'nonce');

  $result = array();

  $asset_type     = sanitize_text_field($_POST['asset_type']);
  $exp_level      = sanitize_text_field($_POST['experience_level']);
  $asset_type_exp = array($asset_type, $exp_level);

  $delete = delete_user_meta(get_current_user_id(), '_re_asset_type', $asset_type_exp);

  if (!$delete) {
    wp_send_json_error('Delete user meta failed.');
  }

  // Update existing user Asset Schedule posts
  _s_update_user_asset_schedules();

  $result['delete_res'] = $delete;

  wp_send_json_success($result);
}
add_action('wp_ajax__s_delete_user_asset_type_event', '_s_delete_user_asset_type_handler');
add_action('wp_ajax_nopriv__s_delete_user_asset_type_event', '_s_delete_user_asset_type_handler');

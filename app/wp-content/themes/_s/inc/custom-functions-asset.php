<?php
/**
 * Create a Schedule Post
 *
 * @param {String} $date_start       the start date YYYY-MM-DD
 * @param {String} $date_end         the end date YYYY-MM-DD
 * @param {String} $asset_request_id the ID of the asset request
 *
 * @return {Int} Post ID on success || 0 on failure
 */
function _s_set_availability($date_start, $date_end, $asset_request_id = null) {
  // Validate input data
  $date_start      = $date_start;
  $date_end        = $date_end;
  $date_start_unix = strtotime($date_start);
  $date_end_unix   = strtotime($date_end);
  $timestamp       = time();

  // Current User Data
  $user_asset_types = get_user_meta(get_current_user_id(), '_re_asset_type', false);
  
  // Get IDs of _s_asset_types terms
  if ($user_asset_types) {
    $user_asset_types_ids = array_map( function($v) { 
      $asset_type_exp         = $v[0] .'-'. $v[1];
      $asset_type_exp_term_id = term_exists($asset_type_exp, '_s_asset_types');
      $asset_type_exp_term_id = $asset_type_exp_term_id['term_id'];
      return $asset_type_exp_term_id;
    }, $user_asset_types );
  } else {
    $user_asset_types_ids = array();
  }
  
  // Gather the data to create the Post
  $post_data = array(
    'post_type'    => '_s_asset_schedules',
    'post_title'   => $date_start .' to '. $date_end,
    'post_content' => $timestamp,
    'post_status'  => 'draft', // 'publish' requires post content
    'tax_input'    => array( 
      '_s_asset_types' => $user_asset_types_ids,
    ),
    'meta_input' => array(
      're_date_start'       => $date_start,
      're_date_end'         => $date_end,
      're_date_start_unix'  => $date_start_unix,
      're_date_end_unix'    => $date_end_unix,
      're_timestamp'        => $timestamp,
      're_asset_request_id' => $asset_request_id,
    ),
    'comment_status' => 'closed',
    'ping_status'    => 'closed',
  );

  // Add the Asset Request
  return wp_insert_post($post_data, true);
}

/**
 * Delete a Schedule Post
 *
 * @param {Int} $post_id the Post ID to delete
 * @return {Array|Boolean} Post Object on success, false on failure
 */
function _s_delete_availability($post_id) {
  $post_id = (int) $post_id;
  return wp_delete_post($post_id, true);
}








/**
 * Accept Booking Request
 *
 * @param {Int} $post_id 
 * @param {Int} $user_id 
 * @param {String} $timestamp
 * @return {Boolean} true on success, false on failure
 */
function _s_accept_booking_request($post_id, $user_id = null, $timestamp = null) {
  $user_id         = ($user_id) ? $user_id : get_current_user_id();
  $timestamp       = ($timestamp) ? $timestamp : time();
  $asset_responses = get_post_meta($post_id, 're_asset_responses', true);
  $asset_responses = ($asset_responses) ? $asset_responses : array();
  $asset_responses[] = array(
    'asset_id'  => $user_id, 
    'response'  => 1, // 0: reject, 1: accept
    'timestamp' => $timestamp
  );

  // Update Meta Fields
  $booking_status      = update_post_meta($post_id, 're_booking_status', 'booked');
  $booking_status_code = update_post_meta($post_id, 're_booking_status_code', 2);
  $asset_responses     = update_post_meta($post_id, 're_asset_responses', $asset_responses);
  $asset_booked        = update_post_meta($post_id, 're_asset_booked', $user_id);
  $asset_queued        = update_post_meta($post_id, 're_asset_queued', null);

  return ($booking_status && $booking_status_code && $asset_responses && $asset_booked && $asset_queued) ? true : false;
} // _s_accept_booking_request()

/**
 * Reject Booking Request
 *
 * @param {Int} $post_id
 * @param {Int} $user_id
 * @param {String} $timestamp
 * @return {Boolean} true on success, false on failure
 */
function _s_reject_booking_request($post_id, $user_id = null, $timestamp = null) {
  $user_id         = ($user_id) ? $user_id : get_current_user_id();
  $timestamp       = ($timestamp) ? $timestamp : time();
  $asset_responses = get_post_meta($post_id, 're_asset_responses', true);
  $asset_responses[] = array(
    'asset_id'  => $user_id, 
    'response'  => 0, // 0: reject, 1: accept
    'timestamp' => $timestamp
  );

  // Update Meta Fields
  $asset_responses = update_post_meta($post_id, 're_asset_responses', $asset_responses);
  $asset_id        = update_post_meta($post_id, 're_asset_id', null);
  $asset_queued    = update_post_meta($post_id, 're_asset_queued', null);

  return ($asset_responses && $asset_id && $asset_queued) ? true : false;
} // _s_reject_booking_request()

/**
 * Reject Booking Request Conflicts
 *
 * @param {String} $date_start ISO8601 formatted date string
 * @param {String} $date_end   ISO8601 formatted date string
 * @param {Int}    $user_id    the User ID
 * @return {Array|Boolean} Post Object on success, false on failure
 */
function _s_reject_booking_request_conflicts($date_start, $date_end, $user_id = null) {
  $results   = array();
  $user_id   = ($user_id) ? $user_id : get_current_user_id(); // @TODO maybe req $user_id
  $conflicts = _s_get_booking_request_conflicts($date_start, $date_end, $user_id);

  if ($conflicts->have_posts()) :
    while ($conflicts->have_posts()) : $conflicts->the_post();
      $reject = _s_reject_booking_request(get_the_ID());
      $results[] = array(
        'id' => get_the_ID(),
        'reject_result' => $reject
      );
    endwhile;
    wp_reset_postdata();
  endif;

  return $results;
} // _s_reject_booking_request_conflicts()

/**
 * Get Booking Request Conflicts
 *
 * Criteria for conflicting booking requests:
 *
 * 1. $date_start || $date_end == the booking request's $date_start || $date_end
 * 2. $date_start is BETWEEN the booking request's $date_start & $date_end
 * 3. $date_end is BETWEEN the booking request's $date_start & $date_end
 * 4. $date_start && $date_end SPAN the booking_request's $date_start && $date_end
 *
 * @param {String} $date_start ISO8601 formatted date string
 * @param {String} $date_end   ISO8601 formatted date string
 * @param {Int}    $user_id    the User ID
 * @return {Object} new WP_Query Object
 */
function _s_get_booking_request_conflicts($date_start, $date_end, $user_id = null) {
  $booking_start = strtotime($date_start);
  $booking_end   = strtotime($date_end);
  $user_id       = ($user_id) ? $user_id : get_current_user_id(); // @TODO maybe req $user_id

  $args = array(
    'post_type'   => '_s_asset_requests',
    'post_status' => 'draft',
    'fields'      => 'ids',
    'order'       => 'ASC',
    'orderby'     => 'meta_value_num',
    'meta_key'    => 're_date_start_unix',
    'meta_query'  => array(
      'relation' => 'AND',

      // User ID
      array(
        'key'     => 're_asset_id',
        'value'   => $user_id,
        'compare' => '=',
        'type'    => 'NUMERIC'
      ),

      // Asset Request booking status
      array(
        'key'     => 're_booking_status',
        'value'   => 'pending',
        'compare' => 'LIKE',
        'type'    => 'CHAR'
      ),
      
      array(
        'relation' => 'OR',

        // Get bookings WITHIN accepted booking
        array(
          'relation' => 'OR',
          array(
            'key'     => 're_date_start_unix', // booking_date_start
            'compare' => 'BETWEEN',
            'value'   => array($booking_start, $booking_end), // inclusive
            'type'    => 'NUMERIC'
          ),
          array(
            'key'     => 're_date_end_unix', // booking_date_end
            'compare' => 'BETWEEN',
            'value'   => array($booking_start, $booking_end), // inclusive
            'type'    => 'NUMERIC'
          ),
        ), // inner 1

        // Get bookings that SPAN accepted booking_date_start AND booking_date_end
        // `key` is actually a value we're comparing
        array(
          'relation' => 'AND',
          array(
            'key'     => 're_date_start_unix', // bookings_start
            'compare' => '<=',
            'value'   => $booking_start, // accepted_booking_start
            'type'    => 'NUMERIC'
          ),
          array(
            'key'     => 're_date_end_unix', // bookings_end
            'compare' => '>=',
            'value'   => $booking_end, // accepted_booking_end
            'type'    => 'NUMERIC'
          ),          
        ),
      ), // inner 2
    ) // meta_query
  );

  return new WP_Query($args);
}












/**
 * Update future _s_asset_schedule posts with _re_asset_type
 *
 * @return {Array|Boolean} Post Object on success, false on failure
 */
function _s_update_user_asset_schedules() {
  // Get Author's Future Asset Schedules
  $args = array(
    'post_type'   => '_s_asset_schedules',
    'author'      => get_current_user_id(),
    'post_status' => 'draft',
    'meta_query'  => array(
      'relation' => 'AND',
      array(
        'key'     => 're_date_start_unix',
        'compare' => '>=',
        'value'   => time(),
        'type'    => 'NUMERIC'
      )
    )
  );

  $q = new WP_Query($args);

  if ($q->have_posts()) :
    // Get Current User _re_asset_type Data
    $user_asset_types = get_user_meta(get_current_user_id(), '_re_asset_type', false);
    
    // Get IDs of _s_asset_types terms
    if ($user_asset_types) {
      $user_asset_types_ids = array_map( function($v) { 
        $asset_type_exp         = $v[0] .'-'. $v[1];
        $asset_type_exp_term_id = term_exists($asset_type_exp, '_s_asset_types');
        $asset_type_exp_term_id = $asset_type_exp_term_id['term_id'];
        return $asset_type_exp_term_id;
      }, $user_asset_types );
    } else {
      $user_asset_types_ids = array();
    }

    while ($q->have_posts()) : $q->the_post();
      // Update _s_asset_type meta on each post
      $post_args = array(
        'ID' => get_the_ID(),
        'tax_input' => array( 
          '_s_asset_types' => $user_asset_types_ids,
        ),
      );
      wp_update_post($post_args);
    endwhile;
    wp_reset_postdata();  
  endif;
} // _s_update_user_asset_schedules

<?php

add_action( 'rest_api_init', function() {
  /**
   * Respond to Asset Requests
   */
  register_rest_route( 'booking/v1', '/respond', array(
    'methods' => 'POST',
    'callback' => '_s_rest_endpoint_respond',
    'args' => array(
      'action' => array(
        'required' => true,
        'validate_callback' => function($param) {
          return ($param === 'accept' || $param === 'reject') ? true : false;
        },
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'user_id' => array(
        'default' => get_current_user_id(),
        'required' => true,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric($param);
        },
        'sanitize_callback' => function($param) {
          return (int) sanitize_text_field($param);
        }
      ),
      'post_id' => array(
        'required' => true,
        'validate_callback' => function($param, $request, $key) {
          // @TODO Validate User ID matches Asset Request `re_asset_id`
          // $asset_req = get_post_meta($param);
          // if (is_numeric($param) && $request['user_id'] === $asset_req['re_asset_id']) {
          //   return true;
          // }
          // return false;

          return is_numeric($param);
        },
        'sanitize_callback' => function($param) {
          return (int) sanitize_text_field($param);
        }
      ),
      'date_start' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'date_end' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      )
    ),
    'permission_callback' => function() {
      return _s_is_user_role('asset', wp_get_current_user());
    }
  ) );
} );

/**
 * Accept or Reject a booking request
 */
function _s_rest_endpoint_respond( $request ) {
  if ($request['action'] === 'accept') {
    return _s_booking_request_accept( $request );
  } 
  else if ($request['action'] === 'reject') {
    return _s_booking_request_reject( $request );
  }
  else {
    return null;
  }
}

/**
 * Accept a booking request
 */
function _s_booking_request_accept( $request ) {
  $user_id    = $request['user_id'];
  $post_id    = $request['post_id'];
  $date_start = $request['date_start'];
  $date_end   = $request['date_end'];
  $timestamp  = time();

  // Get Asset Request data
  $asset_req  = get_post_meta($post_id);
  $asset_type = $asset_req['re_asset_type'][0];
  $asset_exp  = $asset_req['re_experience_level'][0];

  // Accept booking request
  $accept_booking = _s_accept_booking_request($post_id, $user_id, $timestamp);

  // Set Asset Availability
  $set_availability = _s_set_availability($date_start, $date_end, $post_id);

  // Update Asset _re_last_booked
  $update_last_booked = update_user_meta($user_id, '_re_last_booked', $timestamp);

  // Reject any conflicting requests
  $reject_conflicts = _s_reject_booking_request_conflicts($date_start, $date_end, $user_id);

  // Send Notifications
  $email_data = array(
    'post_id'    => $post_id,
    'asset_id'   => $user_id,
    'asset_type' => $asset_type,
    'asset_exp'  => $asset_exp,
    'date_start' => $date_start,
    'date_end'   => $date_end,
  );
  $email_asset     = _s_send_accept_booking_email_asset($email_data);
  $email_requester = _s_send_accept_booking_email_requester($email_data);

  $response = array();
  $response['asset_type']             = $asset_type;
  $response['experience_level']       = $asset_exp;
  $response['date_start']             = $date_start;
  $response['date_end']               = $date_end;
  $response['post_id']                = $post_id;
  $response['booking_status']         = get_post_meta($post_id, 're_booking_status', true);
  $response['booking_status_code']    = get_post_meta($post_id, 're_booking_status_code', true);
  
  $response['asset_booked']           = get_post_meta($post_id, 're_asset_booked', true);
  $response['post_meta']              = get_post_meta($post_id);
  $response['update_last_booked_res'] = $update_last_booked;
  $response['set_availability_res']   = $set_availability;
  $response['accept_booking_res']     = $accept_booking;
  $response['reject_conflicts']       = $reject_conflicts;

  return rest_ensure_response($response);
}

/**
 * Reject a booking request
 */
function _s_booking_request_reject( $request ) {
  $user_id = $request['user_id'];
  $post_id = $request['post_id'];

  // Get Asset Request data
  $asset_req  = get_post_meta($post_id);
  $asset_type = $asset_req['re_asset_type'][0];
  $asset_exp  = $asset_req['re_experience_level'][0];
  $date_start = $asset_req['re_date_start'][0];
  $date_end   = $asset_req['re_date_end'][0];

  // Reject booking request
  $reject_booking = _s_reject_booking_request($post_id, $user_id);

  // Trigger _s_cron_get_next_available_asset()
  $next_available = _s_cron_get_next_available_asset($post_id);

  // Send Notifications
  $email_data = array(
    'post_id'    => $post_id,
    'asset_id'   => $user_id,
    'asset_type' => $asset_type,
    'asset_exp'  => $asset_exp,
    'date_start' => $date_start,
    'date_end'   => $date_end,
  );
  $email_asset = _s_send_reject_booking_email_asset($email_data);

  $response = array();
  $response['asset_type']             = $asset_type;
  $response['experience_level']       = $asset_exp;
  $response['date_start']             = $date_start;
  $response['date_end']               = $date_end;
  $response['post_id']                = $post_id;
  $response['booking_status']         = get_post_meta($post_id, 're_booking_status', true);
  $response['booking_status_code']    = get_post_meta($post_id, 're_booking_status_code', true);

  $response['asset_booked']        = get_post_meta($post_id, 're_asset_booked', true);
  $response['post_meta']           = get_post_meta($post_id);
  $response['reject_booking_res']  = $reject_booking;
  $response['next_available_res']  = $next_available;
  $response['email_asset_res']     = $email_asset;

  return rest_ensure_response($response);
}

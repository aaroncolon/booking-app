<?php

add_action( 'rest_api_init', function() {
  /**
   * CRUD Abilities
   */
  register_rest_route( 'booking/v1', '/asset-abilities', array(
    'methods' => 'GET',
    'callback' => '_s_rest_endpoint_asset_abilities_get',
    'args' => array(
      'user_id' => array(
        'default' => get_current_user_id(),
        'required' => true,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric($param);
        },
        'sanitize_callback' => function($param) {
          return (int) sanitize_text_field($param);
        }
      ),
      'page' => array(
        'default' => 1,
        'required' => false,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric( $param );
        }
      ),
      'per_page' => array(
        'default' => 20,
        'required' => false,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric( $param );
        }
      ),
    ),
    'permission_callback' => function() {
      return _s_is_user_role('asset');
    }
  ) );

  register_rest_route( 'booking/v1', '/asset-abilities', array(
    'methods' => 'POST',
    'callback' => '_s_rest_endpoint_asset_abilities_create',
    'args' => array(
      'user_id' => array(
        'default' => get_current_user_id(),
        'required' => true,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric($param);
        },
        'sanitize_callback' => function($param) {
          return (int) sanitize_text_field($param);
        }
      ),
      'asset_type' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'experience_level' => array(
        'required' => true,
        'validate_callback' => function($param) {
          return in_array($param, array('junior', 'mid', 'senior'));
        },
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      )
    ),
    'permission_callback' => function() {
      return _s_is_user_role('asset');
    }
  ) );

  register_rest_route( 'booking/v1', '/asset-abilities', array(
    'methods' => 'DELETE',
    'callback' => '_s_rest_endpoint_asset_abilities_delete',
    'args' => array(
      'user_id' => array(
        'default' => get_current_user_id(),
        'required' => true,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric($param);
        },
        'sanitize_callback' => function($param) {
          return (int) sanitize_text_field($param);
        }
      ),
      'asset_type' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'experience_level' => array(
        'required' => true,
        'validate_callback' => function($param) {
          return in_array($param, array('junior', 'mid', 'senior'));
        },
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      )
    ),
    'permission_callback' => function() {
      return _s_is_user_role('asset');
    }
  ) );
} );

function _s_rest_endpoint_asset_abilities_get( $request ) {
  $response = array();
  $user_abilities = get_user_meta($request['user_id'], '_re_asset_type', false);

  if ($user_abilities) {
    for ($i = 0; $i < count($user_abilities); $i++) {
      $asset_type     = $user_abilities[$i][0];
      $exp_level      = $user_abilities[$i][1];
      $asset_type_exp = $asset_type .'-'. $exp_level;
      $asset_type_f   = ucwords(str_replace('-', ' ', $asset_type));
      $exp_level_f    = ucwords($exp_level);

      $response[] = array(
        'asset_type' => $asset_type,
        'exp_level' => $exp_level,
        'asset_type_exp' => $asset_type_exp,
        'asset_type_formatted' => $asset_type_f,
        'exp_level_formatted' => $exp_level_f
      );
    }
  }

  return rest_ensure_response($response);
}

function _s_rest_endpoint_asset_abilities_create( $request ) {
  $response = array();
  $add    = null;

  $asset_type     = $request['asset_type'];
  $exp_level      = $request['experience_level'];
  $asset_type_exp = array($asset_type, $exp_level);
  // $asset_type_exp = implode('-', array($asset_type, $exp_level));

  // Manually check if value already exists
  $user_asset_types = get_user_meta(get_current_user_id(), '_re_asset_type', false);
  if ( (! $user_asset_types) || ($user_asset_types && ! in_array($asset_type_exp, $user_asset_types, true)) ) {
    // @NOTE adds duplicate key with unique value. when retrieved with get_user_meta, an array is returned
    $add = add_user_meta(get_current_user_id(), '_re_asset_type', $asset_type_exp, false);

    if (!$add) {
      return new WP_Error('database_error', 'Add user meta failed.');
    }

    // Update existing user Asset Schedule posts
    _s_update_user_asset_schedules();
  }

  $asset_type_f = ucwords(str_replace('-', ' ', $asset_type));
  $exp_level_f = ucwords($exp_level);
  $asset_type_exp = implode('-', $asset_type_exp);

  $response[] = array(
    'add_result'           => $add,
    'asset_type'           => $asset_type,
    'exp_level'            => $exp_level,
    'asset_type_exp'       => $asset_type_exp,
    'asset_type_formatted' => $asset_type_f,
    'exp_level_formatted'  => $exp_level_f,
  );

  return rest_ensure_response($response);
}

function _s_rest_endpoint_asset_abilities_delete( $request ) {
  $response = array();

  $asset_type     = $request['asset_type'];
  $exp_level      = $request['experience_level'];
  $asset_type_exp = array($asset_type, $exp_level);

  $delete = delete_user_meta(get_current_user_id(), '_re_asset_type', $asset_type_exp);

  if (!$delete) {
    return new WP_Error('database_error', 'Delete user meta failed.');
  }

  // Update existing user Asset Schedule posts
  _s_update_user_asset_schedules();

  $response['delete_res'] = $delete;
              
  return rest_ensure_response($response);
}

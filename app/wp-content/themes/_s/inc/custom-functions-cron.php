<?php

/**
 * Checks for an alternate asset after an asset request rejection
 *
 * @param {Int} $post_id the Asset Request Post ID
 * @return {Bool|Null} True on email success, False on failure, Null if error
 */
function _s_cron_get_next_available_asset($post_id) {
  // After booking request rejection, check for an alternate asset
  $asset_type     = get_post_meta($post_id, 're_asset_type', true);
  $asset_exp      = get_post_meta($post_id, 're_experience_level', true);
  $date_start     = get_post_meta($post_id, 're_date_start', true);
  $date_end       = get_post_meta($post_id, 're_date_end', true);
  $asset_resp     = get_post_meta($post_id, 're_asset_responses', true);
  $exclude_assets = array_map('_s_map_asset_id', $asset_resp);

  $assets = _s_check_asset_availability($asset_type, $asset_exp, $date_start, $date_end, $exclude_assets);

  // If an asset is available
  if ($assets) {
    // update Asset Request Post Meta with next Asset ID
    $next_asset_id = _s_set_next_available_asset($post_id, $assets[0]['asset_id']);

    if (! $next_asset_id) {
      return null;
    }

    // Send email / notification
    $email_data = array(
      'asset_type'       => $asset_type,
      'experience_level' => $asset_exp,
      'date_start'       => $date_start,
      'date_end'         => $date_end,
      'post_id'          => $post_id,
      'asset_id'         => $next_asset_id,
    );
    $email_asset = _s_send_request_asset_email($email_data);

    return $email_asset;
  }
  else {
    // @NOTE in the future, we'll maybe look for "next best" alternates here
    return null;
  }
}

/**
 * Update Asset Request Post Meta with new Asset ID
 *
 * @param {Int} $post_id the Asset Request Post ID
 * @param {Int} $asset_id the Asset ID
 */
function _s_set_next_available_asset($post_id, $asset_id) {
  $asset_id_res     = update_post_meta($post_id, 're_asset_id', $asset_id);
  $asset_queued_res = update_post_meta($post_id, 're_asset_queued', $asset_id);
  return ($asset_id_res && $asset_queued_res) ? $asset_id : false;
}

function _s_map_asset_id($v) {
  return $v['asset_id'];
}

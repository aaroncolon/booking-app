<?php

add_action( 'rest_api_init', function() {
  /**
   * Get Current User's Booking Requests
   */
  register_rest_route( 'booking/v1', '/requests', array(
    'methods' => 'GET',
    'callback' => '_s_rest_endpoint_requests',
    'args' => array(
      'user_id' => array(
        'default' => get_current_user_id(),
        'validate_callback' => function($param, $request, $key) {
          return is_numeric( $param );
        }
      ),
      'page' => array(
        'default' => 1,
        'required' => false,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric( $param );
        }
      ),
      'per_page' => array(
        'default' => 10,
        'required' => false,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric( $param );
        }
      )
    ),
    'permission_callback' => function() {
      return ( _s_is_user_role('asset') || _s_is_user_role('requester') ) ? true : false;
    }
  ) );
});

/**
 * Get Current User's Booking Requests
 */
function _s_rest_endpoint_requests( $request ) {
  if ( _s_is_user_role('asset') ) {
    return _s_rest_get_booking_requests_asset( $request );
  } else if ( _s_is_user_role('requester') ) {
    return _s_rest_get_booking_requests_requester( $request );
  } else {
    return null;
  }
}

/**
 * Get Current Requester's Booking Requests
 * 
 * @return {Array} Collection of Asset Request items
 */
function _s_rest_get_booking_requests_requester( $request ) {
  $booking_data = array();

  // Build Query
  $args = array(
    'post_type'      => '_s_asset_requests',
    'post_status'    => array('draft'),
    // 'author'      => get_current_user_id(),
    'author'         => $request['user_id'],
    'order'          => 'DESC',
    'orderby'        => 'meta_value_num',
    'meta_key'       => 're_date_start_unix',
    'paged'          => $request['page'],
    'posts_per_page' => $request['per_page'],
    'fields'         => 'ids'
  );

  $q = new WP_Query($args);

  if ($q->have_posts()) :
    while ($q->have_posts()) : $q->the_post();
      $booking_data[] = array(
        'asset_email'         => get_post_meta(get_the_ID(), 're_asset_email', true),
        'asset_type'          => get_post_meta(get_the_ID(), 're_asset_type', true),
        'experience_level'    => get_post_meta(get_the_ID(), 're_experience_level', true),
        'date_start'          => get_post_meta(get_the_ID(), 're_date_start', true),
        'date_end'            => get_post_meta(get_the_ID(), 're_date_end', true),
        'booking_status'      => get_post_meta(get_the_ID(), 're_booking_status', true),
        'booking_status_code' => get_post_meta(get_the_ID(), 're_booking_status_code', true),
        'post_id'             => get_the_ID(),
      );
    endwhile;
    wp_reset_postdata();
  endif;

  // Response
  $response = rest_ensure_response($booking_data);

  // Headers
  $response->header('X-WP-Total', $q->found_posts);
  $response->header('X-WP-TotalPages', $q->max_num_pages);

  return $response;
}

/**
 * Get Current Asset's Booking Requests
 *
 * @return {Array} Collection of Asset Request items
 */
function _s_rest_get_booking_requests_asset( $request ) {
  $booking_data = array();
  $user_id = $request['user_id'];

  // Build Query
  $args = array(
    'post_type'      => '_s_asset_requests',
    'post_status'    => array('draft'),
    'posts_per_page' => $request['per_page'],
    'paged'          => $request['page'],
    'order'          => 'ASC',
    'orderby'        => 'meta_value_num',
    'meta_key'       => 're_date_start_unix',
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key'     => 're_asset_id',
        'value'   => $user_id,
        'compare' => '=',
        'type'    => 'NUMERIC'
      )
    ),
    'fields' => 'ids'
  );

  // Query
  $q = new WP_Query($args);

  if ($q->have_posts()) :
    while ($q->have_posts()) : $q->the_post();
      // Prepare Data
      $booking_data[] = array(
        'asset_type'          => get_post_meta(get_the_ID(), 're_asset_type', true),
        'experience_level'    => get_post_meta(get_the_ID(), 're_experience_level', true),
        'date_start'          => get_post_meta(get_the_ID(), 're_date_start', true),
        'date_end'            => get_post_meta(get_the_ID(), 're_date_end', true),
        'booking_status'      => get_post_meta(get_the_ID(), 're_booking_status', true),
        'booking_status_code' => get_post_meta(get_the_ID(), 're_booking_status_code', true),
        'post_id'             => get_the_ID(),
        'user_id'             => $user_id,
        'nonce_accept'        => wp_create_nonce('nonce_booking_request_accept_'. get_the_ID() .'_'. $user_id),
        'nonce_reject'        => wp_create_nonce('nonce_booking_request_reject_'. get_the_ID() .'_'. $user_id),
      );
    endwhile;
    wp_reset_postdata();
  endif;

  // Response
  $response = rest_ensure_response($booking_data);

  // Headers
  $response->header('X-WP-Total', $q->found_posts);
  $response->header('X-WP-TotalPages', $q->max_num_pages);

  return $response;
}

<?php

/**
 * Request an Asset
 * 
 * Checks the availability of assets
 * 
 * @return {Array} $result the input query data and query results 
 */
function _s_request_asset_handler() {
  // Verify nonce
  check_ajax_referer('nonce_request_asset', 'nonce');

  $result = array();

  // Validate input data
  $asset_email    = sanitize_email($_POST['asset_email']);
  $asset_type     = sanitize_text_field($_POST['asset_type']);
  $exp_level      = sanitize_text_field($_POST['experience_level']);
  $asset_type_exp = implode('-', array($asset_type, $exp_level));
  $date_start     = sanitize_text_field($_POST['date_start']);
  $date_end       = sanitize_text_field($_POST['date_end']);
  $timestamp      = time();

  // Check Asset Availability
  if (!empty($asset_email)) {
    $available_assets = _s_check_asset_availability_specific($asset_email, $date_start, $date_end);
  } else {
    $available_assets = _s_check_asset_availability($asset_type, $exp_level, $date_start, $date_end);
  }

  // Get the Result
  $result['asset_email']      = (!empty($asset_email)) ? $asset_email : null;
  $result['asset_type']       = $asset_type;
  $result['experience_level'] = $exp_level;
  $result['asset_type_exp']   = $asset_type_exp;
  $result['date_start']       = $date_start;
  $result['date_end']         = $date_end;
  $result['timestamp']        = $timestamp;
  $result['timestamp_form']   = date('Y-m-j h:i:s A', $timestamp);
  $result['available_assets'] = $available_assets;

  wp_send_json_success($result);
}
add_action('wp_ajax__s_request_asset_event', '_s_request_asset_handler');
add_action('wp_ajax_nopriv__s_request_asset_event', '_s_request_asset_handler');

/**
 * Request Asset Complete Handler
 * Creates a new Asset Request post with user's input data
 *
 * @return {Array} $results user's input data, post data, 
 */
function _s_request_asset_complete_handler() {
  // Verify nonce
  check_ajax_referer('nonce_request_asset', 'nonce');

  $result = array();

  // Validate input data
  $asset_email            = sanitize_email($_POST['asset_email']);
  $asset_email            = (!empty($asset_email)) ? $asset_email : null;
  $asset_id               = sanitize_text_field($_POST['asset_id']);
  $asset_type             = sanitize_text_field($_POST['asset_type']);
  $exp_level              = sanitize_text_field($_POST['experience_level']);
  // $exp_level_term_id      = term_exists($exp_level, '_s_experience_levels');
  // $exp_level_term_id      = $exp_level_term_id['term_id'];
  $asset_type_exp         = implode('-', array($asset_type, $exp_level));
  $asset_type_exp_term_id = term_exists($asset_type_exp, '_s_asset_types');
  $asset_type_exp_term_id = $asset_type_exp_term_id['term_id'];
  $date_start             = sanitize_text_field($_POST['date_start']);
  $date_end               = sanitize_text_field($_POST['date_end']);
  $date_start_unix        = strtotime($date_start);
  $date_end_unix          = strtotime($date_end);
  $timestamp              = time();

  // Gather the data to create the Post
  $post_data = array(
    'post_type'    => '_s_asset_requests',
    'post_title'   => $asset_type .' '. $exp_level .' from '. $date_start .' to '. $date_end,
    'post_content' => $timestamp,
    'post_status'  => 'draft', // 'publish' requires post content
    'tax_input'    => array( 
      '_s_asset_types' => $asset_type_exp_term_id,
      // '_s_experience_levels' => $exp_level_term_id
    ),
    'meta_input'   => array(
      're_asset_email'         => $asset_email,
      're_asset_id'            => $asset_id, // @NOTE @TODO maybe server-side
      're_asset_queued'        => $asset_id,
      're_asset_type'          => $asset_type,
      're_experience_level'    => $exp_level,
      're_date_start'          => $date_start,
      're_date_end'            => $date_end,
      're_date_start_unix'     => $date_start_unix,
      're_date_end_unix'       => $date_end_unix,
      're_timestamp'           => $timestamp,
      're_booking_status'      => 'pending', // created, pending, booked, cancelled
      're_booking_status_code' => 1, // 0, 1, 2, 3
      're_asset_responses'     => null,
      're_asset_booked'        => null
    ),
    'comment_status' => 'closed',
    'ping_status'    => 'closed',
  );

  // Add the Asset Request
  $post_id = wp_insert_post($post_data, true);

  // Verify post insertion
  if (is_wp_error($post_id)) {
    wp_send_json_error('Unable to complete request. Please try again.');
  }

  // Get the Result
  $result['asset_email']      = $asset_email;
  $result['asset_id']         = $asset_id;
  $result['asset_type']       = $asset_type;
  $result['experience_level'] = $exp_level;
  $result['asset_type_exp']   = $asset_type_exp;
  $result['asset_type_exp_term_id'] = $asset_type_exp_term_id;
  $result['date_start']       = $date_start;
  $result['date_end']         = $date_end;
  $result['date_start_unix']  = $date_start_unix;
  $result['date_end_unix']    = $date_end_unix;
  $result['timestamp']        = $timestamp;
  $result['time']             = date('Y-m-j h:i:s A', $timestamp);
  $result['post_id']          = $post_id;
  $result['requester_email']  = wp_get_current_user()->user_email;
  // $result['nonce_accept']     = wp_create_nonce('nonce_booking_request_accept_link_'. $post_id .'_'. $asset_id);
  // $result['nonce_reject']     = wp_create_nonce('nonce_booking_request_reject_link_'. $post_id .'_'. $asset_id);

  // @TODO send the asset request email to asset
  $email_asset = _s_send_request_asset_email($result);

  // @TODO send the confirmation email to requester
  $email_requester = _s_send_request_asset_email_confirmation($result);

  $result['email_asset']     = $email_asset;
  $result['email_requester'] = $email_requester;

  wp_send_json_success($result);
}
add_action('wp_ajax__s_request_asset_complete_event', '_s_request_asset_complete_handler');
add_action('wp_ajax_nopriv__s_request_asset_complete_event', '_s_request_asset_complete_handler');


function _s_get_booking_requests_handler() {
  // Verify nonce
  check_ajax_referer('nonce_get_booking_requests', 'nonce');

  $booking_data = array();

  // Build Query
  $args = array(
    'post_type' => '_s_asset_requests',
    'author'    => get_current_user_id(),
    'order'     => 'ASC',
    'orderby'   => 'meta_value_num',
    'meta_key'  => 're_date_start_unix',
    'posts_per_page' => 30,
  );

  $booking_q = new WP_Query($args);

  if ($booking_q->have_posts()) :
    while ($booking_q->have_posts()) : $booking_q->the_post();
      $booking_data[] = array(
        'asset_type'          => get_post_meta(get_the_ID(), 're_asset_type', true),
        'experience_level'    => get_post_meta(get_the_ID(), 're_experience_level', true),
        'date_start'          => get_post_meta(get_the_ID(), 're_date_start', true),
        'date_end'            => get_post_meta(get_the_ID(), 're_date_end', true),
        'booking_status'      => get_post_meta(get_the_ID(), 're_booking_status', true),
        'booking_status_code' => get_post_meta(get_the_ID(), 're_booking_status_code', true),
        'post_id'             => get_the_ID(),
      );
    endwhile;
    wp_reset_postdata();
  endif;

  wp_send_json_success($booking_data);
}
add_action('wp_ajax__s_get_booking_requests_event', '_s_get_booking_requests_handler');
add_action('wp_ajax_nopriv__s_get_booking_requests_event', '_s_get_booking_requests_handler');


/**
 * Get Assets with status of `booked`
 *
 */
function _s_get_booked_assets_handler() {
  // Verify nonce
  check_ajax_referer('nonce_get_booked_assets', 'nonce');

  wp_send_json_success();
}
add_action('wp_ajax__s_get_booked_assets_event', '_s_get_booked_assets_handler');
add_action('wp_ajax_nopriv__s_get_booked_assets_event', '_s_get_booked_assets_handler');

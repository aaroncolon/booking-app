<script type="text/template" id="tmpl-availability-row">
  <tr>
    <td>{{ data.date_start }}</td>
    <td>{{ data.date_end }}</td>
    <td>
      <# if (data.asset_request_id) { #>
        {{ data.asset_request_id }}
      <# } else { #>
        n/a
      <# } #>
    </td>
    <td>
      <# if (! data.asset_request_id) { #>
        <button
          type="button"
          id="btn-post-{{ data.post_id }}"
          data-post-id="{{ data.post_id }}"
          data-action="delete"
        >
          Delete
        </button>
      <# } #>
    </td>
  </tr>
</script>

<script type="text/template" id="tmpl-abilities-row">
  <tr>
    <td>{{ data.asset_type_formatted }}</td>
    <td>{{ data.exp_level_formatted }}</td>
    <td>
      <a id="{{ data.asset_type_exp }}" href="javascript:;" data-action="delete" data-asset-type="{{ data.asset_type }}" data-exp-level="{{ data.exp_level }}">Delete</a>
    </td>
  </tr>
</script>

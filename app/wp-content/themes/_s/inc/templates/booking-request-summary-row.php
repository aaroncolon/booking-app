<script type="text/template" id="tmpl-booking-request-summary-row">
  <tr>
    <td>{{ data.asset_type }}</td>
    <td>{{ data.experience_level }}</td>
    <td>{{ data.date_start }}</td>
    <td>{{ data.date_end }}</td>
    <td>{{ data.asset_email }}</td>
  </tr>
</script>

<script type="text/template" id="tmpl-booking-requests-row">
  <tr>
    <td>{{ data.asset_type }}</td>
    <td>{{ data.experience_level }}</td>
    <td>{{ data.date_start }}</td>
    <td>{{ data.date_end }}</td>
    <td>{{ data.booking_status }}</td>
    <td>
      <# if ( data.booking_status === "pending" && data.user_type === "asset") { #>
        <button
          type="button"
          id="btn-accept-{{ data.post_id }}"
          data-post-id="{{ data.post_id }}"
          data-user-id="{{ data.user_id }}"
          data-date-start="{{ data.date_start }}"
          data-date-end="{{ data.date_end }}"
          data-action="accept"
        >
          Accept
        </button>
        <button
          type="button"
          id="btn-reject-{{ data.post_id }}"
          data-post-id="{{ data.post_id }}"
          data-user-id="{{ data.user_id }}"
          data-date-start="{{ data.date_start }}"
          data-date-end="{{ data.date_end }}"
          data-action="reject"
        >
          Reject
        </button>
      <# } else { #>
        <button
          type="button"
          id="btn-cancel-{{ data.post_id }}"
          name="btn-cancel-{{ data.post_id }}"
          data-post-id="{{ data.post_id }}"
          data-action="cancel"
        >
          Cancel
        </button>
      <# } #>
    </td>
  </tr>
</script>

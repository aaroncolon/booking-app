<?php

add_action( 'rest_api_init', function() {
  /**
   * Get Asset Availability
   */
  register_rest_route( 'booking/v1', '/assets', array(
    'methods' => 'GET',
    'callback' => '_s_rest_get_asset_availability',
    'args' => array(
      'page' => array(
        'default' => 1,
        'required' => false,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric( $param );
        }
      ),
      'per_page' => array(
        'default' => 10,
        'required' => false,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric( $param );
        }
      ),
      'asset_email' => array(
        'required' => false,
        'sanitize_callback' => function($param) {
          return sanitize_email($param);
        }
      ),
      'asset_type' => array(
        'required' => false,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'experience_level' => array(
        'required' => false,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'date_start' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'date_end' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      )
    ),
    'permission_callback' => function() {
      return _s_is_user_role('requester', wp_get_current_user());
    }
  ) );

  /**
   * Create Asset Request
   */
  register_rest_route( 'booking/v1', '/assets', array(
    'methods' => 'POST',
    'callback' => '_s_rest_create_asset_request',
    'args' => array(
      'asset_email' => array(
        'required' => false,
        'sanitize_callback' => function($param) {
          return sanitize_email($param);
        },
        'validate_callback' => function($param) {
          return (!empty($param)) ? true : false;
        }
      ),
      'asset_id' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'asset_type' => array(
        'required' => false,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'experience_level' => array(
        'required' => false,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'date_start' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'date_end' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      )
    ),
    'permission_callback' => function() {
      return _s_is_user_role('requester', wp_get_current_user());
    }
  ) );
});



/**
 * Get Asset Availability
 * 
 * @return {Array} $result the input query data and query results 
 */
function _s_rest_get_asset_availability( $request ) {
  $response = array();

  // Store input data
  $asset_email    = $request['asset_email'];
  $asset_type     = $request['asset_type'];
  $exp_level      = $request['experience_level'];
  $asset_type_exp = implode('-', array($asset_type, $exp_level));
  $date_start     = $request['date_start'];
  $date_end       = $request['date_end'];
  $timestamp      = time();

  // Check Asset Availability
  if (! empty($asset_email)) {
    $available_assets = _s_check_asset_availability_specific($asset_email, $date_start, $date_end);
  } else {
    $available_assets = _s_check_asset_availability($asset_type, $exp_level, $date_start, $date_end);
  }

  // Get the result
  $response['asset_email']      = (! empty($asset_email)) ? $asset_email : null;
  $response['asset_type']       = (! empty($asset_type)) ? $asset_type : null;
  $response['experience_level'] = (! empty($exp_level)) ? $exp_level : null;
  $response['asset_type_exp']   = $asset_type_exp;
  $response['date_start']       = $date_start;
  $response['date_end']         = $date_end;
  $response['timestamp']        = $timestamp;
  $response['timestamp_form']   = date('Y-m-j h:i:s A', $timestamp);
  $response['available_assets'] = $available_assets;

  return rest_ensure_response($response);
}

/**
 * Create Asset Request
 * Creates a new Asset Request post with user's input data
 *
 * @return {Array} $results user's input data, post data, 
 */
function _s_rest_create_asset_request( $request ) {
  $response = array();

  // Save input data
  $asset_email            = $request['asset_email'];
  $asset_id               = $request['asset_id'];
  $asset_type             = $request['asset_type'];
  $exp_level              = $request['experience_level'];
  $date_start             = $request['date_start'];
  $date_end               = $request['date_end'];
  $date_start_unix        = strtotime($date_start);
  $date_end_unix          = strtotime($date_end);
  $timestamp              = time();

  // Get Term data
  $asset_type_exp         = implode('-', array($asset_type, $exp_level));
  $asset_type_exp_term_id = term_exists($asset_type_exp, '_s_asset_types');
  $asset_type_exp_term_id = $asset_type_exp_term_id['term_id'];

  // Gather the data to create the Post
  $post_data = array(
    'post_type'    => '_s_asset_requests',
    'post_title'   => $asset_type .' '. $exp_level .' from '. $date_start .' to '. $date_end,
    'post_content' => $timestamp,
    'post_status'  => 'draft', // 'publish' requires post content
    'tax_input'    => array( 
      '_s_asset_types' => $asset_type_exp_term_id,
      // '_s_experience_levels' => $exp_level_term_id
    ),
    'meta_input'   => array(
      're_asset_email'         => $asset_email,
      're_asset_id'            => $asset_id, // @NOTE @TODO maybe server-side
      're_asset_queued'        => $asset_id,
      're_asset_type'          => $asset_type,
      're_experience_level'    => $exp_level,
      're_date_start'          => $date_start,
      're_date_end'            => $date_end,
      're_date_start_unix'     => $date_start_unix,
      're_date_end_unix'       => $date_end_unix,
      're_timestamp'           => $timestamp,
      're_booking_status'      => 'pending', // created, pending, booked, cancelled
      're_booking_status_code' => 1, // 0, 1, 2, 3
      're_asset_responses'     => null,
      're_asset_booked'        => null
    ),
    'comment_status' => 'closed',
    'ping_status'    => 'closed',
  );

  // Add the Asset Request
  $post_id = wp_insert_post($post_data, true);

  // Verify post insertion
  if (is_wp_error($post_id)) {
    return new WP_Error( 'error', 'Unable to complete request. Please try again.', array( 'status' => 404 ) );
  }

  // Get the Result
  $response['asset_email']      = $asset_email;
  $response['asset_id']         = $asset_id;
  $response['asset_type']       = $asset_type;
  $response['experience_level'] = $exp_level;
  $response['asset_type_exp']   = $asset_type_exp;
  $response['asset_type_exp_term_id'] = $asset_type_exp_term_id;
  $response['date_start']       = $date_start;
  $response['date_end']         = $date_end;
  $response['date_start_unix']  = $date_start_unix;
  $response['date_end_unix']    = $date_end_unix;
  $response['timestamp']        = $timestamp;
  $response['time']             = date('Y-m-j h:i:s A', $timestamp);
  $response['post_id']          = $post_id;
  $response['requester_email']  = wp_get_current_user()->user_email;
  // $response['nonce_accept']     = wp_create_nonce('nonce_booking_request_accept_link_'. $post_id .'_'. $asset_id);
  // $response['nonce_reject']     = wp_create_nonce('nonce_booking_request_reject_link_'. $post_id .'_'. $asset_id);

  // @TODO send the asset request email to asset
  $email_asset = _s_send_request_asset_email($response);

  // @TODO send the confirmation email to requester
  $email_requester = _s_send_request_asset_email_confirmation($response);

  $response['email_asset']     = $email_asset;
  $response['email_requester'] = $email_requester;

  return rest_ensure_response($response);
}

<?php
/**
 * Get Assets By Type
 *
 * @param {String} $asset_type type of asset
 * @param {String} $asset_exp  experience level
 * @param {Int}    $exclude_asset User ID to exclude from query
 *
 * @return {Array|null} Array of author IDs || null
 */
function _s_get_assets_by_type($asset_type, $asset_exp, $exclude_assets) {
  $asset_data     = array();
  $asset_type_exp = serialize(array($asset_type, $asset_exp)); // @NOTE must serialize to match storage in db

  // User Query args
  $args = array(
    'order'    => 'ASC',
    'orderby'  => 'meta_value_num',
    'meta_key' => '_re_last_booked', // for orderby
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key'     => '_re_asset_type',
        'value'   => $asset_type_exp,
        'compare' => 'LIKE' // bc we are searching for a string
      )
    ),
    'fields' => 'ID' // specifying an array() returns WP User Obj
  );

  // Exclude assets that have rejected this asset request
  if ($exclude_assets) {
    $args['exclude'] = $exclude_assets;
  }

  $q = new WP_User_Query($args);

  $asset_data = $q->get_results();

  return (!empty($asset_data)) ? $asset_data : null;
}

/**
 * Check Asset Availability
 *
 * - Checks if requested asset exists, sorted by _re_last_booked.
 * - Compares _s_asset_schedules against booking dates.
 * - Checks unavailability isn't within booking dates.
 * - Checks booking isn't within unavailability dates.
 *
 * @param {String} $asset_type type of asset
 * @param {String} $asset_exp  experience level
 * @param {String} $date_start booking start date
 * @param {String} $date_end   booking end date
 * @param {Int}    $exclude_asset Asset ID to exclude from query (rejections)
 *
 * @return {Array|null} 
 */
function _s_check_asset_availability($asset_type, $asset_exp, $date_start, $date_end, $exclude_assets = null) {
  // Check if assets exists, sorted by _re_last_booked
  $user_query = _s_get_assets_by_type($asset_type, $asset_exp, $exclude_assets);

  // If there are no matching users, return
  if (!$user_query) {
    return null;
  }

  // Check User ID's availability schedule
  $asset_data = array();
  $book_start = strtotime($date_start);
  $book_end   = strtotime($date_end . '+1 days -1 seconds'); // set to 23:59:59
  
  for ($i = 0; $i < count($user_query); $i++) :
    // WP Query args
    $args = array(
      'post_type'   => '_s_asset_schedules',
      'post_status' => 'draft',
      'author'      => $user_query[$i],
      // 'tax_query' => array(
      //   // 'relation' => 'AND',
      //   array(
      //     'taxonomy' => '_s_asset_types',
      //     'field'    => 'slug',
      //     'terms'    => $asset_type_exp
      //   ),
      // ),
      'meta_query' => array(
        array(
          'relation' => 'OR',

          // Check unavailability isn't WITHIN booking
          array(
            'relation' => 'OR',
            array(
              'key'     => 're_date_start_unix', // unavailable_date_start
              'compare' => 'BETWEEN', // inclusive
              'value'   => array($book_start, $book_end),
              'type'    => 'NUMERIC'
            ),
            array(
              'key'     => 're_date_end_unix', // unavailable_date_end
              'compare' => 'BETWEEN', // inclusive
              'value'   => array($book_start, $book_end),
              'type'    => 'NUMERIC'
            ),
          ),

          // Check booking isn't WITHIN unavailability (opp of above)
          // `key` is actually a value we're comparing
          array(
            'relation' => 'AND',
            array(
              'value'   => $book_start,
              'compare' => '<=',
              'key'     => 're_date_start_unix',              
              'type'    => 'NUMERIC'
            ),
            array(
              'value'   => $book_start,
              'compare' => '>=',
              'key'     => 're_date_end_unix',
              'type'    => 'NUMERIC'
            ),
            array(
              'value'   => $book_end,
              'compare' => '<=',
              'key'     => 're_date_start_unix',
              'type'    => 'NUMERIC'
            ),
            array(
              'value'   => $book_end,
              'compare' => '>=',
              'key'     => 're_date_end_unix',
              'type'    => 'NUMERIC'
            )
          )
        )
      ) 
    );

    // WP Query
    $q = new WP_Query($args);

    // if have_posts(), user is unavailable.
    if ($q->have_posts()) :
      wp_reset_postdata();
      continue;
    // user is available
    else :
      // add current User ID to $asset_data and break
      $asset_data[] = array(
        'asset_id' => $user_query[$i]
      );
      wp_reset_postdata();
      break; // exit for loop
    endif;
  endfor;

  return (!empty($asset_data)) ? $asset_data : null;
}

/**
 * Check Specific Asset Availability
 *
 * @param {String} $email      asset's email
 * @param {String} $date_start booking start date
 * @param {String} $date_end   booking end date
 *
 * @return {Array|null} 
 */
function _s_check_asset_availability_specific($email = null, $date_start, $date_end) {
  // Query Schedule Posts by $email (author) and meta $date_start, $date_end
  $user = get_user_by('email', $email);

  if (!$user) {
    return null;
  }

  $asset_data = array();
  $user_id    = $user->ID;
  $book_start = strtotime($date_start);
  $book_end   = strtotime($date_end . '+1 days -1 seconds'); // set to 23:59:59
  
  // WP Query args
  $args = array(
    'post_type'   => '_s_asset_schedules',
    'post_status' => 'draft',
    'author'      => $user_id,
    'meta_query' => array(
      'relation' => 'OR',

      // Check unavailability isn't WITHIN booking
      array(
        'relation' => 'OR',
        array(
          'key'   => 're_date_start_unix', // unavailable_date_start
          'value' => array($book_start, $book_end),
          'compare' => 'BETWEEN', // @NOTE inclusive
          'type' => 'NUMERIC'
        ),
        array(
          'key'   => 're_date_end_unix', // unavailable_date_end
          'value' => array($book_start, $book_end),
          'compare' => 'BETWEEN', // @NOTE inclusive
          'type' => 'NUMERIC'
        )
      ),

      // Check booking isn't WITHIN unavailability (opp of above)
      // `key` is actually a value we're comparing
      array(
        'relation' => 'AND',
        array(
          'value'   => $book_start,
          'compare' => '<=',
          'key'     => 're_date_start_unix',              
          'type'    => 'NUMERIC'
        ),
        array(
          'value'   => $book_start,
          'compare' => '>=',
          'key'     => 're_date_end_unix',
          'type'    => 'NUMERIC'
        ),
        array(
          'value'   => $book_end,
          'compare' => '<=',
          'key'     => 're_date_start_unix',
          'type'    => 'NUMERIC'
        ),
        array(
          'value'   => $book_end,
          'compare' => '>=',
          'key'     => 're_date_end_unix',
          'type'    => 'NUMERIC'
        )
      )
    )
  );

  // WP Query
  $q = new WP_Query($args);

  // if have_posts(), user is unavailable.
  if ($q->have_posts()) :
    wp_reset_postdata();
  // user is available
  else :
    $asset_data[] = array(
      'asset_id' => $user_id
    );
    wp_reset_postdata();
  endif;

  return (!empty($asset_data)) ? $asset_data : null;
}

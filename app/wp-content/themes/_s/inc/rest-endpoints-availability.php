<?php

add_action( 'rest_api_init', function() {
  /**
   * CRUD Availability
   */
  register_rest_route( 'booking/v1', '/availability', array(
    'methods' => 'GET',
    'callback' => '_s_rest_endpoint_availability_get',
    'args' => array(
      'user_id' => array(
        'default' => get_current_user_id(),
        'required' => true,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric($param);
        },
        'sanitize_callback' => function($param) {
          return (int) sanitize_text_field($param);
        }
      ),
      'page' => array(
        'default' => 1,
        'required' => false,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric( $param );
        }
      ),
      'per_page' => array(
        'default' => 10,
        'required' => false,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric( $param );
        }
      ),
    ),
    'permission_callback' => function() {
      return _s_is_user_role('asset');
    }
  ) );

  register_rest_route( 'booking/v1', '/availability', array(
    'methods' => 'POST',
    'callback' => '_s_rest_endpoint_availability_create',
    'args' => array(
      'user_id' => array(
        'default' => get_current_user_id(),
        'required' => true,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric($param);
        },
        'sanitize_callback' => function($param) {
          return (int) sanitize_text_field($param);
        }
      ),
      'date_start' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      ),
      'date_end' => array(
        'required' => true,
        'sanitize_callback' => function($param) {
          return sanitize_text_field($param);
        }
      )
    ),
    'permission_callback' => function() {
      return _s_is_user_role('asset');
    }
  ) );

  register_rest_route( 'booking/v1', '/availability', array(
    'methods' => 'DELETE',
    'callback' => '_s_rest_endpoint_availability_delete',
    'args' => array(
      'user_id' => array(
        'default' => get_current_user_id(),
        'required' => true,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric($param);
        },
        'sanitize_callback' => function($param) {
          return (int) sanitize_text_field($param);
        }
      ),
      'post_id' => array(
        'required' => true,
        'validate_callback' => function($param, $request, $key) {
          return is_numeric($param);
        },
        'sanitize_callback' => function($param) {
          return (int) sanitize_text_field($param);
        }
      )
    ),
    'permission_callback' => function() {
      return _s_is_user_role('asset');
    }
  ) );
} );

function _s_rest_endpoint_availability_get( $request ) {
  $availability_data = array();

  // Build Query
  $args = array(
    'post_type'      => '_s_asset_schedules',
    'post_status'    => array('draft'),
    'posts_per_page' => $request['per_page'],
    'paged'          => $request['page'],
    'author'         => get_current_user_id(),
    'order'          => 'ASC',
    'orderby'        => 'meta_value_num',
    'meta_key'       => 're_date_start_unix',
    'fields'         => 'ids'
  );

  $q = new WP_Query($args);

  if ($q->have_posts()) :
    while ($q->have_posts()) : $q->the_post();
      $availability_data[] = array(
        'date_start'       => get_post_meta(get_the_ID(), 're_date_start', true),
        'date_end'         => get_post_meta(get_the_ID(), 're_date_end', true),
        'asset_request_id' => get_post_meta(get_the_ID(), 're_asset_request_id', true),
        'post_id'          => get_the_ID(),
      );
    endwhile;
    wp_reset_postdata();
  endif;

  // Response
  $response = rest_ensure_response($availability_data);
  
  // Headers
  $response->header('X-WP-Total', $q->found_posts);
  $response->header('X-WP-TotalPages', $q->max_num_pages);

  return $response;
}

function _s_rest_endpoint_availability_create( $request ) {
  // Validate input data
  $date_start = $request['date_start'];
  $date_end   = $request['date_end'];
  
  // Create the post
  $post_id = _s_set_availability($date_start, $date_end);

  // Verify post insertion
  if (is_wp_error($post_id)) {
    return $post_id;
  }

  // Build response
  $response = array();
  $response['post_id']    = $post_id;
  $response['date_start'] = $date_start;
  $response['date_end']   = $date_end;

  return rest_ensure_response($response);
}

function _s_rest_endpoint_availability_delete( $request ) {
  $post_id = $request['post_id'];

  // Delete the post
  $post_result = _s_delete_availability($post_id);

  // Verify post insertion
  if (!$post_result) {
    return new WP_Error('database_error', 'Unable to complete request. Please try again.');
  }

  // Build response
  $response = array();
  $response['post_id'] = $post_result;
              
  return rest_ensure_response($response);
}

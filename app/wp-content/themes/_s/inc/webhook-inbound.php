<?php

function _s_webhook_inbound($data, $nonce) {
  // Verify nonce
  if ( ! wp_verify_nonce($nonce, 'nonce_webhook_inbound') ):
    $res = array(
      'http_status' => 400,
      'text_status' => 'Unable to complete request. Nonce incorrect.'
    );
    echo json_encode($res);
    exit();
  endif;

  // Parse JSON response

  // action (accept||reject)
  // user id
  // booking id
  // timestamp

  // update Asset Request post

  // if (accept)
    // update Asset's Availability to Booking Request's dates
    // email Asset
    // email 

  return;
}

/**
 * Collects GET URL parameters and POSTs them to the appropriate URL via AJAX
 */
function _s_get_request_router_ajax($nonce_get_request) {
  // Verify nonce
  if ( ! wp_verify_nonce($nonce_get_request, 'nonce_get_request') ):
    $res = array(
      'http_status' => 400,
      'text_status' => 'Unable to complete request. Nonce incorrect.'
    );
    echo json_encode($res);
    exit();
  endif;

  // Get the URL parameters
  $action  = filter_input(INPUT_GET, 'action',  FILTER_SANITIZE_STRING);
  $user_id = filter_input(INPUT_GET, 'user_id', FILTER_SANITIZE_STRING);
  $post_id = filter_input(INPUT_GET, 'post_id', FILTER_SANITIZE_STRING);
  $nonce   = filter_input(INPUT_GET, 'nonce',   FILTER_SANITIZE_STRING);

  // Verify fields
  if (empty($action) || empty($user_id) || empty($post_id) || empty($nonce)) {
    wp_die('Invalid Arguments');
  }

  // Verify $action
  if ($action === 'accept') {
    $ajax_action = '_s_booking_request_link_accept_event';
  }
  elseif ($action === 'reject') {
    $ajax_action = '_s_booking_request_link_reject_event';
  }
  else {
    wp_die('Invalid Action');
  }

  $url = admin_url('admin-ajax.php');
  $fields = array(
    'action'      => $action,
    'ajax_action' => $ajax_action,
    'user_id'     => $user_id,
    'post_id'     => $post_id,
    'nonce'       => $nonce,
    'url'         => $url
  );
  
  return json_encode($fields);
}

/**
 * Collects GET URL parameters and POSTs them to the appropriate URL 
 */
function _s_get_request_router_curl($nonce_get_request) {
  // Verify nonce
  if ( ! wp_verify_nonce($nonce_get_request, 'nonce_get_request') ):
    $res = array(
      'http_status' => 400,
      'text_status' => 'Unable to complete request. Nonce incorrect.'
    );
    echo json_encode($res);
    exit();
  endif;

  // Get the URL parameters
  $action  = filter_input(INPUT_GET, 'action',  FILTER_SANITIZE_STRING);
  $user_id = filter_input(INPUT_GET, 'user_id', FILTER_SANITIZE_STRING);
  $post_id = filter_input(INPUT_GET, 'post_id', FILTER_SANITIZE_STRING);
  $nonce   = filter_input(INPUT_GET, 'nonce',   FILTER_SANITIZE_STRING);

  // Verify fields
  if (empty($action) || empty($user_id) || empty($post_id) || empty($nonce)) {
    wp_die('Invalid Arguments');
  }

  // Verify $action
  if ($action === 'accept') {
    $ajax_action = '_s_booking_request_link_accept_event';
  }
  elseif ($action === 'reject') {
    $ajax_action = '_s_booking_request_link_reject_event';
  }
  else {
    wp_die('Invalid Action');
  }

  $url = admin_url('admin-ajax.php');
  $fields = array(
    'action'      => $action,
    'ajax_action' => $ajax_action,
    'user_id'     => $user_id,
    'post_id'     => $post_id,
    'nonce'       => $nonce,
    'url'         => $url
  );
  $fields_string = http_build_query($fields);

  // Create POST request
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
  // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // follow Location: headers

  // Execute POST
  $result = curl_exec($ch);

  $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

  // Close connection
  curl_close($ch);

  // Redirect to simple "status" page
  return $status;
}

(function(exports) {
  'use strict';

  class Utilities {

    /**
     * Make a GET request using Fetch
     *
     * @param {String} url the url
     * @param {Object} data the data to POST
     * @return {Object} Promise object
     */
    static async ajaxGet(url = '', data = {}) {
      let fullUrl = url + '?' + this.buildQuery(data);
      const res = await fetch(fullUrl, {
        method: 'GET',
        // mode: 'same-origin',
        // cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
          'X-WP-Nonce': _s_utilities.rest_nonce
        }
      });
      // console.log('res', res);
      return {
        'headers' : res.headers,
        'json'    : await res.json()
      }
    }

    /**
     * Make a POST request using Fetch
     *
     * @param {String} url the url
     * @param {Object} data the data to POST
     * @return {Object} Promise object
     */
    static async ajaxPostJson(url = '', data = {}) {
      const res = await fetch(url, {
        method: 'POST',
        // mode: 'same-origin',
        // cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
          'X-WP-Nonce': _s_utilities.rest_nonce
        },
        body: JSON.stringify(data)
      });
      // console.log('resAjaxPostJson', res);
      return {
        'headers' : res.headers,
        'json'    : await res.json()
      }
    }

    /**
     * Make a DELETE request using Fetch
     *
     * @param {String} url the url
     * @param {Object} data the data to POST
     * @return {Object} Promise object
     */
    static async ajaxDelete(url = '', data = {}) {
      const res = await fetch(url, {
        method: 'DELETE',
        // mode: 'same-origin',
        // cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
          'X-WP-Nonce': _s_utilities.rest_nonce
        },
        body: JSON.stringify(data)
      });
      // console.log('resAjaxDelete', res);
      return {
        'headers' : res.headers,
        'json'    : await res.json()
      }
    }

    /**
     * Make a DELETE request using Fetch
     *
     * @param {String} url the url
     * @param {Object} data the data to POST
     * @return {Object} Promise object
     */
    static async ajaxDeleteMethod(url = '', data = {}) {
      const res = await fetch(url, {
        method: 'POST',
        // mode: 'same-origin',
        // cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
          'X-WP-Nonce': _s_utilities.rest_nonce,
          'X-HTTP-Method-Override' : 'DELETE'
        },
        body: JSON.stringify(data)
      });
      // console.log('resAjaxDeleteMethod', res);
      return {
        'headers' : res.headers,
        'json'    : await res.json()
      }
    }

    /**
     * Make a POST request using Fetch
     *
     * @param {String} url the url
     * @param {Object} data the data to POST
     * @return {Object} Promise object
     */
    static async ajaxPost(url = '', data = {}) {
      const res = await fetch(url, {
        method: 'POST',
        // mode: 'same-origin',
        // cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        },
        body: this.buildQuery(data)
      });
      return await res.json();
    }

    /**
     * Build a query string from on input object
     *
     * @param {Object} data key-value pairs to transform into a string
     * @return {String} query formated string
     */
    static buildQuery(data) {
      if (typeof data === 'string') return data;

      const query = [];

      for (let key in data) {
        if (data.hasOwnProperty(key)) {
          query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
        }
      }

      return query.join('&');
    }

    static getDateTodayISO() {
      let now       = new Date();
      let year      = now.getFullYear();
      let month     = now.getMonth() + 1; // zero-based
      let day       = now.getDate();
      month         = String(month);
      day           = String(day);
      month         = month.padStart(2, '0');
      day           = day.padStart(2, '0');
      const pieces  = [year, month, day];
      const dateISO = pieces.join('-');

      return dateISO;
    }

  } // Utilities

  exports.Utilities = Utilities;
})(window)

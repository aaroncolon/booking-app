(function(exports) {
  'use strict';

  const Utilities = function() {

    this.getDateTodayISO = function() {
      let now       = new Date();
      let year      = now.getFullYear();
      let month     = now.getMonth() + 1; // zero-based
      let day       = now.getDate();
      month         = String(month);
      day           = String(day);
      month         = month.padStart(2, '0');
      day           = day.padStart(2, '0');
      const pieces  = [year, month, day];
      const dateISO = pieces.join('-');

      return dateISO;
    };

    /**
     * Make a POST request using Fetch
     *
     * @param {String} url the url
     * @param {Object} data the data to POST
     * @return {Object} Promise object
     */
    this.ajaxPost = async function(url = '', data = {}) {
      const res = await fetch(url, {
        method: 'POST',
        // mode: 'same-origin',
        // cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        },
        body: this.buildQuery(data)
      });
      return await res.json();
    };

    /**
     * Build a query string from on input object
     *
     * @param {Object} data key-value pairs to transform into a string
     * @return {String} query formated string
     */
    this.buildQuery = function(data) {
      if (typeof data === 'string') return data;

      const query = [];

      for (let key in data) {
        if (data.hasOwnProperty(key)) {
          query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
        }
      }

      return query.join('&');
    };

  };

  exports.Utilities = Utilities;
})(window)

(function() {
  'use strict';

  const assetView = {

    init: function () {
      this.cacheDom();
      this.bindEvents();
    },

    render: function() {

    },

    cacheDom: function() {
      // Booking Requests
      this.$bookingRequestsTable = jQuery('#booking-requests__table tbody');

      // Availability Summary
      this.$availabilitySummaryTable = jQuery('#availability-summary__table tbody');

      this.$abilitiesTable = jQuery('#user-asset-types__table tbody');

      // JS Templates
      this.fnBookingRequestsRow_ = wp.template('booking-requests-row');
      this.fnAvailabilityRow_ = wp.template('availability-row');
      this.fnAbilitiesRow_ = wp.template('abilities-row');

      // Nonces
      this.$nonceBookingAccept = jQuery('#nonce-booking-request-accept');
      this.$nonceBookingReject = jQuery('#nonce-booking-request-reject');
    },

    bindEvents: function() {
      // Booking Requests 
      events.on('getBookingRequestsDone', this.handleGetBookingRequestsDone, this);

      // Availability Summary
      events.on('getAvailabilityDone', this.handleGetAvailabilityDone, this);
      events.on('handleDeleteAvailabilityDone', this.handleDeleteAvailabilityDone, this);

      // Abilities
      events.on('getAbilitiesDone', this.handleGetAbilitiesDone, this);
      events.on('setAbilitiesDone', this.handleSetAbilities, this);
      events.on('deleteAbilitiesDone', this.handleDeleteAbilities, this);
    },


    /**
     * Booking Requests
     */
    handleGetBookingRequestsDone: function(data) {
      if (data.length) {
        this.setBookingRequestsSummary(data);  
      } else {
        this.resetBookingRequestsSummary();  
      }
    },

    setBookingRequestsSummary: function(data) {
      this.resetBookingRequestsSummary();

      for (let i = 0; i < data.length; i++) {
        data[i].user_type = _s_utilities.user_type;
        this.$bookingRequestsTable.append( this.fnBookingRequestsRow_(data[i]) );
      }
    },

    resetBookingRequestsSummary: function() {
      this.$bookingRequestsTable.empty();
    },


    /**
     * Availability
     */
    handleGetAvailabilityDone: function (data) {
      this.setAvailabilitySummary(data);
    },

    setAvailabilitySummary: function(data) {
      this.resetAvailabilitySummary();
      
      for (let i = 0; i < data.length; i++) {
        this.$availabilitySummaryTable.append(this.fnAvailabilityRow_(data[i]));
      }
    },

    resetAvailabilitySummary: function() {
      this.$availabilitySummaryTable.empty();
    },


    /**
     * Asset Abilities
     */
    handleGetAbilitiesDone: function(data) {
      this.renderAbilities(data);
    },

    handleSetAbilities: function(data, e) {
      this.renderAbilities(data);
    },

    handleDeleteAbilities: function(data, e) {
      jQuery(e.currentTarget).closest('tr').remove();
    },

    renderAbilities: function(data) {
      if (data.length) {
        for (let i = 0; i < data.length; i++) {
          // pass data to template function
          this.$abilitiesTable.append( this.fnAbilitiesRow_( data[i] ) );
        }
      }
    },

    resetAbilities: function() {
      this.$abilitiesTable.empty();
    }

  };

  assetView.init();
})()

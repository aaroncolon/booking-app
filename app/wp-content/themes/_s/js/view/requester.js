(function() {
  "use strict";

  const requesterView = {

    _data: {
      'copy': {
        'assetDefault': 'Please request an asset.'
      }
    },

    init: function() {
      this.cacheDom();
      this.bindEvents();
    },

    cacheDom: function() {
      // Request Asset Summary
      this.$requestSummary      = jQuery('#request-summary');
      this.$requestSummaryMsg   = this.$requestSummary.find('.request-summary__message');
      this.$requestSummaryInner = this.$requestSummary.find('.request-summary__summary');
      this.$requestSummaryTable = this.$requestSummary.find('.request-summary__table');
      this.$requestSummaryTableBody = this.$requestSummary.find('#request-summary__table tbody');

      // Form Confirm
      this.$formConfirm = jQuery('#form-request-asset-complete');
      this.$formConfirmSubmit = this.$formConfirm.find('input[type=submit]');

      // Booking Requests
      this.$bookingRequestsTable = jQuery('#booking-requests__table tbody');
      this.$nonceGetBookingRequests = jQuery('#nonce-get-booking-requests');

      // JS Templates
      this.fnBookingRequestSummary_ = wp.template( 'booking-request-summary-row' );
      this.fnBookingRequests_ = wp.template( 'booking-requests-row' );
    },

    bindEvents: function() {
      events.on('renderRequestSummary', this.renderRequestSummary, this);
      events.on('renderBookingRequests', this.renderBookingRequests, this);
    },

    render: function() {

    },


    /**
     * Request Summary
     */
    renderRequestSummary: function(copy, data) {
      this.setRequestSummary(copy, data);
    },

    setRequestSummary: function(message, data) {
      console.log('message', message);
      this.resetRequestSummary();

      data.asset_type = (data.asset_type) ? data.asset_type : 'n/a';
      data.experience_level = (data.experience_level) ? data.experience_level : 'n/a';
      data.asset_email = (data.asset_email) ? data.asset_email : 'n/a';

      this.setRequestSummaryMsg(message);
      this.setRequestSummaryTableBody(this.fnBookingRequestSummary_(data));
      this.setRequestSummaryButton(data);
      this.show(this.$requestSummaryInner);
    },

    setRequestSummaryMsg: function(message) {
      this.resetRequestSummaryMsg();
      const p = document.createElement('p');
      const t = document.createTextNode(message);
      p.appendChild(t);
      this.$requestSummaryMsg.append(p);
    },

    resetRequestSummaryMsg: function() {
      this.$requestSummaryMsg.empty();
    },

    setRequestSummaryTableBody: function(template) {
      this.$requestSummaryTableBody.append(template);
    },

    setRequestSummaryButton: function(data) {
      // show / hide form submit button
      if (!data.available_assets) {
        this.$formConfirmSubmit.prop('disabled', true);
        this.hide(this.$formConfirmSubmit);
      } else {
        this.$formConfirmSubmit.prop('disabled', false);
        this.show(this.$formConfirmSubmit);
      }
    },

    resetRequestSummary: function() {
      this.hide(this.$requestSummaryInner);
      this.resetRequestSummaryMsg();
      this.setRequestSummaryMsg(this._data.copy.assetDefault);
      this.$requestSummaryTableBody.empty();
    },


    /**
     * Booking Requests
     */
    renderBookingRequests: function(data) {
      this.setBookingRequests(data);
    },

    setBookingRequests: function(data) {
      this.resetBookingRequests();

      for (let i = 0; i < data.length; i++) {
        this.$bookingRequestsTable.append( this.fnBookingRequests_( data[i] ) );
      }
    },

    resetBookingRequests: function() {
      this.$bookingRequestsTable.empty();
    },


    /**
     * Utilities
     */
    show: function($el) {
      $el.css('display', 'block');
      $el.css('opacity', '1');
    },

    hide: function($el) {
      $el.css('opacity', '0');
      $el.css('display', 'none');
    }

  };

  requesterView.init();

})()

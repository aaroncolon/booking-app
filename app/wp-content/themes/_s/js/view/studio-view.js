(function(exports) {
  'use strict';

  const requesterView = {

    init: function() {

    },

    cacheDom: function() {

    },

    bindEvents: function() {
      events.on('assetAvailable', this.handleAssetAvailable, this);
      events.on('assetUnavailable', this.handleAssetUnavailable, this);
      events.on('bookingRequestCreated', this.handleBookingRequestCreated, this);
      events.on('getBookingRequestsDone', this.handleGetBookingRequestsDone, this);
    },

    render: function() {
      this.getBookingRequests();
    }

  };

  requesterView.init();

})(window)

(function(){
  "use strict";

  const asset = {

    init: function() {
      this.cacheDom();
      this.bindEvents();
      this.render();
    },

    render: function() {
      this.setMinDateToday();
      this.getBookingRequests();
      this.getAvailability();
      this.getAbilities();
    },

    cacheDom: function() {
      // Booking Requests
      this.$bookingRequestsTable = jQuery('#booking-requests__table tbody');

      // Availability Form
      this.$availabilityForm  = jQuery('#form-availability');
      this.$dateStart         = this.$availabilityForm.find('#date-start');
      this.$dateEnd           = this.$availabilityForm.find('#date-end');
      
      // Availability Summary
      this.$availabilitySummaryTable = jQuery('#availability-summary__table tbody');

      // Asset Types
      this.$assetTypeForm   = jQuery('#form-user-asset-types');
      // this.$assetType       = this.$assetTypeForm.find('#asset-type');
      // this.$experienceLevel = this.$assetTypeForm.find('#experience-level'); 
      this.$assetTypesTable = jQuery('#user-asset-types__table tbody');

      // Nonces
      // this.$nonceBookingRequestsGet   = jQuery('#nonce-booking-requests-get');
      // this.$nonceBookingRequestAccept = jQuery('#nonce-booking-request-accept');
      // this.$nonceBookingRequestReject = jQuery('#nonce-booking-request-reject');
      // this.$nonceAvailability         = this.$availabilityForm.find('#nonce-availability');
      // this.$nonceAssetTypes           = this.$assetTypeForm.find('#nonce-user-asset-types');
    },

    bindEvents: function() {
      // Booking Requests
      this.$bookingRequestsTable.on('click', 'button', null, (e) => { this.handleBookingRequestAction(e) });

      // Availability
      this.$dateStart.on('change', (e) => { this.handleDateChange(e) });
      this.$dateEnd.on('change', (e) => { this.handleDateChange(e) });
      this.$availabilityForm.on('submit', (e) => { this.setAvailability(e) });
      this.$availabilitySummaryTable.on('click', 'button', null, (e) => { this.deleteAvailability(e) }); // Delegation

      // Asset Types
      this.$assetTypeForm.on('submit', (e) => { this.setAbilities(e) });
      this.$assetTypesTable.on('click', 'a[data-action="delete"]', null, (e) => { this.deleteAbilities(e) }); // Delegation
    },


    /**
     * Booking Requests
     */
    handleBookingRequestAction: function(e) {
      e.preventDefault();

      if (e.target.dataset.action === 'accept') {
        this.acceptBookingRequest(e);
      } else if (e.target.dataset.action === 'reject') {
        this.rejectBookingRequest(e);
      }
    },

    getBookingRequests: function() {
      events.trigger('getBookingRequests');
    },

    acceptBookingRequest: function(e) {
      events.trigger('acceptBookingRequest', e);
    },

    rejectBookingRequest: function(e) {
      events.trigger('rejectBookingRequest', e);
    },


    /**
     * Availability
     */
    getAvailability: function() {
      events.trigger('getAvailability');
    },

    setAvailability: function(e) {
      e.preventDefault();
      events.trigger('setAvailability');
    },

    deleteAvailability: function(e) {
      e.preventDefault();
      events.trigger('deleteAvailability', e);
    },

    handleDateChange: function(e) {
      if (e.target.id === 'date-start') {
        // reset $dateEnd val
        this.$dateEnd.val('');
        // change #date-end min to date-start val
        this.$dateEnd.prop('min', e.target.value);
      }
    },

    setMinDateToday: function() {
      const date = Utilities.getDateTodayISO();
      this.$dateStart.prop('min', date);
      this.$dateEnd.prop('min', date);
    },


    /**
     * Abilities
     */
    getAbilities: function() {
      events.trigger('getAbilities');
    },

    setAbilities: function(e) {
      e.preventDefault();
      events.trigger('setAbilities');
    },

    deleteAbilities: function(e) {
      e.preventDefault();
      events.trigger('deleteAbilities', e);
    }

  };

  asset.init();

})()

(function(exports) {
  "use strict";

  const requester = {

    _data: {
      'copy': {
        'assetDefault'     : 'Please request an asset.',
        'assetAvailable'   : 'Match found. Please confirm your request.',
        'assetUnavailable' : 'No matches available at this time.',
        'requestAssetComplete' : 'Your asset request has been received. The asset you requested must now accept or reject your request. We will notify you of the results shortly.'
      },
      'request' : {}
    },

    init: function() {
      this.cacheDom();
      this.bindEvents();
      this.render();
    },

    cacheDom: function() {
      // Request Asset Form
      this.$form = jQuery('#form-request-asset');
      this.$assetNewSpecific = this.$form.find('input[name="asset-new-specific"]');
      this.$assetEmail       = this.$form.find('#asset-email');
      this.$assetType        = this.$form.find('#asset-type');
      this.$experienceLevel  = this.$form.find('#experience-level');
      this.$dateStart        = this.$form.find('#date-start');
      this.$dateEnd          = this.$form.find('#date-end');
      this.$nonce            = this.$form.find('#nonce-request-asset');
      this.$submit           = this.$form.find('#submit-request-asset');



      // Request Asset Summary
      this.$requestSummary      = jQuery('#request-summary');
      this.$requestSummaryMsg   = this.$requestSummary.find('.request-summary__message');
      this.$requestSummaryInner = this.$requestSummary.find('.request-summary__summary');
      this.$requestSummaryTable = this.$requestSummary.find('.request-summary__table');
      this.$requestSummaryTableBody = this.$requestSummary.find('#request-summary__table tbody');

      this.$formConfirm = jQuery('#form-request-asset-complete');
      this.$formConfirmSubmit = this.$formConfirm.find('input[type=submit]');



      // Booking Requests
      this.$bookingRequestsTable = jQuery('#booking-requests__table tbody');
      this.$nonceGetBookingRequests = jQuery('#nonce-get-booking-requests');

    }, 

    bindEvents: function() {
      // Request Asset Form
      this.$assetNewSpecific.on('change', (e) => { this.setAssetNewSpecific(e) });
      this.$dateStart.on('change', (e) => { this.handleDateChange(e) });
      this.$dateEnd.on('change', (e) => { this.handleDateChange(e) });
      this.$form.on('submit', (e) => { this.requestAsset(e) });
      this.$formConfirm.on('submit', (e) => { this.requestAssetComplete(e) });

      events.on('assetAvailable', this.handleAssetAvailable, this);
      events.on('assetUnavailable', this.handleAssetUnavailable, this);
      events.on('bookingRequestCreated', this.handleBookingRequestCreated, this);
      events.on('getBookingRequestsDone', this.handleGetBookingRequestsDone, this);
    },

    render: function() {
      this.setMinDateToday();
      this.getBookingRequests();
    },

    setMinDateToday: function() {
      const date = Utilities.getDateTodayISO();
      this.$dateStart.prop('min', date);
      this.$dateEnd.prop('min', date);
    },

    setAssetNewSpecific: function(e) {
      if (typeof e !== 'undefined' && e.target.value === 'specific') {
        // show specific-related fields
        // show asset-email
        this.$assetEmail.prop('disabled', false);
        this.$assetEmail.parent().css('display', 'block');

        // hide asset-type
        this.$assetType.val('');
        this.$assetType.prop('disabled', true);
        this.$assetType.parent().css('display', 'none');

        // hide experience level
        this.$experienceLevel.val('');
        this.$experienceLevel.prop('disabled', true);
        this.$experienceLevel.parent().css('display', 'none');
      }
      else {
        // hide specific-related fields
        // hide asset-email
        this.$assetEmail.val('');
        this.$assetEmail.prop('disabled', true);
        this.$assetEmail.parent().css('display', 'none');

        // show asset-type
        this.$assetType.prop('disabled', false);
        this.$assetType.parent().css('display', 'block');

        // show experience level
        this.$experienceLevel.prop('disabled', false);
        this.$experienceLevel.parent().css('display', 'block');
      }
    },

    handleDateChange: function(e) {
      if (e.target.id === 'date-start') {
        // reset $dateEnd val
        this.$dateEnd.val('');
        // change #date-end min to date-start val
        this.$dateEnd.prop('min', e.target.value);
      }
    },

    requestAsset: function(e) {
      e.preventDefault();

      this.resetRequestData();

      const args = {
        'asset_email'      : this.$assetEmail.val(),
        'asset_type'       : this.$assetType.val(),
        'experience_level' : this.$experienceLevel.val(),
        'date_start'       : this.$dateStart.val(),
        'date_end'         : this.$dateEnd.val()
      };

      console.log("args", args);

      events.trigger('requestAsset', args);
    },

    requestAssetComplete: function(e) {
      e.preventDefault();

      const args = {
        'asset_email'      : this._data.request.asset_email,
        'asset_id'         : this._data.request.available_assets[0].asset_id,
        'asset_type'       : this._data.request.asset_type,
        'experience_level' : this._data.request.experience_level,
        'date_start'       : this._data.request.date_start,
        'date_end'         : this._data.request.date_end,
        'available_assets' : this._data.request.available_assets
      };

      events.trigger('requestAssetComplete', args);
    },

    handleAssetAvailable: function(data) {
      // save data locally
      this.setRequestData(data);
      events.trigger('renderRequestSummary', this._data.copy.assetAvailable, data);
    },

    handleAssetUnavailable: function(data) {
      // reset local data
      this.resetRequestData();
      events.trigger('renderRequestSummary', this._data.copy.assetUnavailable, data);
    },

    setRequestData: function(data) {
      for (const prop in data) {
        this._data.request[prop] = data[prop];
        let input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('name', prop);
        input.setAttribute('value', JSON.stringify(data[prop]));
        this.$formConfirm.prepend(input);
      }
    },

    resetRequestData: function() {
      console.log('resetRequestData');
      this._data.request = null;
      this._data.request = {};
      this.$formConfirm.find('input[type=hidden]').remove();
    },

    handleBookingRequestCreated: function(data) {
      this.resetRequestData();
      this.hide(this.$formConfirmSubmit);
      events.trigger('renderRequestSummary', this._data.copy.requestAssetComplete, data);
    },


    /**
     * Booking Requests
     */
    getBookingRequests: function() {
      events.trigger('getBookingRequests');
    },

    handleGetBookingRequestsDone: function(data) {
      events.trigger('renderBookingRequests', data);
    },



    /**
     * Utilities
     */
    show: function($el) {
      $el.css('display', 'block');
      $el.css('opacity', '1');
    },

    hide: function($el) {
      $el.css('opacity', '0');
      $el.css('display', 'none');
    }

  };

  requester.init();

})(window)

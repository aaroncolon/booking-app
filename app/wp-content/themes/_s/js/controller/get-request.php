<script>

  const _re_data = <?php echo $query_data ?>;

  (function() {
    'use strict';

    const getRequest = {

      init: function() {
        this.doAjax();
      },

      doAjax: function() {
        if (typeof _re_data == 'undefined' || ! _re_data) {
          return;
        }
        else if (_re_data.action === 'accept') {
          this.doAjaxAccept();
        }
        else if (_re_data.action === 'reject') {
          this.doAjaxReject();
        }
      },

      doAjaxAccept: function() {
        jQuery.ajax({
          url      : _re_data.url,
          type     : 'POST',
          dataType : 'json',
          data: {
            'action'  : _re_data.ajax_action,
            'post_id' : _re_data.post_id,
            'user_id' : _re_data.user_id,
            'nonce'   : _re_data.nonce
          }
        })
        .done((data) => {
          console.log('accept done', data);
        })
        .fail(() => {
          console.log('accept fail', arguments);
        });
      },

      doAjaxReject: function() {
        jQuery.ajax({
          type: 'post',
          dataType: 'json',
          url: _re_data.url,
          data: {
            'action'  : _re_data.ajax_action,
            'post_id' : _re_data.post_id,
            'user_id' : _re_data.user_id,
            'nonce'   : _re_data.nonce
          }
        })
        .done((data) => {
          console.log('reject done', data);
        })
        .fail(() => {
          console.log('reject fail', arguments);
        });
      }

    }; // getRequest

    getRequest.init();
  })()

</script>

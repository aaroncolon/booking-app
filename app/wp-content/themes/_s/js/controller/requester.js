(function(exports) {
  "use strict";

  const requester = {
    _data: {
      'copy': {
        'assetDefault'     : 'Please request an asset.',
        'assetAvailable'   : 'Match found. Please confirm your request.',
        'assetUnavailable' : 'No matches available at this time.',
        'requestAssetComplete' : 'Your asset request has been received. The asset you requested must now accept or reject your request. We will notify you of the results shortly.'
      },
      'request' : {}
    },

    init: function() {
      this.cacheDom();
      this.bindEvents();
      this.render();
    },

    cacheDom: function() {
      // Request Asset Form
      this.$form = jQuery('#form-request-asset');
      this.$assetNewSpecific = this.$form.find('input[name="asset-new-specific"]');
      this.$assetEmail       = this.$form.find('#asset-email');
      this.$assetType        = this.$form.find('#asset-type');
      this.$experienceLevel  = this.$form.find('#experience-level');
      this.$dateStart        = this.$form.find('#date-start');
      this.$dateEnd          = this.$form.find('#date-end');
      this.$nonce            = this.$form.find('#nonce-request-asset');
      this.$submit           = this.$form.find('#submit-request-asset');

      // Request Asset Summary
      this.$requestSummary      = jQuery('#request-summary');
      this.$requestSummaryMsg   = this.$requestSummary.find('.request-summary__message');
      this.$requestSummaryInner = this.$requestSummary.find('.request-summary__summary');
      this.$requestSummaryTable = this.$requestSummary.find('.request-summary__table');
      this.$requestSummaryTableBody = this.$requestSummary.find('#request-summary__table tbody');
      this.$formConfirm = jQuery('#form-request-asset-complete');
      this.$formConfirmSubmit = this.$formConfirm.find('input[type=submit]');

      // Booking Requests
      this.$bookingRequestsTable = jQuery('#booking-requests__table tbody');
      this.$nonceGetBookingRequests = jQuery('#nonce-get-booking-requests');
    }, 

    bindEvents: function() {
      this.$assetNewSpecific.on('change', (e) => { this.setAssetNewSpecific(e) });
      this.$dateStart.on('change', (e) => { this.handleDateChange(e) });
      this.$dateEnd.on('change', (e) => { this.handleDateChange(e) });
      this.$form.on('submit', (e) => { this.requestAsset(e) });
      this.$formConfirm.on('submit', (e) => { this.requestAssetComplete(e) });

      events.on('assetAvailable', this.handleAssetAvailable, this);
      events.on('assetUnavailable', this.handleAssetUnavailable, this);
      events.on('bookingRequestCreated', this.handleBookingRequestCreated, this);
      events.on('getBookingRequestsDone', this.handleGetBookingRequestsDone, this);
    },

    render: function() {
      this.setMinDateToday();
      this.getBookingRequests();
    },

    requestAsset: async function(e) {
      e.preventDefault();

      this.resetRequestData();
      this.resetRequestSummary();

      try {
        const data = await Utilities.ajaxPost(_s_requester.ajax_url, {
          'action'           : '_s_request_asset_event',
          'asset_email'      : this.$assetEmail.val(),
          'asset_type'       : this.$assetType.val(),
          'experience_level' : this.$experienceLevel.val(),
          'date_start'       : this.$dateStart.val(),
          'date_end'         : this.$dateEnd.val(),
          'nonce'            : this.$nonce.val()
        });

        console.log('data', data);

        if (data.data.available_assets) {
          events.trigger('assetAvailable', data.data);
        } else {
          events.trigger('assetUnavailable', data.data);
        }
      } catch(err) {
        console.log('requestAsset fail', err);
      }

      // ajax...
        // check assets
        // true
          // notification sent
          // awaiting acceptance
        // false
          // check for next best assets
            // true
              // email requester
                // next best asset requested
    },

    /**
     * @TODO Redirect to WooCommerce payment page
     * @TODO Upon successful payment, complete booking
     * @TODO Must assign user status to prevent double booking
     */
    requestAssetComplete: async function(e) {
      e.preventDefault();

      try {
        const data = await Utilities.ajaxPost(_s_requester.ajax_url, {
          'action'           : '_s_request_asset_complete_event',
          'asset_email'      : this._data.request.asset_email,
          'asset_id'         : this._data.request.available_assets[0].asset_id,
          'asset_type'       : this._data.request.asset_type,
          'experience_level' : this._data.request.experience_level,
          'date_start'       : this._data.request.date_start,
          'date_end'         : this._data.request.date_end,
          'available_assets' : this._data.request.available_assets,
          'nonce'            : this.$nonce.val()
        });

        console.log('requestAssetComplete data', data);

        if (data.data.post_id) {
          events.trigger('bookingRequestCreated', data.data);
        } else {
          events.trigger('bookingRequestFailed', data.data);
        }
      } catch(err) {
        console.log('requestAssetComplete fail', err);
        events.trigger('bookingRequestFailed', data.data);
      }

      // jQuery.ajax({
      //   type: 'POST',
      //   dataType: 'json',
      //   url: _s_requester.ajax_url,
      //   context: this,
      //   data: {
      //     'action'           : '_s_request_asset_complete_event',
      //     'asset_email'      : this._data.request.asset_email,
      //     'asset_id'         : this._data.request.available_assets[0].asset_id,
      //     'asset_type'       : this._data.request.asset_type,
      //     'experience_level' : this._data.request.experience_level,
      //     'date_start'       : this._data.request.date_start,
      //     'date_end'         : this._data.request.date_end,
      //     'available_assets' : this._data.request.available_assets,
      //     'nonce'            : this.$nonce.val()
      //   }
      // })
      // .done((data, textStatus, jqXHR) => {
      //   console.log('requestAssetComplete data', data);

      //   if (data.post_id) {
      //     events.trigger('bookingRequestCreated', data);
      //   } else {
      //     events.trigger('bookingRequestFailed', data);
      //   }
      // })
      // .fail((jqXHR, textStatus, errorThrown) => {
      //   console.log('requestAssetComplete fail', arguments);
      //   events.trigger('bookingRequestFailed', data);
      // });
    },

    handleAssetAvailable: function(data) {
      // save data locally
      this.setRequestData(data);
      this.setRequestSummary(this._data.copy.assetAvailable, data);
    },

    handleAssetUnavailable: function(data) {
      // reset local data
      this.resetRequestData();
      this.setRequestSummary(this._data.copy.assetUnavailable, data);
    },

    handleBookingRequestCreated: function(data) {
      this.resetRequestData();
      this.hide(this.$formConfirmSubmit);
      this.setRequestSummary(this._data.copy.requestAssetComplete, data);
    },

    setRequestSummary: function(message, data) {
      this.resetRequestSummary();
      const asset_type  = (data.asset_type) ? data.asset_type : 'n/a';
      const exp_level   = (data.experience_level) ? data.experience_level : 'n/a';
      const asset_email = (data.asset_email) ? data.asset_email : 'n/a';
      const tr   = document.createElement('tr'),
            td1  = document.createElement('td'),
            td2  = document.createElement('td'),
            td3  = document.createElement('td'),
            td4  = document.createElement('td'),
            td5  = document.createElement('td'),
            td1t = document.createTextNode(asset_type),
            td2t = document.createTextNode(exp_level),
            td3t = document.createTextNode(data.date_start),
            td4t = document.createTextNode(data.date_end),
            td5t = document.createTextNode(asset_email);
      td1.appendChild(td1t);
      td2.appendChild(td2t);
      td3.appendChild(td3t);
      td4.appendChild(td4t);
      td5.appendChild(td5t);
      tr.appendChild(td1);
      tr.appendChild(td2);
      tr.appendChild(td3);
      tr.appendChild(td4);
      tr.appendChild(td5);

      this.setRequestSummaryMsg(message);
      this.setRequestSummaryTableBody(tr);
      this.setRequestSummaryButton(data);
      this.show(this.$requestSummaryInner);
    },

    setRequestSummaryMsg: function(message) {
      this.resetRequestSummaryMsg();
      const p = document.createElement('p');
      const t = document.createTextNode(message);
      p.appendChild(t);
      this.$requestSummaryMsg.append(p);
    },

    resetRequestSummaryMsg: function() {
      this.$requestSummaryMsg.empty();
    },

    setRequestSummaryTableBody: function(el) {
      this.$requestSummaryTableBody.append(el);
    },

    setRequestSummaryButton: function(data) {
      // show / hide form submit button
      if (!data.available_assets) {
        this.$formConfirmSubmit.prop('disabled', true);
        this.hide(this.$formConfirmSubmit);
      } else {
        this.$formConfirmSubmit.prop('disabled', false);
        this.show(this.$formConfirmSubmit);
      }
    },

    resetRequestSummary: function() {
      this.hide(this.$requestSummaryInner);
      this.resetRequestSummaryMsg();
      this.setRequestSummaryMsg(this._data.copy.assetDefault);
      this.$requestSummaryTableBody.empty();
    },


    
    getBookingRequests: async function() {
      try {
        const data = await Utilities.ajaxPost(_s_requester.ajax_url, {
          'action' : '_s_get_booking_requests_event',
          'nonce'  : this.$nonceGetBookingRequests.val()
        });
        console.log('getBookingRequests', data);
        events.trigger('getBookingRequestsDone', data);
      } catch(err) {
        console.log('getBookingRequests fail', err);
      }
    },

    handleGetBookingRequestsDone: function(data) {
      this.setBookingRequests(data.data);
    },

    setBookingRequests: function(data) {
      this.resetBookingRequests();

      for (let i = 0; i < data.length; i++) {
        const tr   = document.createElement('tr'),
              td1  = document.createElement('td'),
              td2  = document.createElement('td'),
              td3  = document.createElement('td'),
              td4  = document.createElement('td'),
              td5  = document.createElement('td'),
              td6  = document.createElement('td'),
              td1t = document.createTextNode(data[i].asset_type),
              td2t = document.createTextNode(data[i].experience_level),
              td3t = document.createTextNode(data[i].date_start),
              td4t = document.createTextNode(data[i].date_end),
              td5t = document.createTextNode(data[i].booking_status),
              btn  = document.createElement('button'),
              btnt = document.createTextNode('Cancel');

        // Button
        btn.setAttribute('id', 'btn-cancel-' + data[i].post_id);
        btn.setAttribute('name', 'btn-cancel-' + data[i].post_id);
        btn.setAttribute('data-post-id', data[i].post_id);
        btn.appendChild(btnt);

        td1.appendChild(td1t);
        td2.appendChild(td2t);
        td3.appendChild(td3t);
        td4.appendChild(td4t);
        td5.appendChild(td5t);
        td6.appendChild(btn);
        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(td4);
        tr.appendChild(td5);
        tr.appendChild(td6);

        this.$bookingRequestsTable.append(tr);
      }
    },

    resetBookingRequests: function() {
      this.$bookingRequestsTable.empty();
    },

    setAssetNewSpecific: function(e) {
      if (typeof e !== 'undefined' && e.target.value === 'specific') {
        // show specific-related fields
        // show asset-email
        this.$assetEmail.prop('disabled', false);
        this.$assetEmail.parent().css('display', 'block');

        // hide asset-type
        this.$assetType.val('');
        this.$assetType.prop('disabled', true);
        this.$assetType.parent().css('display', 'none');

        // hide experience level
        this.$experienceLevel.val('');
        this.$experienceLevel.prop('disabled', true);
        this.$experienceLevel.parent().css('display', 'none');
      }
      else {
        // hide specific-related fields
        // hide asset-email
        this.$assetEmail.val('');
        this.$assetEmail.prop('disabled', true);
        this.$assetEmail.parent().css('display', 'none');

        // show asset-type
        this.$assetType.prop('disabled', false);
        this.$assetType.parent().css('display', 'block');

        // show experience level
        this.$experienceLevel.prop('disabled', false);
        this.$experienceLevel.parent().css('display', 'block');
      }
    },

    handleDateChange: function(e) {
      if (e.target.id === 'date-start') {
        // reset $dateEnd val
        this.$dateEnd.val('');
        // change #date-end min to date-start val
        this.$dateEnd.prop('min', e.target.value);
      }
    },

    setMinDateToday: function() {
      const date = Utilities.getDateTodayISO();
      this.$dateStart.prop('min', date);
      this.$dateEnd.prop('min', date);
    },

    resetFields: function() {

    }, 

    show: function($el) {
      $el.css('display', 'block');
      $el.css('opacity', '1');
    },

    hide: function($el) {
      $el.css('opacity', '0');
      $el.css('display', 'none');
    },





    setRequestData: function(data) {
      for (const prop in data) {
        this._data.request[prop] = data[prop];
        let input = document.createElement('input');
        input.setAttribute('type', 'hidden');
        input.setAttribute('name', prop);
        input.setAttribute('value', JSON.stringify(data[prop]));
        this.$formConfirm.prepend(input);
      }
    }, 

    resetRequestData: function() {
      console.log('resetRequestData');
      this._data.request = null;
      this._data.request = {};
      this.$formConfirm.find('input[type=hidden]').remove();
    }

  };

  requester.init();

})(window)

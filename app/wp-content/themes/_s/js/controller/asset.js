(function(){
  "use strict";

  const asset = {

    init: function() {
      this.cacheDom();
      this.bindEvents();
      this.render();
    },

    render: function() {
      this.setMinDateToday();
      this.getBookingRequests();
      this.getAvailability();
    },

    cacheDom: function() {
      // Booking Requests
      this.$bookingRequestsTable = jQuery('#booking-requests__table tbody');

      // Availability Form
      this.$availabilityForm  = jQuery('#form-availability');
      this.$dateStart         = this.$availabilityForm.find('#date-start');
      this.$dateEnd           = this.$availabilityForm.find('#date-end');
      
      // Availability Summary
      this.$availabilitySummaryTable = jQuery('#availability-summary__table tbody');

      // Asset Types
      this.$assetTypeForm   = jQuery('#form-user-asset-types');
      this.$assetType       = this.$assetTypeForm.find('#asset-type');
      this.$experienceLevel = this.$assetTypeForm.find('#experience-level'); 
      this.$assetTypesTable = jQuery('#user-asset-types__table tbody');

      // Nonces
      this.$nonceBookingRequestsGet   = jQuery('#nonce-booking-requests-get');
      this.$nonceBookingRequestAccept = jQuery('#nonce-booking-request-accept');
      this.$nonceBookingRequestReject = jQuery('#nonce-booking-request-reject');
      this.$nonceAvailability         = this.$availabilityForm.find('#nonce-availability');
      this.$nonceAssetTypes           = this.$assetTypeForm.find('#nonce-user-asset-types');
    },

    bindEvents: function() {
      // Booking Requests
      this.$bookingRequestsTable.on('click', 'button', null, (e) => { this.handleBookingRequestAction(e) });
      events.on('acceptBookingRequestDone', this.getBookingRequests, this);
      events.on('acceptBookingRequestDone', this.getAvailability, this);
      events.on('rejectBookingRequestDone', this.getBookingRequests, this);

      // Availability
      this.$dateStart.on('change', (e) => { this.handleDateChange(e) });
      this.$dateEnd.on('change', (e) => { this.handleDateChange(e) });
      this.$availabilityForm.on('submit', (e) => { this.setAvailability(e) });
      this.$availabilitySummaryTable.on('click', 'button', null, (e) => { this.handleAvailabilityDelete(e) }); // Delegation

      events.on('setAvailabilityDone', this.getAvailability, this);
      events.on('deleteAvailabilityDone', this.getAvailability, this);

      // Asset Types
      this.$assetTypeForm.on('submit', (e) => { this.setAssetType(e) });
      this.$assetTypesTable.on('click', 'a[data-action="delete"]', null, (e) => { this.handleAssetTypeDelete(e) }); // Delegation
      events.on('setAssetTypeDone', this.handleSetAssetTypeDone, this);
    },


    /**
     * Booking Requests
     */
    getBookingRequests: async function() {
      let _this = this;
      let data  = [];
      let page  = 1;

      async function req() {
        try {
          const res = await Utilities.ajaxPost(_s_asset.ajax_url, {
            'action'       : '_s_get_user_booking_requests_event',
            'paged'        : page,
            'nonce'        : _this.$nonceBookingRequestsGet.val(),
            'nonce_accept' : _this.$nonceBookingRequestAccept.val(),
            'nonce_reject' : _this.$nonceBookingRequestReject.val()
          });

          if (res.data.paged < res.data.max_num_pages) {
            // save data
            data = data.concat(res.data.booking_data);

            // increment page
            page++;

            // recurse
            req();
          }
          else {
            data = data.concat(res.data.booking_data);

            events.trigger('getBookingRequestsDone', data);
          }
        } catch(err) {
          console.log('getBookingRequests fail', arguments);
          events.trigger('getBookingRequestsFail', data);
        }
      }

      req();
    },

    // getBookingRequests: function(_data = [], _page = 1) {
    //   let data = _data;
    //   let page = _page;

    //   jQuery.ajax({
    //     'type'     : 'POST',
    //     'dataType' : 'json',
    //     'url'      : _s_asset.ajax_url,
    //     'context'  : this,
    //     'data'     : {
    //       'action'       : '_s_get_user_booking_requests_event',
    //       'paged'        : page,
    //       'nonce'        : this.$nonceBookingRequestsGet.val(),
    //       'nonce_accept' : this.$nonceBookingRequestAccept.val(),
    //       'nonce_reject' : this.$nonceBookingRequestReject.val()
    //     }
    //   })
    //   .done((res) => {
    //     console.log('getBookingRequests done', res);

    //     if (res.paged < res.max_num_pages) {
    //       // save data
    //       data = data.concat(res.booking_data);

    //       // increment page
    //       page++;

    //       // recurse
    //       this.getBookingRequests(data, page);
    //     }
    //     else {
    //       data = data.concat(res.booking_data);

    //       events.trigger('getBookingRequestsDone', data);
    //     }
    //   })
    //   .fail(() => {
    //     console.log('getBookingRequests fail', arguments);
    //     events.trigger('getBookingRequestsFail', data);
    //   });
    // },
    
    acceptBookingRequest: async function(e) {
      e.preventDefault();
      
      try {
        const data = await Utilities.ajaxPost(_s_asset.ajax_url, {
          'action'     : '_s_booking_request_accept_event',
          'post_id'    : e.target.dataset.postId,
          'user_id'    : e.target.dataset.userId,
          'date_start' : e.target.dataset.dateStart,
          'date_end'   : e.target.dataset.dateEnd,
          'nonce'      : this.$nonceBookingRequestAccept.val()
        });

        console.log('acceptBookingRequest done', data);
        if (data.data.asset_booked) {
          events.trigger('acceptBookingRequestDone', data.data);  
        }
      } catch(err) {
        console.log('acceptBookingRequest fail', err);
      }
    },

    // acceptBookingRequest: function(e) {
    //   e.preventDefault();

    //   jQuery.ajax({
    //     'type'     : 'POST',
    //     'dataType' : 'json',
    //     'url'      : _s_asset.ajax_url,
    //     'context'  : this,
    //     'data'     : {
    //       'action'     : '_s_booking_request_accept_event',
    //       'post_id'    : e.target.dataset.postId,
    //       'user_id'    : e.target.dataset.userId,
    //       'date_start' : e.target.dataset.dateStart,
    //       'date_end'   : e.target.dataset.dateEnd,
    //       'nonce'      : this.$nonceBookingRequestAccept.val()
    //     }
    //   })
    //   .done((data) => {
    //     console.log('acceptBookingRequest done', data);
    //     if (data.asset_booked) {
    //       events.trigger('acceptBookingRequestDone', data);  
    //     }
    //   })
    //   .fail(() => {
    //     console.log('acceptBookingRequest fail', arguments);
    //     // events.trigger('getBookingRequestsFail', arguments);
    //   });
    // },

    rejectBookingRequest: async function(e) {
      e.preventDefault();

      try {
        const data = await Utilities.ajaxPost(_s_asset.ajax_url, {
          'action'  : '_s_booking_request_reject_event',
          'post_id' : e.target.dataset.postId,
          'user_id' : e.target.dataset.userId,
          'nonce'   : this.$nonceBookingRequestReject.val()
        });
        console.log('rejectBookingRequest done', data);
        if (!data.data.asset_booked) {
          events.trigger('rejectBookingRequestDone', data.data);  
        }
      } catch(err) {
        console.log('rejectBookingRequest fail', err);
      }
    },

    // rejectBookingRequest: function(e) {
    //   e.preventDefault();

    //   jQuery.ajax({
    //     'type'     : 'POST',
    //     'dataType' : 'json',
    //     'url'      : _s_asset.ajax_url,
    //     'context'  : this,
    //     'data'     : {
    //       'action'  : '_s_booking_request_reject_event',
    //       'post_id' : e.target.dataset.postId,
    //       'user_id' : e.target.dataset.userId,
    //       'nonce'   : this.$nonceBookingRequestReject.val()
    //     }
    //   })
    //   .done((data) => {
    //     console.log('rejectBookingRequest done', data);
    //     if (!data.asset_booked) {
    //       events.trigger('rejectBookingRequestDone', data);  
    //     }
    //   })
    //   .fail(() => {
    //     console.log('rejectBookingRequest fail', arguments);
    //     // events.trigger('bookingRequestRejectFail', arguments);
    //   });
    // },

    handleBookingRequestAction: function(e) {
      e.preventDefault();

      if (e.target.dataset.action === 'accept') {
        this.acceptBookingRequest(e);
      } else if (e.target.dataset.action === 'reject') {
        this.rejectBookingRequest(e);
      }
    },




    /**
     * Availability
     */
    getAvailability: function() {
      let _this = this;
      let data  = [];
      let page  = 1;

      async function req() {
        try {
          const res = await Utilities.ajaxPost(_s_asset.ajax_url, {
            'action' : '_s_get_availability_event',
            'paged'  : page,
            'nonce'  : _this.$nonceAvailability.val()
          });

          console.log('getAvailability done', res);

          if (res.data.paged < res.data.max_num_pages) {
            // save data
            data = data.concat(res.data.availability_data);

            // increment page
            page++;

            // recurse
            req();
          }
          else {
            // save data
            data = data.concat(res.data.availability_data);

            events.trigger('getAvailabilityDone', data);
          }
        } catch(err) {
          console.log('getAvailability fail', arguments);
          events.trigger('getAvailabilityFail', arguments);
        }
      }
      req();
    },

    // getAvailability: function(_data = [], _page = 1) {
    //   let data = _data;
    //   let page = _page;

    //   jQuery.ajax({
    //     'type'     : 'POST',
    //     'dataType' : 'json',
    //     'url'      : _s_asset.ajax_url,
    //     'context'  : this,
    //     'data'     : {
    //       'action' : '_s_get_availability_event',
    //       'paged'  : page,
    //       'nonce'  : this.$nonceAvailability.val()
    //     }
    //   })
    //   .done((res) => {
    //     console.log('getAvailability done', res);

    //     if (res.paged < res.max_num_pages) {
    //       // save data
    //       data = data.concat(res.availability_data);

    //       // increment page
    //       page++;

    //       // recurse
    //       this.getAvailability(data, page);
    //     }
    //     else {
    //       // save data
    //       data = data.concat(res.availability_data);

    //       events.trigger('getAvailabilityDone', data);
    //     }
    //   })
    //   .fail(() => {
    //     console.log('getAvailability fail', arguments);
    //     events.trigger('getAvailabilityFail', arguments);
    //   });
    // },

    setAvailability: async function(e) {
      e.preventDefault();

      try {
        const data = await Utilities.ajaxPost(_s_asset.ajax_url, {
          'action'     : '_s_set_availability_event',
          'date_start' : this.$dateStart.val(),
          'date_end'   : this.$dateEnd.val(),
          'nonce'      : this.$nonceAvailability.val()
        });
        console.log('setAvailability done', data);
        // @TODO error handling 
        events.trigger('setAvailabilityDone', data.data);
      } catch(err) {
        console.log('setAvailability fail', arguments);
        events.trigger('setAvailabilityFail', arguments);
      }
    },

    deleteAvailability: async function(e) {
      e.preventDefault();

      try {
        const data = await Utilities.ajaxPost(_s_asset.ajax_url, {
          'action'  : '_s_delete_availability_event',
          'post_id' : e.target.dataset.postId,
          'nonce'   : this.$nonceAvailability.val()
        });

        console.log('deleteAvailability done', data);
        // @TODO error handling
        events.trigger('deleteAvailabilityDone', data.data, e);
      } catch(err) {
        console.log('deleteAvailability fail', arguments);
        events.trigger('deleteAvailabilityFail', e);
      }
    },

    handleAvailabilityDelete: function(e) {
      this.deleteAvailability(e);
    },

    

    handleDateChange: function(e) {
      if (e.target.id === 'date-start') {
        // reset $dateEnd val
        this.$dateEnd.val('');
        // change #date-end min to date-start val
        this.$dateEnd.prop('min', e.target.value);
      }
    },

    setMinDateToday: function() {
      const date = Utilities.getDateTodayISO();
      this.$dateStart.prop('min', date);
      this.$dateEnd.prop('min', date);
    },






    setAssetType: async function(e) {
      e.preventDefault();

      try {
        const data = await Utilities.ajaxPost(_s_asset.ajax_url, {
          'action'           : '_s_add_user_asset_type_event',
          'asset_type'       : this.$assetType.val(),
          'experience_level' : this.$experienceLevel.val(),
          'nonce'            : this.$nonceAssetTypes.val()
        });

        console.log('setAssetType done', data);
        // @TODO error handling
        events.trigger('setAssetTypeDone', data.data);
      } catch(err) {
        console.log('setAssetType fail', arguments);
        events.trigger('setAssetTypeFail', arguments);
      }
    },

    handleSetAssetTypeDone: function(data) {
      console.log('handleSetAssetTypeDone');
    },

    handleAssetTypeDelete: async function(e) {
      e.preventDefault();

      try {
        const data = await Utilities.ajaxPost(_s_asset.ajax_url, {
          'action'           : '_s_delete_user_asset_type_event',
          'asset_type'       : e.target.dataset.assetType,
          'experience_level' : e.target.dataset.expLevel,
          'nonce'            : this.$nonceAssetTypes.val()
        });

        console.log('handleAssetTypeDelete done', data);
        // @TODO error handling
        events.trigger('deleteAssetTypeDone', data.data);
      } catch(err) {
        throw new Error('handleAssetTypeDelete fail');
      }
    }

  };

  asset.init();

})()

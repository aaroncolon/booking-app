(function(exports) {
  'use strict';

  const Notification = function(_copy) {
    this._copy      = _copy || '';
    this._baseClass = 'notification';
  };

  Notification.prototype.create = function() {
    const p = document.createElement('p');
    const t = document.createTextNode(this._copy);
    p.appendChild(t);
    p.className = this._baseClass;
    return jQuery(p);
  };

  exports.Notification = Notification;
})(window)

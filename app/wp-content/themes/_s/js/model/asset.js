(function() {
  "use strict";

  const assetModel = {

    init: function() {
      this.cacheDom();
      this.bindEvents();
    },

    cacheDom: function() {
      // Availability Form
      this.$availabilityForm = jQuery('#form-availability');
      this.$dateStart        = this.$availabilityForm.find('#date-start');
      this.$dateEnd          = this.$availabilityForm.find('#date-end');

      // Asset Types
      this.$assetTypeForm   = jQuery('#form-user-asset-types');
      this.$assetType       = this.$assetTypeForm.find('#asset-type');
      this.$experienceLevel = this.$assetTypeForm.find('#experience-level'); 
      this.$assetTypesTable = jQuery('#user-asset-types__table tbody');

    },

    bindEvents: function() {
      // Booking Requests
      events.on('getBookingRequests', this.getBookingRequests, this);
      events.on('acceptBookingRequest', this.acceptBookingRequest, this);
      events.on('rejectBookingRequest', this.rejectBookingRequest, this);
      events.on('acceptBookingRequestDone', this.getBookingRequests, this);
      events.on('acceptBookingRequestDone', this.getAvailability, this);
      events.on('rejectBookingRequestDone', this.getBookingRequests, this);

      // Availability
      events.on('getAvailability', this.getAvailability, this);
      events.on('setAvailability', this.setAvailability, this);
      events.on('deleteAvailability', this.deleteAvailability, this);
      events.on('setAvailabilityDone', this.getAvailability, this);
      events.on('deleteAvailabilityDone', this.getAvailability, this);

      // Asset Types
      events.on('getAbilities', this.getAbilities, this);
      events.on('setAbilities', this.setAbilities, this);
      events.on('deleteAbilities', this.deleteAbilities, this);
    },


    /**
     * Booking Requests
     */
    getBookingRequests: function() {
      let data = [];
      let page = 1;

      async function req() {
        try {
          const res = await Utilities.ajaxGet(_s_utilities.rest_url + 'booking/v1/requests', {
            'page' : page
          });

          if (page < res.headers.get('X-WP-TotalPages')) {
            data = data.concat(res.json);
            page++;
            req();
          } else {
            data = data.concat(res.json);
            events.trigger('getBookingRequestsDone', data);
          }
        } catch(err) {
          console.log('getBookingRequests fail', err);
          events.trigger('getBookingRequestsFail', data);
        }
      }

      req();
    },

    acceptBookingRequest: async function(e) {
      e.preventDefault();
      
      try {
        const data = await Utilities.ajaxPostJson(_s_utilities.rest_url + 'booking/v1/respond', {
          'action'     : e.target.dataset.action,
          'post_id'    : e.target.dataset.postId,
          'user_id'    : e.target.dataset.userId,
          'date_start' : e.target.dataset.dateStart,
          'date_end'   : e.target.dataset.dateEnd
        });

        // console.log('acceptBookingRequest done', data);

        if (data.json.asset_booked) {
          events.trigger('acceptBookingRequestDone', data.json);  
        }
      } catch(err) {
        console.log('acceptBookingRequest fail', err);
      }
    },

    rejectBookingRequest: async function(e) {
      e.preventDefault();

      try {
        const data = await Utilities.ajaxPostJson(_s_utilities.rest_url + 'booking/v1/respond', {
          'action'     : e.target.dataset.action,
          'post_id'    : e.target.dataset.postId,
          'user_id'    : e.target.dataset.userId,
          'date_start' : e.target.dataset.dateStart,
          'date_end'   : e.target.dataset.dateEnd
        });

        // console.log('rejectBookingRequest done', data);

        if (!data.json.asset_booked) {
          events.trigger('rejectBookingRequestDone', data.json);  
        }
      } catch(err) {
        console.log('rejectBookingRequest fail', err);
      }
    },





    /**
     * Availability
     */
    getAvailability: function() {
      let data  = [];
      let page  = 1;

      async function req() {
        try {
          const res = await Utilities.ajaxGet(_s_utilities.rest_url + 'booking/v1/availability', {
            'page'  : page
          });

          if (page < res.headers.get('X-WP-TotalPages')) {
            data = data.concat(res.json);
            page++;
            req();
          } else {
            data = data.concat(res.json);
            events.trigger('getAvailabilityDone', data);
          }
        } catch(err) {
          console.log('getAvailability fail', arguments);
          events.trigger('getAvailabilityFail', arguments);
        }
      }
      req();
    },

    setAvailability: async function() {
      try {
        const data = await Utilities.ajaxPostJson(_s_utilities.rest_url + 'booking/v1/availability', {
          'date_start' : this.$dateStart.val(),
          'date_end'   : this.$dateEnd.val()
        });
        // console.log('setAvailability done', data.json);
        // @TODO error handling 
        events.trigger('setAvailabilityDone', data.json);
      } catch(err) {
        console.log('setAvailability fail', err);
        events.trigger('setAvailabilityFail', err);
      }
    },

    deleteAvailability: async function(e) {
      e.preventDefault();

      try {
        const data = await Utilities.ajaxDelete(_s_utilities.rest_url + 'booking/v1/availability', {
          'post_id' : e.target.dataset.postId
        });

        // console.log('deleteAvailability done', data.json);
        // @TODO error handling
        events.trigger('deleteAvailabilityDone', data.json, e);
      } catch(err) {
        console.log('deleteAvailability fail', arguments);
        events.trigger('deleteAvailabilityFail', e);
      }
    },







    /**
     * Abilities
     */
    getAbilities: async function() {
      try {
        const data = await Utilities.ajaxGet(_s_utilities.rest_url + 'booking/v1/asset-abilities', {});

        events.trigger('getAbilitiesDone', data.json);
      } catch(err) {
        console.log('getAbilities fail', err);
        events.trigger('getAbilitiesFail', err);
      }
    },

    setAbilities: async function() {
      try {
        const data = await Utilities.ajaxPostJson(_s_utilities.rest_url + 'booking/v1/asset-abilities', {
          'asset_type'       : this.$assetType.val(),
          'experience_level' : this.$experienceLevel.val()
        });

        console.log('data', data);

        events.trigger('setAbilitiesDone', data.json, e);
      } catch(err) {
        console.log('setAbilities fail', err);
        events.trigger('setAbilitiesFail', err);
      }
    },

    deleteAbilities: async function(e) {
      try {
        const data = await Utilities.ajaxDelete(_s_utilities.rest_url + 'booking/v1/asset-abilities', {
          'asset_type'       : e.target.dataset.assetType,
          'experience_level' : e.target.dataset.expLevel
        });

        events.trigger('deleteAbilitiesDone', data.json, e);
      } catch(err) {
        throw new Error('handleDeleteAbilities fail');
        events.trigger('deleteAbilitiesFail', err);
      }
    }

  };

  assetModel.init();

})()

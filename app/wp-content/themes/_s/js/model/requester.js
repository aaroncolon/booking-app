(function() {
  "use strict";

  const requesterModel = {

    _data: {
      'copy': {
        'assetDefault'     : 'Please request an asset.',
        'assetAvailable'   : 'Match found. Please confirm your request.',
        'assetUnavailable' : 'No matches available at this time.',
        'requestAssetComplete' : 'Your asset request has been received. The asset you requested must now accept or reject your request. We will notify you of the results shortly.'
      },
      'request' : {}
    },

    init: function() {
      this.bindEvents();
    },

    bindEvents: function() {
      events.on('requestAsset', this.requestAsset, this);
      events.on('requestAssetComplete', this.requestAssetComplete, this);
      events.on('getBookingRequests', this.getBookingRequests, this);
    },

    requestAsset: async function(args) {
      try {
        const data = await Utilities.ajaxGet(_s_utilities.rest_url + 'booking/v1/assets', args);

        console.log('data', data);

        if (data.json.available_assets) {
          events.trigger('assetAvailable', data.json);
        } else {
          events.trigger('assetUnavailable', data.json);
        }
      } catch(err) {
        console.log('requestAsset fail', err);
      }

      // ajax...
        // check assets
        // true
          // notification sent
          // awaiting acceptance
        // false
          // check for next best assets
            // true
              // email requester
                // next best asset requested
    },

    /**
     * @TODO Redirect to WooCommerce payment page
     * @TODO Upon successful payment, complete booking
     * @TODO Must assign user status to prevent double booking
     */
    requestAssetComplete: async function(args) {
      try {
        const data = await Utilities.ajaxPostJson(_s_utilities.rest_url + 'booking/v1/assets', args);

        console.log('requestAssetComplete data', data);

        if (data.json.post_id) {
          events.trigger('bookingRequestCreated', data.json);
        } else {
          events.trigger('bookingRequestFailed', data.json);
        }
      } catch(err) {
        console.log('requestAssetComplete fail', err);
        events.trigger('bookingRequestFailed', err);
      }
    },


    getBookingRequests: function() {
      let data = [];
      let page = 1;

      async function req() {
        try {
          const res = await Utilities.ajaxGet(_s_utilities.rest_url + 'booking/v1/requests', {
            'page': page
          });

          if (page < res.headers.get('X-WP-TotalPages')) {
            data = data.concat(res.json);
            page++;
            req();
          } else {
            data = data.concat(res.json);
            events.trigger('getBookingRequestsDone', data);
          }
        } catch(err) {
          console.log('getBookingRequests fail', err);
          events.trigger('getBookingRequestsFail', err);
        }
      }

      req();
    }

    // getBookingRequests: function() {
    //   // const daFetch = 
    //   fetch(_s_requester.rest_url + 'wp/v2/posts?per_page=3&_wpnonce='+_s_requester.rest_nonce).then((res) => {
    //       let entries = res.headers.entries();
    //       let ited = entries.next();
    //       while (!ited.done) {
    //         console.log(ited.value);
    //         ited = entries.next();
    //       }

    //       console.log('headers?', res.headers.get('X-WP-Total'), res.headers.get('X-WP-TotalPages'));
    //       console.log('res', res);
    //       res.json().then((resj) => {console.log('resj', resj)});
    //     });
    // },

  };

  requesterModel.init();
  
})()

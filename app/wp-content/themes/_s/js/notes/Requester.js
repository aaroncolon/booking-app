(function(exports) {
  'use strict';

  const Requester = function() {
    this.name = null;
    this.location = {
      primary: {
        address: {
          street: null,
          city: null,
          state: null,
          country: null
        }
      },
      secondary: {}
    };
  }; // Requester

  exports.Requester = Requester;
})(window)

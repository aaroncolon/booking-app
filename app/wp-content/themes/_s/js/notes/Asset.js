(function(exports) {
  'use strict';

  const Asset = function() {
    this.name = null;
    this.age = 0;
    this.location = {
      'primary': {
        'address': {
          'street': null,
          'city': null,
          'state': null,
          'country': null
        }
      },
      'secondary': {}
    };

    this.abilities = {
      'animator': {
        'level': {
          'label': 'junior',
          'value': 1
        }
      },
      'rigger': {
        'level': {
          'label': 'mid',
          'value': 2
        }
      },
      'supervisor': {
        'level': {
          'label': 'senior',
          'value': 3
        }
      }
    };

    this.availability = {
      'unavailable': 
      [
        {
          'start': '2019-02-01',
          'end': '2019-02-28'
        },
        {
          'start': '2019-03-01',
          'end': '2019-03-30'
        }
      ],
      'booked': 
      [
        {
          'requesterId': null,
          'dates': [
            {
              'start': '2019-03-01',
              'end' : '2019-03-31'
            }
          ]
        }
      ],
      'lastBooked' : null
    };

    this.requestStatus = 'pending'

    // once requested, asset can't be requested again until they accept/reject

    this.rate = {
      'primary': {
        'amount': null,
        'frequency': {
          'hour': 0,
          'day': 1,
          'week': 5
        },
        'overtime': {}
      },
      'secondary': {}      
    };
  }; // Asset

  exports.Asset = Asset;
})(window)

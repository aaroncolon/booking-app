(function(exports) {
  'use strict';

  const AssetRequest = function() {
    this.title            = null;
    this.date_start       = null;
    this.date_end         = null;
    this.date_start_unix  = null;
    this.date_end_unix    = null;
    this.asset_type       = null;
    this.experience_level = null;
    this.asset_email      = null; // currently used for Specific Asset request
    this.timestamp        = null;
    this.booking_status   = ['created', 'pending', 'booked', 'cancelled'];
    this.booking_status_code = [0, 1, 2, 3];

    // A way to track the asset we're waiting for a response from
    this.asset_queued = null; // null | ID

    // A way to track the history of asset responses
    this.asset_responses = [
      {
        'asset_id'  : 0,
        'response'  : 0, // 0: reject, 1: accept 
        'timestamp' : 12423564356
      },
      {
        'asset_id'  : 1,
        'response'  : 0, // 0: reject, 1: accept 
        'timestamp' : 65421123456
      }
    ];

    // A way to track the booked asset
    this.asset_booked = null; // null | ID



    // this.specificAsset = null; // null | ID
  };

  exports.AssetRequest = AssetRequest;
})(window)

<?php
/**
 * Template Name: App
 * 
 * The template for displaying the app page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

get_header();
?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main">

    <?php
    while ( have_posts() ) :
      the_post();
    
      $user = wp_get_current_user();
      if ( _s_is_user_role('asset', $user) ) :
        get_template_part( 'template-parts/content', 'asset' );
      elseif ( _s_is_user_role('requester', $user) ) :
        get_template_part( 'template-parts/content', 'requester' );
      elseif ( ! is_user_logged_in() ) :
        auth_redirect();
      endif;
    endwhile; // End of the loop.
    ?>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_footer();

<?php
/**
 * _s functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package _s
 */

if ( ! function_exists( '_s_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function _s_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on _s, use a find and replace
		 * to change '_s' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( '_s', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		// add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', '_s' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', '_s_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function _s_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( '_s_content_width', 640 );
}
add_action( 'after_setup_theme', '_s_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function _s_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', '_s' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', '_s' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', '_s_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function _s_scripts() {
	wp_enqueue_style( '_s-style', get_stylesheet_uri() );

	wp_enqueue_script( '_s-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( '_s-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( '_s-model-events', get_template_directory_uri() . '/js/model/Events.js', array(), '', true );

	wp_enqueue_script( '_s-model-utilities', get_template_directory_uri() . '/js/utilities/Utilities.js', array('jquery'), '', true );

	wp_localize_script( '_s-model-utilities', '_s_utilities', array(
			'rest_url'   => rest_url(),
			'rest_nonce' => wp_create_nonce('wp_rest'),
			'user_type'  => (is_user_logged_in()) ? ( (_s_is_user_role('requester')) ? 'requester' : 'asset' ) : null
		)
	);

	wp_enqueue_script( '_s-model-notification', get_template_directory_uri() . '/js/model/Notification.js', array('jquery'), '', true );

	wp_enqueue_script( '_s-global', get_template_directory_uri() . '/js/view/global.js', array('jquery'), '', true );

	wp_enqueue_script( '_s-notifications', get_template_directory_uri() . '/js/view/notifications.js', array('jquery'), '', true );

	if (_s_is_user_role('requester') || is_page_template('page_requester.php')) {
		// wp_enqueue_script( '_s-requester', get_template_directory_uri() . '/js/controller/requester.js', array('jquery'), '', true );

		wp_enqueue_script( '_s-requester-model', get_template_directory_uri() . '/js/model/requester.js', array('jquery'), '', true );

		wp_enqueue_script( '_s-requester-view', get_template_directory_uri() . '/js/view/requester.js', array('jquery', 'wp-util'), '', true );

		wp_enqueue_script( '_s-requester-controller', get_template_directory_uri() . '/js/controller/requester-rest.js', array('jquery'), '', true );

		wp_localize_script( '_s-requester-controller', '_s_requester', array( 
				'ajax_url'   => admin_url('admin-ajax.php'),
				'rest_nonce' => wp_create_nonce('wp_rest')
			)
		);
	}
	
	if (_s_is_user_role('asset') || is_page_template('page_asset.php')) {
		wp_enqueue_script( '_s-asset-view', get_template_directory_uri() . '/js/view/asset-view.js', array('jquery', 'wp-util'), '', true );

		wp_enqueue_script( '_s-asset-model', get_template_directory_uri() . '/js/model/asset.js', array('jquery'), '', true );

		// wp_enqueue_script( '_s-asset', get_template_directory_uri() . '/js/controller/asset.js', array('jquery'), '', true );	

		wp_enqueue_script( '_s-asset', get_template_directory_uri() . '/js/controller/asset-rest.js', array('jquery'), '', true );	

		wp_localize_script( '_s-asset', '_s_asset', array( 
			'ajax_url'   => admin_url('admin-ajax.php'), 
			'rest_nonce' => wp_create_nonce('wp_rest'),
			'last_booked' => date( 'Y-m-d', get_user_meta(get_current_user_id(), '_re_last_booked', true) )
			)
		);
	}

	if (is_page_template('page_app-react.php')) {
		wp_enqueue_script( '_s-react', get_template_directory_uri() . '/js-react/dist/scripts.js', array(), '', true );

		wp_localize_script( '_s-react', '_s_react', array(
				'asset_types' => _s_get_asset_types(),
				'exp_levels' => _s_get_exp_levels()
			)
		);
	}
}
add_action( 'wp_enqueue_scripts', '_s_scripts' );

function _s_get_asset_types() {
	$asset_types_options = array();
	$asset_types = get_terms( array('taxonomy' => '_s_asset_types', 'hide_empty' => false) );

	foreach ($asset_types as $k => $v) :
	  // exclude child terms
	  if ($v->parent !== 0) {
	    continue;
	  }
	  $asset_types_options[] = array(
	  	'name' => $v->name,
	  	'slug' => $v->slug
	  );
	endforeach;
	
	return $asset_types_options;
}

function _s_get_exp_levels() {
	$exp_levels_options = array();
	$exp_levels = get_terms( array('taxonomy' => '_s_experience_levels', 'hide_empty' => false) );

	foreach ($exp_levels as $k => $v) :
	  $exp_levels_options[] = array(
	  	'name' => $v->name,
	  	'slug' => $v->slug
	  );
	endforeach;
	
	return $exp_levels_options;
}

function _s_js_templates() {
	require 'inc/templates/booking-request-summary-row.php';
	require 'inc/templates/booking-requests-row.php';
	require 'inc/templates/availability-row.php';
	require 'inc/templates/abilities-row.php';

}
add_action( 'wp_footer', '_s_js_templates' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Custom user functions
 */
require get_template_directory() . '/inc/custom-functions.php';

/**
 * Custom functions requester
 */
require get_template_directory() . '/inc/custom-functions-requester.php';

/**
 * Custom functions asset
 */
require get_template_directory() . '/inc/custom-functions-asset.php';

/**
 * Custom CRON functions
 */
require get_template_directory() . '/inc/custom-functions-cron.php';

/**
 * Custom Email functions
 */
require get_template_directory() . '/inc/custom-functions-emails.php';

/**
 * AJAX functions requester
 */
// require get_template_directory() . '/inc/ajax-functions-requester.php';

/**
 * AJAX functions asset
 */
// require get_template_directory() . '/inc/ajax-functions-asset.php';

/**
 * Webhook functions
 */
require get_template_directory() . '/inc/webhook-inbound.php';

/**
 * REST endpoints Common
 */
require get_template_directory() . '/inc/rest-endpoints-common.php';

/**
 * REST endpoints Requester
 */
require get_template_directory() . '/inc/rest-endpoints-requester.php';

/**
 * REST endpoints Asset
 */
require get_template_directory() . '/inc/rest-endpoints-asset.php';

/**
 * REST endpoints Availability
 */
require get_template_directory() . '/inc/rest-endpoints-availability.php';

/**
 * REST endpoints Asset Abilities
 */
require get_template_directory() . '/inc/rest-endpoints-asset-abilities.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

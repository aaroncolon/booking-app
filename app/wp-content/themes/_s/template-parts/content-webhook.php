<?php
/**
 * Template part for displaying page content in page_webhook.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

?>
<?php
// create nonce
$nonce_webhook_inbound = wp_create_nonce('nonce_webhook_inbound');

// get raw input data
$raw_data = file_get_contents('php://input');

// call php method directly
_s_webhook_inbound($raw_data, $nonce_webhook_inbound);

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  </header><!-- .entry-header -->

  <div class="entry-content">

  </div><!-- .entry-content -->

  <footer class="entry-footer">
    
  </footer><!-- .entry-footer -->
  
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for displaying page content in requester.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

?>
<?php
$nonce_request_asset        = wp_create_nonce('nonce_request_asset');
$nonce_get_booking_requests = wp_create_nonce('nonce_get_booking_requests');
$nonce_get_booked_assets    = wp_create_nonce('nonce_get_booked_assets');
$asset_types                = get_terms( array('taxonomy' => '_s_asset_types', 'hide_empty' => false) );
$experience_levels          = get_terms( array('taxonomy' => '_s_experience_levels', 'hide_empty' => false) );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
    <div class="row column">
      <?php the_title( '<h1 class="entry-title">', ' (Requester View)</h1>' ); ?>
    </div>
  </header><!-- .entry-header -->

  <div class="entry-content">

    <div class="row">

      <div class="column medium-4">
        <!-- Request Asset Section -->
        <div class="section section--request-asset">
          
          <h2 class="title title--section">Request Asset</h2>

          <div class="wrap wrap--form">
            <form id="form-request-asset" action="" method="POST">

              <div class="form-group form-group--asset-new-specific">
                <fieldset name="asset-new-specific-options">
                  <legend>New or Specific Asset?</legend>
                  <div class="radio">
                    <label>
                      <input type="radio" name="asset-new-specific" id="asset-new" value="new" checked>
                      New Asset
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="asset-new-specific" id="asset-specific" value="specific">
                      Specific Asset
                    </label>
                  </div>
                </fieldset>
              </div>

              <div class="form-group" style="display: none">
                <label for="asset-email">Asset's Email</label>
                <input class="form-control" type="text" id="asset-email" name="asset-email" required disabled>
              </div>

              <div class="form-group form-group--asset-new">
                <label for="asset-type">Asset Type</label>
                <select class="form-control" id="asset-type" name="asset-type" required>
                  <option value="" selected>Select Asset Type...</option>
                  <?php
                  foreach ($asset_types as $k => $v) :
                    // exclude child terms
                    if ($v->parent !== 0) {
                      continue;
                    }
                  ?>
                    <option value="<?php echo esc_attr($v->slug) ?>"><?php echo esc_html($v->name) ?></option>
                  <?php
                  endforeach;
                  ?>
                </select>
              </div>

              <div class="form-group form-group--asset-new">
                <label for="experience-level">Experience Level</label>
                <select class="form-control" id="experience-level" name="experience-level" required>
                  <option value="" selected>Select Experience Level...</option>
                  <?php
                  foreach ($experience_levels as $k => $v) :
                  ?>
                    <option value="<?php echo esc_attr($v->slug) ?>"><?php echo esc_html($v->name) ?></option>
                  <?php
                  endforeach;
                  ?>
                </select>
              </div>

              <div class="form-group">
                <label for="date-start">Start Date</label>
                <input class="form-control" type="date" id="date-start" name="date-start" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
              </div>

              <div class="form-group">
                <label for="date-end">End Date</label>
                <input class="form-control" type="date" id="date-end" name="date-end" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
              </div>

              <div class="form-group">
                <input id="nonce-request-asset" type="hidden" name="nonce" value="<?php esc_attr_e($nonce_request_asset); ?>">
                <input id="submit-request-asset" type="submit" value="Submit">
              </div>

            </form>
          </div>
        </div><!-- / Request Asset Section -->
      </div><!-- / .column -->

      <div class="column medium-8">

        <div id="request-summary" class="section section--request-summary">
          <header class="request-summary__header">
            <h2 class="title title--section">Request Summary</h2>
            <div class="request-summary__message">Please request an asset.</div>
          </header>
          <div class="request-summary__summary" style="display: none; opacity: 0;">
            <table id="request-summary__table">
              <thead>
                <tr>
                  <th>Asset Type</th>
                  <th>Experience Level</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Asset Email</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>

            <div class="request-summary__button-wrap">
              <form id="form-request-asset-complete" action="" method="POST">
                <input id="submit-request-asset-complete" type="submit" value="Complete Request">
              </form>
            </div><!-- / .request-summary__button-wrap -->

          </div><!-- / .request-summary__summary -->
        </div><!-- / #request-summary -->
      
      </div><!-- / .column -->
    </div><!-- / .row -->

    <div class="row column">
      <hr />
    </div>

    <div class="row">
      <div class="column">

        <div id="booking-requests" class="section section--booking-requests">
          <h2 class="title title--section">Booking Requests</h2>
          
          <div class="booking-requests__table-wrap">
            <table id="booking-requests__table">
              <thead>
                <tr>
                  <th>Asset Type</th>
                  <th>Exp Level</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
            <input id="nonce-get-booking-requests" type="hidden" value="<?php esc_attr_e($nonce_get_booking_requests) ?>">
          </div>
        </div>

      </div>
    </div>

  </div><!-- .entry-content -->

  <footer class="entry-footer">
    
  </footer><!-- .entry-footer -->
  
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * Template part for displaying the Notifiactions content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */
?>

<div class="section-notifications">
  <div id="notification-wrap" class="notification-wrap">
  </div>
</div>

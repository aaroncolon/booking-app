<?php
/**
 * Template part for displaying page content in page_asset.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
    <div class="row column">
      <?php the_title( '<h1 class="entry-title">', ' (Asset View)</h1>' ); ?>
    </div>
  </header><!-- .entry-header -->

  <div class="entry-content">

    <div id="react-root"></div>

  </div><!-- .entry-content -->

  <footer class="entry-footer">
    
  </footer><!-- .entry-footer -->
  
</article><!-- #post-<?php the_ID(); ?> -->

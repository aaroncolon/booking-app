<?php
/**
 * Template part for displaying page content in page_asset.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

?>
<?php
$nonce_booking_request_accept = wp_create_nonce('nonce_booking_request_accept');
$nonce_booking_request_reject = wp_create_nonce('nonce_booking_request_reject');
$nonce_booking_requests_get   = wp_create_nonce('nonce_booking_requests_get');
$nonce_availability           = wp_create_nonce('nonce_availability');

$user_abilities = get_user_meta(get_current_user_id(), '_re_asset_type', false);
$last_booked = get_user_meta(get_current_user_id(), '_re_last_booked', true);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
    <div class="row column">
      <?php the_title( '<h1 class="entry-title">', ' (Asset View)</h1>' ); ?>
    </div>
  </header><!-- .entry-header -->

  <div class="entry-content">

    <div class="row">
      <div class="column">
        <p><strong>Last Booked:</strong> <?php echo date('Y-m-d', $last_booked) ?></p>

        <!-- Booking Requests Section -->
        <div id="booking-requests" class="section section--booking-requests">
          <h2 class="title title--section">Booking Requests</h2>
          <p>These are your current Booking Requests:</p>
          
          <div class="booking-requests__table-wrap">
            <table id="booking-requests__table">
              <thead>
                <tr>
                  <th>Asset Type</th>
                  <th>Experience Level</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
            <input id="nonce-booking-requests-get" type="hidden" value="<?php esc_attr_e($nonce_booking_requests_get) ?>">
            <input id="nonce-booking-request-accept" type="hidden" value="<?php esc_attr_e($nonce_booking_request_accept) ?>">
            <input id="nonce-booking-request-reject" type="hidden" value="<?php esc_attr_e($nonce_booking_request_reject) ?>">
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="column medium-8">
        <div id="availability-summary" class="section section--availability-summary">
          <h2 class="title title--section">Unavailability Summary</h2>
          <p>You won't be booked for the dates below:</p>

          <div class="availability-summary__table-wrap">
            <table id="availability-summary__table">
              <thead>
                <tr>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Booking ID</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="column medium-4">
        <!-- Availability Section -->
        <div id="availability-add" class="section section--availability-add">
          <h2 class="title title--section">Add Unavailability</h2>

          <form id="form-availability" action="" method="POST">

            <div class="form-group">
              <label for="date-start">Start Date</label>
              <input class="form-control" type="date" id="date-start" name="date-start" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
            </div>

            <div class="form-group">
              <label for="date-end">End Date</label>
              <input class="form-control" type="date" id="date-end" name="date-end" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
            </div>

            <div class="form-group">
              <input id="nonce-availability" name="nonce-availability" type="hidden" value="<?php esc_attr_e($nonce_availability); ?>">
              <?php // wp_nonce_field('add_availability'); ?>
              <input id="submit-availability" type="submit" value="Submit">
            </div>

          </form>
        </div>
      </div>
    </div><!-- / .row -->

    <div class="row">
      <div class="column">

        <div id="user-asset-types" class="section section--user-asset-types">
          <h2 class="title title--section">Abilities</h2>

          <div class="row">
            <div class="column medium-8">
              <h3 class="title title--column">Abilities Summary</h3>

              <div class="user-asset-types__summary">
                <table id="user-asset-types__table">
                  <thead>
                    <tr>
                      <th>Asset Type</th>
                      <th>Experience</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>

            </div>
            <div class="column medium-4">
              <h3 class="title title--column">Add Abilities</h3>

              <div class="user-asset-types__add">
                <?php
                function _s_do_form_user_asset_types() {
                  $nonce_user_asset_types = wp_create_nonce('nonce_user_asset_types');
                  $asset_types = get_terms( array('taxonomy' => '_s_asset_types', 'hide_empty' => false) );
                  $experience_levels = get_terms( array('taxonomy' => '_s_experience_levels', 'hide_empty' => false) ); 
                ?>
                  <form id="form-user-asset-types" action="" method="POST">

                    <div class="form-group">
                      <label for="asset-type">Asset Type</label>
                      <select class="form-control" id="asset-type" name="asset-type" required>
                        <option value="" selected>Select Asset Type...</option>
                        <?php
                        foreach ($asset_types as $k => $v) :
                          // exclude child terms
                          if ($v->parent !== 0) {
                            continue;
                          }
                        ?>
                          <option value="<?php echo esc_attr($v->slug) ?>"><?php echo esc_html($v->name) ?></option>
                        <?php
                        endforeach;
                        ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="experience-level">Experience Level</label>
                      <select class="form-control" id="experience-level" name="experience-level" required>
                        <option value="" selected>Select Experience Level...</option>
                        <?php
                        foreach ($experience_levels as $k => $v) :
                        ?>
                          <option value="<?php echo esc_attr($v->slug) ?>"><?php echo esc_html($v->name) ?></option>
                        <?php
                        endforeach;
                        ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <input id="nonce-user-asset-types" type="hidden" name="nonce" value="<?php esc_attr_e($nonce_user_asset_types); ?>">
                      <input id="submit-user-asset-types" type="submit" value="Submit">
                    </div>

                  </form>
                <?php 
                } 
                _s_do_form_user_asset_types();
                ?>
              </div>

            </div>
          </div>

        </div><!-- / .section--user-asset-types -->

      </div><!-- .column -->
    </div><!-- / .row -->

  </div><!-- .entry-content -->

  <footer class="entry-footer">
    
  </footer><!-- .entry-footer -->
  
</article><!-- #post-<?php the_ID(); ?> -->

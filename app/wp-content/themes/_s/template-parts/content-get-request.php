<?php
/**
 * Template part for displaying page content in page_get-request.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

$nonce_get_request = wp_create_nonce('nonce_get_request');
$query_data        = _s_get_request_router_ajax($nonce_get_request);

require get_template_directory() . '/js/controller/get-request.php';

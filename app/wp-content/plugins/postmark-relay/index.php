<?php
/**
 * Plugin Name: Postmark Relay
 * Description: Postmark Email Relay
 * Author: Aaron Colón
 * Version: 1.0
 */

require_once('vendor/autoload.php');
require_once "postmark_relay_settings.php";

use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;

function ac_dump_data($data = null) {
  echo '<pre>';
    var_dump($data);
  echo '</pre>';
}

function ac_get_json() {
  $json = null;

  if (!empty(file_get_contents('php://input'))) {
    $json = file_get_contents('php://input');
  }

  return $json;
}

/** 
 *
 *
 *
 */
function ac_send_email() {
  $client = new PostmarkClient('bc398e77-196e-4284-8f05-81d0d9aec63b');
  // $sendResult = $client->sendEmail(
  //   'sender@example.org',
  //   'aaron@aaron-colon.com',
  //   'Hello!',
  //   `'Email Body'
  // );
  echo '<pre>';
    var_dump($client);
  echo '</pre>';
}




/*

Mail Relay Flow
1. User registers - UID is created
2. User creates ad
    1. Ad ID is created.
    2. Ad unique Email ID generated
    3. Email ID associated with User ID
        1. Store unique Email ID on User's Options table in array 

1. Buyer emails Seller UEID (ad-0001@inbound.aaron-colon.com)
2. Postmark layer Buyer -> Seller
    1. Postmark receives email
    2. Event/Webhook: Receive Email (inbound)
        1. Wordpress Webhook Listener???
        2. Check buyer's real email WordPress database
        3. If not already saved, save buyer's real email (and assign an UID)? (wp database)
            1. Generate unique buyer anon email address and save (wp database)
        4. If already saved, get buyer's anon email address from database
        5. Look-up ad's UEID
        6. Get seller's real email address
        7. Postmark set "From" to buyer's anon email address
        8. Relay/Send email to Seller's real email address

1. Seller emails Buyer
2. Postmark layer Seller -> Buyer
    1. Seller replies to Buyer's anon email address
    2. Postmark sending email...
    3. Event: Pre-Send Email
        1. Find Buyer's real email address from Buyer's anon email address
        2. Postmark set "From" to ad's UEID
        3. Postmark set "To" to Buyer's real address
    4. Send email to Buyer's real address

Craigslist relay anons all emails
*/


